<?php
/**
 *
 * Additional Dynamic CSS for CC2 Premium Pack
 *
 * A part of the CSS will be created dynamically
 * and then loaded in the header.
 *
 * @author Konrad Sroka
 * @package cc2
 * @since 2.0
 *
 */
 
if( !function_exists('cc2_maybe_add_hex') ) :
	function cc2_maybe_add_hex( $color ) {
		$return = $color;
		
		if( strpos( $return, '#' ) === false ) {
			$return = '#' . $return;
		}
		
		return $return;
	}

endif;

if( !class_exists( 'cc2_Pasteur' ) ) :
	include_once( trailingslashit( get_template_directory() ). 'includes/pasteur.class.php' );
endif;
 
//add_action( 'wp_head', 'cc2pro_additional_dynamic_css' );

add_action( 'cc2_additional_dynamic_css_after_close_tag', 'cc2pro_additional_dynamic_css' );

function cc2pro_additional_dynamic_css() {
	error_reporting( E_ALL );
	
	$cc2_advanced_settings = get_option( 'cc2_advanced_settings', false);
	$arrWidgetSettings = array(
		'widgets_footer_fullwidth' => array('class' => 'footer-fullwidth-wrap' ),
		'widgets_footer_column' => array('class' => 'footer-columns-wrap'),
		'widgets_header' => array('class' => ''),
	);

	$arrTopNavMods = array(
		'top_nav_font_family', 
		'top_nav_font_size', 
		
		'top_nav_hover_background_color', 
	 
		
		'top_nav_active_background_color', /** current_menu_item */
		'top_nav_active_text_color', 
		
		
		'top_nav_dropdown_background_color',
		'top_nav_dropdown_text_color',
		'top_nav_dropdown_hover_background_color', /* (<= propably doesnt work => clashes with @link_hover)*/
		'top_nav_dropdown_hover_text_color',
	);

    $arrSecondaryNavMods = array(
        'secondary_nav_font_family',
        'secondary_nav_font_size',

        'secondary_nav_hover_background_color',


        'secondary_nav_active_background_color', /** current_menu_item */
        'secondary_nav_active_text_color',


        'secondary_nav_dropdown_background_color',
        'secondary_nav_dropdown_text_color',
        'secondary_nav_dropdown_hover_background_color', /* (<= propably doesnt work => clashes with @link_hover)*/
        'secondary_nav_dropdown_hover_text_color',
    );


    // populate data
    foreach( $arrTopNavMods as $strThemeMod ) {
        $arrTopNavSettings[ $strThemeMod ] = get_theme_mod( $strThemeMod, false );
    }
    foreach( $arrSecondaryNavMods as $strThemeMod ) {
        $arrSecondaryNavSettings[ $strThemeMod ] = get_theme_mod( $strThemeMod, false );
    }


    ?>    
    <!-- cc2pro additional css -->
    <style type="text/css">

     /*
     *
     * --> Top Navigation <--
     *
     */
	
	<?php if( !empty( $arrTopNavSettings['top_nav_font_family'] ) || !empty( $arrTopNavSettings['top_nav_font_size'] ) ) { ?>
	
		.site-navigation-top {
		<?php 
		
		// font family + optional font size
		if( !empty( $arrTopNavSettings['top_nav_font_family'] ) && $arrTopNavSettings['top_nav_font_family'] != 'inherit' ) { ?>
			font-family: <?php echo $arrTopNavSettings['top_nav_font_family']; ?>;
		<?php }
		
		if( !empty( $arrTopNavSettings['top_nav_font_size'] ) ) { ?>
			font-size: <?php echo $arrTopNavSettings['top_nav_font_size']; ?>;
		<?php } ?>
		}
	
	<?php
	}
	
	
	// top_nav_hover_background_color => LESS candidate
	
	if( !empty( $arrTopNavSettings['top_nav_hover_background_color'] ) ) { ?>	
	.site-navigation-top .navbar-nav > li > a:hover, 
	.site-navigation-top .navbar-nav > li > a:active,
	.site-navigation-top .navbar-nav > li > a:focus,
	.site-navigation-top.navbar-default .navbar-nav > li > a:hover, 
	.site-navigation-top.navbar-default .navbar-nav > li > a:active,
	.site-navigation-top.navbar-default .navbar-nav > li > a:focus
	{
		background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrTopNavSettings['top_nav_hover_background_color'] ); ?>;
	}
	<?php
	}
	
	// .active / .current_menu_item => future LESS candidate 
	// 'top_nav_active_background_color', 'top_nav_active_text_color', 
	
	if( !empty( $arrTopNavSettings['top_nav_active_background_color'] ) || !empty( $arrTopNavSettings['top_nav_active_text_color'] ) ) { ?>
	.site-navigation-top .navbar-nav > .active > a {
		<?php if( !empty( $arrTopNavSettings['top_nav_active_background_color'] ) ) { ?>
		background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrTopNavSettings['top_nav_active_background_color'] ); ?>;
		<?php }
		if( !empty( $arrTopNavSettings['top_nav_active_text_color'] ) ) { ?>
		color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrTopNavSettings['top_nav_active_text_color'] ); ?>;
		<?php } ?>
	}
		
	<?php
	}
	
	/*'top_nav_dropdown_text_color',
		'top_nav_dropdown_hover_background_color',
		* 'top_nav_dropdown_hover_text_color'
	*/

	// 
	if( !empty( $arrTopNavSettings['top_nav_dropdown_background_color'] ) ) { ?>
        .site-navigation-top .navbar-nav > li > .dropdown-menu, .site-navigation-top .navbar-nav > li > .dropdown-menu ul {
            <?php if( !empty( $arrTopNavSettings['top_nav_dropdown_background_color'] ) ) { ?>
                background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrTopNavSettings['top_nav_dropdown_background_color'] ); ?>;
            <?php } ?>
        }
    <?php
	}

	if( !empty( $arrTopNavSettings['top_nav_dropdown_text_color'] ) ) { ?>
        .site-navigation-top .navbar-nav > li > .dropdown-menu a {
            <?php if( !empty( $arrTopNavSettings['top_nav_dropdown_text_color'] ) ) { ?>
                color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrTopNavSettings['top_nav_dropdown_text_color'] ); ?>;
            <?php } ?>
        }
    <?php
    }


    if( !empty( $arrTopNavSettings['top_nav_dropdown_hover_background_color'] ) ||
        !empty( $arrTopNavSettings['top_nav_dropdown_hover_text_color'] )
    ) { ?>
		
		.site-navigation-top .navbar-nav > li > .dropdown-menu a:hover {
		<?php if( !empty( $arrTopNavSettings['top_nav_dropdown_hover_background_color'] ) ) { ?>
			background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrTopNavSettings['top_nav_dropdown_hover_background_color'] ); ?>;
		<?php } ?>
		
		
		<?php if( !empty( $arrTopNavSettings['top_nav_dropdown_hover_text_color'] ) ) { ?>
			color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrTopNavSettings['top_nav_dropdown_hover_text_color'] ); ?>;
		<?php } ?>
		
		
		}
	<?php
		
	}
	
    /*
     *
     * --> SITE NAVIGATION INNER <--
     *
     */

	if( !empty( $arrSecondaryNavSettings['secondary_nav_font_family'] ) || !empty( $arrSecondaryNavSettings['secondary_nav_font_size'] ) ) { ?>

    .site-navigation-inner {
    <?php

    // font family + optional font size
    if( !empty( $arrSecondaryNavSettings['secondary_nav_font_family'] ) && $arrSecondaryNavSettings['secondary_nav_font_family'] != 'inherit' ) { ?>
        font-family: <?php echo $arrSecondaryNavSettings['secondary_nav_font_family']; ?>;
    <?php }

    if( !empty( $arrSecondaryNavSettings['secondary_nav_font_size'] ) ) { ?>
        font-size: <?php echo $arrSecondaryNavSettings['secondary_nav_font_size']; ?>;
    <?php } ?>
    }

    <?php
    }


    // secondary_nav_hover_background_color => LESS candidate

    if( !empty( $arrSecondaryNavSettings['secondary_nav_hover_background_color'] ) ) { ?>
    .site-navigation-inner .navbar-nav > li > a:hover,
    .site-navigation-inner .navbar-nav > li > a:active,
    .site-navigation-inner .navbar-nav > li > a:focus,
    .site-navigation-inner.navbar-default .navbar-nav > li > a:hover,
    .site-navigation-inner.navbar-default .navbar-nav > li > a:active,
    .site-navigation-inner.navbar-default .navbar-nav > li > a:focus
    {
        background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrSecondaryNavSettings['secondary_nav_hover_background_color'] ); ?>;
    }
    <?php
    }

    // .active / .current_menu_item => future LESS candidate
    // 'secondary_nav_active_background_color', 'secondary_nav_active_text_color',

    if( !empty( $arrSecondaryNavSettings['secondary_nav_active_background_color'] ) || !empty( $arrSecondaryNavSettings['secondary_nav_active_text_color'] ) ) { ?>
    .site-navigation-inner .navbar-nav > .active > a {
    <?php if( !empty( $arrSecondaryNavSettings['secondary_nav_active_background_color'] ) ) { ?>
        background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrSecondaryNavSettings['secondary_nav_active_background_color'] ); ?>;
    <?php }
    if( !empty( $arrSecondaryNavSettings['secondary_nav_active_text_color'] ) ) { ?>
        color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrSecondaryNavSettings['secondary_nav_active_text_color'] ); ?>;
    <?php } ?>
    }

    <?php
    }

    /*'secondary_nav_dropdown_text_color',
		'secondary_nav_dropdown_hover_background_color',
		* 'secondary_nav_dropdown_hover_text_color'
	*/

	//
	if( !empty( $arrSecondaryNavSettings['secondary_nav_dropdown_background_color'] ) ) { ?>
        .site-navigation-inner .navbar-nav > li > .dropdown-menu, .site-navigation-inner .navbar-nav > li > .dropdown-menu ul {
        <?php if( !empty( $arrSecondaryNavSettings['secondary_nav_dropdown_background_color'] ) ) { ?>
            background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrSecondaryNavSettings['secondary_nav_dropdown_background_color'] ); ?>;
        <?php } ?>
        }
    <?php
	}

	if( !empty( $arrSecondaryNavSettings['secondary_nav_dropdown_text_color'] ) ) { ?>
        .site-navigation-inner .navbar-nav > li > .dropdown-menu a {
        <?php if( !empty( $arrSecondaryNavSettings['secondary_nav_dropdown_text_color'] ) ) { ?>
            color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrSecondaryNavSettings['secondary_nav_dropdown_text_color'] ); ?>;
        <?php } ?>
        }
    <?php
    }


    if( !empty( $arrSecondaryNavSettings['secondary_nav_dropdown_hover_background_color'] ) ||
        !empty( $arrSecondaryNavSettings['secondary_nav_dropdown_hover_text_color'] )
    ) { ?>

    .site-navigation-inner .navbar-nav > li > .dropdown-menu a:hover {
    <?php if( !empty( $arrSecondaryNavSettings['secondary_nav_dropdown_hover_background_color'] ) ) { ?>
        background-color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrsecondaryNavSettings['secondary_nav_dropdown_hover_background_color'] ); ?>;
    <?php } ?>


    <?php if( !empty( $arrsecondaryNavSettings['secondary_nav_dropdown_hover_text_color'] ) ) { ?>
        color: <?php echo cc2_Pasteur::sanitize_hex_color_with_transparency( $arrsecondaryNavSettings['secondary_nav_dropdown_hover_text_color'] ); ?>;
    <?php } ?>


    }
    <?php

    }


    ?>
    /** Widgets **/

    <?php 	// Widget Title Font Family
    if( get_theme_mod( 'widget_title_background_color', false ) != false || get_theme_mod( 'widget_title_font_family', false ) != false ) { ?>
    .widgettitle {
    <?php if( get_theme_mod( 'widget_title_font_family', false ) != false ): ?>
        font-family: <?php echo get_theme_mod( 'widget_title_font_family' ); ?>;
    <?php endif; ?>
    <?php if( get_theme_mod( 'widget_title_background_color', false ) != false ): ?>
        margin-top: 10px;
        padding: 10px;
        background: #<?php echo get_theme_mod( 'widget_title_background_color' ); ?>;
    <?php endif; ?>
    }
    <?php } ?>
    

	<?php
	foreach( $arrWidgetSettings as $strWidgetSetting => $arrWidgetAreaAtts ) { 
		$arrWidgetThemeMod = get_theme_mod( $strWidgetSetting, false );
		
		
		if( $arrWidgetThemeMod != false && !empty( $arrWidgetAreaAtts['class']) ) { ?>
	
	/** Widgets: <?php echo ucwords( str_replace('_', ' ', $strWidgetSetting ) ); ?> */
		
			<?php if( !empty( $arrWidgetThemeMod['title_font_family'] )
			|| !empty( $arrWidgetThemeMod['title_font_size'] )
			|| !empty( $arrWidgetThemeMod['title_text_color'] ) 
			|| !empty( $arrWidgetThemeMod['title_background_color'] ) 	
			) { ?>
				
	#<?php echo $arrWidgetAreaAtts['class']; ?> .widgettitle {
		
		<?php if( !empty( $arrWidgetThemeMod['title_font_family'] ) ) { ?>
		font-family: <?php echo $arrWidgetThemeMod['title_font_family']; ?>;
		<?php } ?>
		
		<?php if( !empty( $arrWidgetThemeMod['title_font_size'] ) ) { ?>
		font-size: <?php echo $arrWidgetThemeMod['title_font_size']; ?>;
		<?php } ?>
		
		<?php if( !empty( $arrWidgetThemeMod['title_font_weight'] ) ) { ?>
		font-weight: <?php echo $arrWidgetThemeMod['title_font_weight']; ?>;
		<?php } ?>
		
		<?php if( !empty( $arrWidgetThemeMod['title_text_color'] ) ) { ?>
		color: <?php echo cc2_maybe_add_hex($arrWidgetThemeMod['title_text_color']); ?>;
		<?php } ?>
		
		<?php if( !empty( $arrWidgetThemeMod['[title_background_color'] ) ) { ?>
		background-color: <?php echo cc2_maybe_add_hex( $arrWidgetThemeMod['title_background_color'] ); ?>;
		<?php } ?>
		
	}
			<?php }
		
			if( !empty( $arrWidgetThemeMod['background_color'] ) ) { ?>
				
	#<?php echo $arrWidgetAreaAtts['class']; ?> .widgetarea {
		background-color: <?php echo cc2_maybe_add_hex( $arrWidgetThemeMod['background_color'] ); ?>;
	}
				
			<?php } ?>
	
			<?php if( !empty( $arrWidgetThemeMod['link_color'] ) ) { ?>
				
	.<?php echo $arrWidgetAreaAtts['class']; ?> .widgetarea a {
		color: <?php echo cc2_maybe_add_hex( $arrWidgetThemeMod['link_color'] ); ?>;
	}
			
			
			<?php } ?>
			
			<?php if( !empty( $arrWidgetThemeMod['link_hover_color'] )
			|| !empty( $arrWidgetThemeMod['link_hover_background_color'] )
			) { ?>
				
	#<?php echo $arrWidgetAreaAtts['class']; ?> .widgetarea a:hover, 
	#<?php $arrWidgetAreaAtts['class']; ?> .widgetarea a:active, 
	#<?php $arrWidgetAreaAtts['class']; ?> .widgetarea a:focus {
		
		<?php if( !empty( $arrWidgetThemeMod['link_hover_color'] ) ) { ?>
		color: <?php echo cc2_maybe_add_hex( $arrWidgetThemeMod['link_hover_color'] ); ?>;
		<?php } ?>
		
		<?php if( !empty( $arrWidgetThemeMod['link_hover_background_color'] ) ) { ?>
		background-color: <?php echo cc2_maybe_add_hex( $arrWidgetThemeMod['link_hover_background_color'] ); ?>;
		<?php } ?>
	}
			<?php } ?>
	
		
		<?php } // endif !empty
	} ?>
    

	/** Top-link button */
	/*
	<?php
	
	
	// NOTE: false = empty ^_^
	
	if( !empty( $cc2_advanced_settings ) && !empty( $cc2_advanced_settings['load_scroll_top'] ) ) {
		$toplink_settings = get_theme_mod('bootstrap_scroll_top');
		//new __debug( $toplink_settings, 'top link button' );
		
		// link_color, hover_text_color, icon_size, font_size
		// maybe: height, position, position_offset, offset (= general offset)
		
		if( !empty( $toplink_settings['link_color'] ) ) {
			$arrTopLinkStyles[] = 	'.top-link-block a:link, .top-link-block a:visited {' .
				'color: #' . $toplink_settings['link_color'] .
			'}';
		}
		
		if( !empty( $toplink_settings['hover_text_color'] ) ) {
			$arrTopLinkStyles[] = 	'.top-link-block a:hover, .top-link-block a:active, .top-link-block a:focus  {' .
				'color: #' . $toplink_settings['hover_text_color'] .
			'}';
		}
		
		if( !empty( $toplink_settings['font_size'] ) ) {
			$arrTopLinkStyles[] = 	'.top-link-block {' .
				'font-size: ' . $toplink_settings['font_size'] .
			'}';
		}
		
		
		if( !empty( $toplink_settings['icon_size'] ) ) {
			$arrTopLinkStyles[] = 	'.top-link-block i.' . str_replace(' ', '.', $toplink_settings['button_icon_class'] ) . ' {' .
				'font-size: ' . $toplink_settings['icon_size'] .
			'}';
		}

		
		if( !empty( $arrTopLinkStyles ) ) {
			echo implode("\n", $arrTopLinkStyles );
		}
		
	} ?>
	*/

	</style>
<?php

}
