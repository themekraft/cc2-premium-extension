<?php
/**
 * Extended color scheme implementation
 *
 * @author Fabian Wolf
 * @package cc2pro
 * @since 2.0.1
 */


if( !defined('CC2_COLOR_SCHEMES_CLASS' ) ) {
	define( 'CC2_COLOR_SCHEMES_CLASS', 'cc2_ColorSchemes' );
}


class cc2_ColorSchemes {
	var $themePrefix = 'cc2_',
		$iCacheTimeout = 900, /* 60 * 15 */
		$arrKnownLocations = array(),
		$config = false; 
	
	
	/**
	 * @static
	 * @var    \wp_less Reusable object instance.
	 */
	protected static $instance = null;


	/**
	 * Creates a new instance. Called on 'after_setup_theme'.
	 * May be used to access class methods from outside. Both global AND once-instanced
	 *
	 * @see    __construct()
	 * @static
	 * @return \wp_less
	 */
	public static function init() {
		global $cc2_color_schemes;
		null === self::$instance AND self::$instance = new self;
		
		$cc2_color_schemes = self::$instance;
		
		
		return self::$instance;
	}
	
	function __construct() {
		$this->get_config();
		
		$upload_dir = wp_upload_dir();
		
		$this->arrKnownLocations = array(
			'default' => array( 
				'path' => get_template_directory() . '/includes/schemes/',
				'url' => get_template_directory_uri() . '/includes/schemes/',
			),
			
			'cc2_schemes' => array(
				'path' => $upload_dir['basedir'] . '/cc2/schemes/',
				'url' =>  $upload_dir['baseurl'] . '/cc2/schemes/',
			),
			'wp_less' => array(
				'path' => $upload_dir['basedir'] . '/wp-less/schemes/',
				'url' => $upload_dir['baseurl'] . '/wp-less/schemes/',
			),
			/*
			'theme' => array(
				'path' => trailingslashit( get_template_directory() ),
				'url' => trailingslashit( get_template_directory_uri() ),
			),*/
		);
		
		add_filter('cc2_switch_scheme_get_current_color_scheme', array( $this, 'add_missing_scheme_variables' ) );
		
		$this->init_schemes(); // uses $arrKnownLocations
		
		// check if there is a scheme set at all
		$this->set_default_scheme();
		
		// add filter and action hooks
		$this->init_hooks();
	}
	
	function init_schemes() {
		// populate color schemes
		$arrColorSchemes = $this->get_color_schemes();
		
		if( !empty( $arrColorSchemes ) ) {
			$this->arrColorSchemes = $arrColorSchemes;
		}
	}
	
	/**
	 * Mostly used as a failsafe.
	 * 
	 * NOTE: maybe some initialization error occured .. or an update from a beta to a stable theme version. You never know, so - this is the failsafe.
	 */
	
	function set_default_scheme() {
		$return = false;
		
		$current_scheme = get_theme_mod('color_scheme', false );
		
		if( empty( $current_scheme ) != false ) {
			$return = set_theme_mod( 'color_scheme', 'default');
		}
		
		return $return;
	}
	
	
	
	
	function get_config() {
		if( defined( 'CC2_THEME_CONFIG' ) ) {
			$config = maybe_unserialize( CC2_THEME_CONFIG );
			
			if( !empty( $config ) ) {
				$this->config = $config;
			}
		}
		
	}
	
	
	function get_location( $handle = 'default', $type = 'path' ) {
		$return = false;
		
		if( !empty( $handle ) && !empty( $this->arrKnownLocations[$handle] ) ) {
			switch( $type ) {
				case 'url':
					$return = $this->arrKnownLocations[$handle]['url'];	
					break;
				case 'path':
				default: // path
					$return = $this->arrKnownLocations[$handle]['path'];
					break;
			}
			
		}
		
		return $return;
	}
	
	public function set_known_locations( $arrLocations = array() ) {
		$return = false;
		
		if( !empty( $arrLocations ) && is_array( $arrLocations ) ) {
			$this->arrKnownLocations = apply_filters('cc2_color_schemes_set_locations', $arrLocations );
			
			$return = true;
		}
	
		return $return;
	}
	
	public function switch_color_scheme( $url ) {
		$return = $url;
		
		$current_scheme = apply_filters('cc2_switch_scheme_get_current_color_scheme', $this->get_current_color_scheme() );
		
		//new __debug( array('current_scheme' => $current_scheme ), 'switch_color_scheme' );
		
		//if( !empty( $current_scheme ) && isset( $current_scheme['slug'] ) && $current_scheme['slug'] != 'default' ) {
			//new __debug( array('current_scheme' => $current_scheme ), 'switch_color_scheme = true' );
		
		if( !empty( $current_scheme ) && isset( $current_scheme['slug'] ) && !empty( $current_scheme['style_url'] ) ) {
			$return = apply_filters('cc2_set_style_url', $current_scheme['style_url'] );
		}
		
		return $return;
	}
	
	function init_hooks() {
		add_filter('cc2_get_current_color_scheme', array( $this, 'filter_get_current_color_scheme' ) );
		
		add_action('cc_before_header', array( $this, 'action_color_schemes_debug' ) );
		add_filter( 'cc2_add_missing_scheme_variables', array( $this, 'add_missing_scheme_variables' ), 20 );
	}
	
	public function action_color_schemes_debug() {
		//$cc2_color_schemes = $GLOBALS['cc2_color_schemes'];
		
		if( isset($_POST['customized'] ) ) { // watching the Theme Customizer
			
			$customizer_vars = json_decode( stripcslashes( $_POST['customized'] ), true );
			//new __debug( $customizer_vars , '$customized' );
			
			$methodPost = $_POST; 
			unset( $methodPost['customized'] );
			
			//__debug::log( $customizer_vars, __METHOD__ . ': $_POST[customized]' );
			//__debug::log( $methodPost, __METHOD__ . ': $_POST (w/o $customized)' );
		}
		
		/*
		$current_scheme = $this->get_current_color_scheme();
		
		$less_vars = cc2pro_Less_Handler::get_theme_settings();
		
		new __debug( $current_scheme, 'current scheme' );
		new __debug( $cc2_color_schemes, __METHOD__ );
		new __debug( $less_vars, 'less_vars' );
		*/
		//__debug::log( $current_scheme, 'current scheme' );
		
	}
	
	
	public function filter_get_current_color_scheme( $color_schemes ) {
		$return = $color_schemes;
		
		$arrColorSchemes = $this->get_color_schemes(); // ... are already combined with the config schemes
		
		if( !empty( $arrColorSchemes ) ) {
			
			if( !empty( $color_schemes ) ) { // update already existing color schemes, if possible
				
				foreach( $color_schemes as $strSlug => $arrMeta ) {
					if( !empty( $arrColorSchemes[ $strSlug ] ) ) {
						$return[ $strSlug ] = $arrColorSchemes[ $slug ];
					}
				}
				
			} else {
				$return = $arrColorSchemes;
			}
		}
		
		return $return;
	}
	
	


	public function get_current_color_scheme( $default = false ) {
		$return = $default;
		$strDefaultOutputPath = '%cc2_schemes%';
		$strDefaultOutputURL = $strDefaultOutputPath;
		
		//new __debug('get_current_color_scheme fires', __CLASS__ );
		
		//$arrColorSchemes = apply_filters( 'cc2_get_available_color_schemes', $this->arrColorSchemes );
		$arrColorSchemes = apply_filters( 'cc2_get_available_color_schemes', $this->get_color_schemes() );
		
		$current_scheme_slug = get_theme_mod('color_scheme', $default );
		
		//new __debug( $current_scheme_slug, 'current_scheme_slug' );
		
		
		if( !empty( $current_scheme_slug ) && !empty( $arrColorSchemes ) && !empty( $arrColorSchemes[ $current_scheme_slug ] ) ) {
			
			$return = $arrColorSchemes[ $current_scheme_slug ];
			
			
			if( empty( $return['slug'] ) != false ) {
				$return['slug'] = $current_scheme_slug;
			}
			
			$return = $this->add_missing_scheme_variables( $return );
			//$return['function_call'] = __METHOD__;
			
			// check vars
			//$return = $this->add_current_vars( $return );
			
			
			

		}
		
		$return['current_color_scheme_parser'][] = 'cc2pro_ColorSchemes';
		
		return $return;
	}
	
	/*
	
	
	function _get_current_color_scheme( $default = false ) {
		$return = $default;
		
		$arrColorSchemes = apply_filters( 'cc2_get_available_color_schemes', $this->arrColorSchemes );
		
		$current_scheme_slug = get_theme_mod('color_scheme', $default );
		
		
		if( !empty( $current_scheme_slug ) && !empty( $arrColorSchemes ) && !empty( $arrColorSchemes[ $current_scheme_slug ] ) ) {
				
			$return = $arrColorSchemes[ $current_scheme_slug ];
			
			
			if( empty( $return['slug'] ) ) {
				$return['slug'] = $current_scheme_slug;
			}
		
			
			if( empty( $return['output_file'] ) != false ) {
				$strOutputFile = $current_scheme_slug . '.css';
				
				if( !empty( $return['file'] ) ) {
					$strOutputFile = basename( $return['file'], '.less' ) . '.css';
				}
				
				$return['output_file'] = $strOutputFile;
			}
			
			if( empty( $return['settings'] ) ) {
				$return['settings'] = '%default_settings%';
			}
			
			// check paths
			

			
			foreach( $this->arrKnownLocations as $strPath => $strURL ) { 
				
				if( file_exists( $strPath . $return['output_file'] ) ) {
					$return['style_path'] = $strPath . $return['output_file'];
					$return['style_url'] = $strURL . $return['output_file'];
					break;
				}
			}
			
			$return = apply_filters(' cc2_add_missing_scheme_variables', $return );
		}
		
		$return['current_color_scheme_parser'][] = 'cc2_ColorSchemes';
		
		return $return;
	}*/
	
	public function add_missing_scheme_variables( $color_scheme = array() ) {
		$return = $color_scheme;
		$current_scheme_slug = $return['slug'];
		
		$strDefaultOutputPath = '%cc2_schemes%';
		$strDefaultOutputURL = $strDefaultOutputPath;
		
		
		if( empty( $return['output_file'] ) != false ) {
			$strOutputFile = $current_scheme_slug . '.css';
			
			if( !empty( $return['file'] ) && strpos( $return['file'], '.less' ) !== false ) {
				$strOutputFile = basename( $return['file'], '.less' ) . '.css';
			}
			
			$return['output_file'] = $strOutputFile;
		}
		
		// where to write the $output_file to?
		if( empty( $return['output_dir'] ) != false ) {
			$return['output_dir'] = $strDefaultOutputPath;
		}
		
		// apply output path replacers
		$return['output_dir'] = cc2pro_Less_Handler::parse_placeholders( $return['output_dir'] );
		
		// add url
		if( empty( $return['output_url'] ) != false ) {
			$return['output_url'] = $strDefaultOutputURL;
		}
		
		// apply output url replacers
		$return['output_url'] = cc2pro_Less_Handler::parse_placeholders( $return['output_url'], true );
		
		// check default settings
		
		if( !empty( $return['settings'] ) ) {
			$return['default_settings'] = $this->get_scheme_default_settings( $current_scheme_slug );
		}
		
		// add main LESS variables to the default settings, too!
		if( !empty($color_scheme['scheme'] ) ) {
			foreach( $color_scheme as $strBaseSetting => $value ) {
				$strSetting = ( substr( $strBaseSetting, 0, 1 ) == '_' ? substr( $strBaseSetting, 1 ) : $strBaseSetting );
				
				$return['default_settings'][ $strSetting ] = $value;
			}
		}
		// check paths
		
		/**
		 * NOTE: A (do-)while-loop might be the better choice. Avoids nasty breaks.
		 */
		
		foreach( $this->arrKnownLocations as $strHandle => $arrPathMeta ) {


			// CSS path + url for dynamic mode (not yet in use)
			if( file_exists( $arrPathMeta['path'] . $return['output_file'] ) && empty( $return['style_path'] ) != false ) {
				$return['style_path'] = $arrPathMeta['path'] . $return['output_file'];
				$return['style_url'] = $arrPathMeta['url'] . $return['output_file'];
				break;
			}
			
		}
		
		$return['parser'] = 'cc2pro_ColorSchemes';
		
		
		return $return;
	}
	
	/**
	 * Fetch the default settings of the current scheme (mostly for the customizer API).
	 * 
	 * @param string $scheme_slug	Required.
	 * @return mixed $return		Returns FALSE if not found, missing scheme_slug or something else is broken. On success, returns the complete default settings data (array).
	 */
	
	function get_scheme_default_settings( $scheme_slug = false ) {
		$return = false;
		
		if( !empty( $scheme_slug ) ) {
		
			/**
			 * NOTE: AVOID using get_single_color_scheme, because that one relies on THIS current method as well!
			 */
			//$scheme = $this->get_single_color_scheme( $scheme_slug );
			if( !empty( $this->arrColorSchemes ) && !empty( $this->arrColorSchemes[ $scheme_slug ]['settings'] ) ) {
				$scheme = $this->arrColorSchemes[ $scheme_slug ];
				
				
				// theoretically, it could be the complete settings in an array instead
				if( is_array( $scheme['settings'] ) ) {
					$return = $scheme['settings'];
					
				} else {
					
					
					if( $scheme['settings'] != '%default_settings%' && file_exists( get_template_directory() . '/includes/schemes/' . $scheme['settings'] ) != false ) {
						//new __debug( $scheme_slug, __METHOD__ . ': fetching default settings for ' . $scheme_slug  );
						
						include( get_template_directory() . '/includes/schemes/' . $scheme['settings'] );
						if( !empty( $settings ) && is_array( $settings) != false ) {
							$return = $settings;
						}
						
						//new __debug( $settings, ': default settings for ' . $scheme_slug );
					
				
			
					
					} elseif( $scheme['settings'] == '%default_settings%') {
						if( !defined( 'CC2_DEFAULT_SETTINGS') ) {
							include( get_template_directory() . '/includes/default-settings.php' );
						}
						
						$default_settings = maybe_unserialize( CC2_DEFAULT_SETTINGS );
						
						$return = $default_settings['theme_mods'];
						
						unset( $return['color_scheme'] ); // avoid overwriting the color scheme when the settings are being used inside the customizer
					} else {
						if( file_exists( cc2pro_Less_Handler::parse_placeholders( $scheme['settings'] ) ) ) {
							include( cc2pro_Less_Handler::parse_placeholders( $scheme['settings'] ) );
								if( !empty( $settings ) && is_array( $settings) != false ) {
								$return = $settings;
							}
							
						}
						
					}
				}
			}
			
		}
		
		return $return;
	}
	
	
	/**
	 * FIXME: is_customizer_preview() does not seem to work reliable. AS THE BLOODY REST OF THAT UTTER FUCKING MESS called "Theme Customizer API".
	 */
	
	
	public function get_color_schemes( $include_settings = false ) {
		$return = false;
		$useCache = false;
		

		// get default themes
		if( !empty( $this->config ) && isset( $this->config['color_schemes']['default'] ) != false ) {
			$return = $this->config['color_schemes'];
			//new __debug( $return, 'default schemes' );
			
			// check for scheme paths
			foreach( $return as $strBaseSlug => $arrBaseMeta ) {
				
				/**
				 * FIXME: default input path is %default% = includes/schemes/
				 */
				$return[ $strBaseSlug ]['scheme_path'] = trailingslashit( $this->get_location() );
				
				//$return[ $strBaseSlug ]['scheme_path'] = trailingslashit( get_template_directory() );
				
				// check for input file
				if( empty( $arrBaseMeta['file'] ) ) { // default file = style.less
					$return[ $strBaseSlug ]['file'] = 'style.less';
				}
				
	
				
				if( !empty( $include_settings ) && !empty($return[ $strBaseSlug ]['settings'] ) ) {
					
					
					$return[ $strBaseSlug ]['default_settings'] = $this->get_scheme_default_settings( $strBaseSlug );
								
					// add main LESS variables to the default settings, too!
					if( !empty($return[ $strBaseSlug ]['scheme'] ) ) {
						
						
						foreach( $return[ $strBaseSlug ]['scheme'] as $strBaseSetting => $value ) {
							$strSchemeSetting = ( substr( $strBaseSetting, 0, 1 ) == '_' ? substr( $strBaseSetting, 1 ) : $strBaseSetting );
							
							$return[ $strBaseSlug ]['default_settings'][ $strSchemeSetting ] = $value;
						}
					}
					
					//new __debug( $return[ $strBaseSlug ]['default_settings'], __METHOD__ . ': default settings for ' . $strBaseSlug );
				}
				
				// set output files etc.
				//if( empty( 
				
				$return[$strBaseSlug]['_parser'] = 'plugin';
			}
			
			
		}
		
		// look up locations
		$arrFoundSchemes = $this->find_color_schemes();
		
		if( !empty( $arrFoundSchemes ) ) { // combine config + found color schemes
			foreach( $arrFoundSchemes as $strSlug => $arrMeta ) {

				$return[ $strSlug ] = $arrMeta;
				
				// copy handling: add source file (based on slug)
				
				// replace file with source file
				if( !empty( $arrMeta['file']['variation_of'] ) ) {
					$strCopySlug = $arrMeta['file']['variation_of'];
					$arrFileMeta = $arrMeta['file']; // safety first (+ compatiblity as well)
				
					
					if( !empty( $return[ $strCopySlug ] ) ) { // config-based (override)
						unset( $return[ $strSlug ]['file'] );
						
						$return[ $strSlug ]['file'] = $return[ $strCopySlug ]['file'];	
				
					} elseif( empty( $return[ $strCopySlug ] ) != false && !empty( $arrFoundSchemes[ $strCopySlug ] ) ) { // fs-based
						unset( $return[ $strSlug ]['file'] );
					
						$return[ $strSlug ]['file'] = $arrFoundSchemes[ $strCopySlug ]['file'];
					} 
					
					
					//$return[ $strSlug ]['source_file'] = $arrFoundSchemes[ $arrMeta['file']['variation_of'] ]['file']; // less file
					
					unset( $strCopySlug );
					unset( $arrFileMeta );
				}
				
				
				if( !empty( $include_settings ) && !empty($return[ $strSlug ]['settings'] ) ) {
					$return[ $strSlug ]['default_settings'] = $this->get_scheme_default_settings( $strSlug );
					
					// add main LESS variables to the default settings, too!
					if( !empty($arrMeta['scheme'] ) ) {
						foreach( $arrMeta as $strBaseSetting => $value ) {
							$strSetting = ( substr( $strBaseSetting, 0, 1 ) == '_' ? substr( $strBaseSetting, 1 ) : $strBaseSetting );
							
							$return[ $strSlug ]['default_settings'][ $strSetting ] = $value;
						}
					}
				}
			}
		}
		
		// look out for backed up scheme
		if( cc2_Helper::has_settings_backup( 'scheme' ) != false ) {
			$scheme_backup = cc2_Helper::get_settings_backup( 'scheme' );
			
			if( !empty( $scheme_backup ) && !empty( $scheme_backup['slug']) ) {
				$return[ $scheme_backup['slug'] ] = $scheme_backup;
			}
		}
	
		
		
		return $return;
	}
	
	/**
	 * Find color schemes
	 */
	 
	function find_color_schemes() {
		$return = false;
		
		if( !empty( $this->arrKnownLocations ) ) {
			
			foreach( $this->arrKnownLocations as $strHandle => $arrPathMeta ) {
				$arrSchemeFiles = array(); // soft reset
				
				if( file_exists( $arrPathMeta['path'] ) ) {
				
					$arrFiles = scandir( $arrPathMeta['path'] );
					
					//new __debug( $arrFiles, 'scandir: ' . $strPath );
					
					foreach( $arrFiles as $strFile ) {
						
						/**
						 *  fetch meta-data
							- theme description 
							- json-file: (scheme_name).scheme.json, (scheme_name).scheme
								- may contain custom variables
								- may contain 'variation_of' key; value = 'scheme_slug' => so for a copy, we do not need to duplicate the whole file
							- if custom variables, add section in customizer
						 * 
						 */
						$strFileExt = pathinfo( $strFile, PATHINFO_EXTENSION );
						
						if( !empty( $strFileExt ) && $strFile[0] != '.' && strpos( 'css||less||json||scheme', $strFileExt ) !== false && strpos( $strFile, '||') === false ) {
							
							$arrSchemeFiles[ $strFileExt ][] = $strFile;
							
						}
					}
					
					//new __debug( $arrSchemeFiles, 'find_color_schemes: found files' );
					
					// sort files
					
					// priorize scheme files (.scheme.json || .json) over theme description (.css) + less files
					if( !empty( $arrSchemeFiles['json'] ) ) {
					
						foreach( $arrSchemeFiles['json'] as $strSchemeFile ) {
							$data = ''; // soft reset
							$sanitized_data = '';
							$arrSchemeData = array();
							
							// fetch scheme file
							$data = file_get_contents( $arrPathMeta['path'] . $strSchemeFile );
							
							// correct common "mistakes" (ie. Javascript notation)
							$sanitized_data = str_replace( array("'"), array('"'), $data );
							
							$arrSchemeData = json_decode( $sanitized_data, true );
							
							// slug MUST be given to work
							if( !empty( $arrSchemeData['slug'] ) ) {
				
								$arrReturn[ $arrSchemeData['slug'] ] = $arrSchemeData; /** NOTE: will automatically overwrite any pre-existing slug */
								
								// add source path
								$arrReturn[ $arrSchemeData['slug'] ]['scheme_path'] = $arrPathMeta['path'];
							}
							
						}
					} // !empty( scheme_files )
					
				} // endif.path_exists
				
					
			} // endforeach
			
			if( !empty( $arrReturn ) ) {
				//new __debug( $arrReturn, 'find_color_schemes: return' );
				
				$return = $arrReturn;
			}
			
		}
	
		return $return;
	}
	
	function get_single_color_scheme( $slug = null ) {
		$return = false;
		
		if( !empty( $slug ) ) {
			$arrColorSchemes = $this->get_color_schemes();
			
			
			if( !empty( $arrColorSchemes ) && !empty( $arrColorSchemes[ $slug ]['scheme'] ) ) {
				$return = $arrColorSchemes[ $slug ];
				$return['slug'] = $slug;
			}
			
			//new __debug( array('colorSchemes' => $arrColorSchemes, 'return' => $return, 'slug' => $slug ), __METHOD__ . ': return' );
		}
		
		return $return;
	}
	
	
	/**
	 * Set cache to transient. Might add fs-based caching later.
	 * 
	 * @param array $color_schemes	Format identical to the CC2_THEME_CONFIG array 'color_schemes'.
	 * @return bool $status			Returns false if cache was not updated, or true, if it was.
	 */
	
	function update_cache( $arrSchemes = null ) {
		$return = false;
		
		if( !empty( $arrSchemes ) ) {
			$return = set_transient( $this->themePrefix . 'color_schemes_cache', $arrSchemes, $this->iCacheTimeout );
		}
		
		return $return;
	}
	
}


//add_action('cc2_init_color_schemes', array('cc2pro_ColorSchemes', 'init'), 10 );
add_action('cc2_init_color_schemes', array('cc2_ColorSchemes', 'init'), 11 );
//remove_action('cc2_init_color_schemes', array('cc2_ColorSchemes', 'init'), 11 );
