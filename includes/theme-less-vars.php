<?php
/**
 * Important theme mod variables for the LESS file compilation process
 * Separated for better maintenance and easier enhancement
 * 
 * @author Fabian Wolf
 * @since 2.0.1
 * @package cc2pro
 */

/**
 * syntax:  'theme_var' => array( 'less_var' => 'less-variable-name', 'filter' => 'maybe_hex' )
 * @param string $less_var		Name of the LESS variable.
 * @param mixed $default		Default value. Optional.
 * @param array $is_not			Check if the current theme_mod value is NOT one of these.
 * @param array $filter			Filter callbacks; false / empty for none. Defaults to array('maybe_hex')
 * 
 */

define('CC2_THEME_LESS_VARIABLES', serialize( 
	array(
		/**
		 * Color options
		 */
		
		'font_color' => array( 
			'less_var' => 'text-color'
		),
		'link_color' => array( 
			'less_var' => 'brand-primary'
		),
		'hover_color' => array( 
			'less_var' => 'link-hover-color'
		),
		
		/**
		 * Fonts-related options
		 */
		
		'font-family-sans-serif' => array( 
			'less_var' => 'font-family-sans-serif',
			'default' => 'Ubuntu, "Helvetica Neue", Helvetica, Arial, sans-serif',
			'filter' => false,
			'no_theme_mod' => true,
		),
		'font_family' => array(
			'less_var' => 'font-family-base',
			'is_not' => array('inherit'),
			'filter' => false,
		),
		

	)
) );
