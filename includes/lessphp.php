<?php
/**
 * Compiled into a class - mostly for better function call handling
 * 
 * @author Fabian Wolf
 * @since 2.0b1
 * @package cc2pro
 */
 
if( !class_exists( 'cc2_less_handler' ) ) :

	class cc2_less_handler {

		public static function add_css($vars = array(), $echo = true) {
			global $cc2_color_schemes;
			
			$return = '';
			$output_file = 'style.css';
			$less_file = 'style.less';

			$less = new lessc;
			$vars = self::cc2_less_vars($vars);
			
			//new __debug( $vars, 'set vars' );
			
			$less->setVariables( $vars );
			
			$less->setPreserveComments(true);
			$less->setFormatter('compressed');
			
			$upload_dir = wp_upload_dir();
			
			$strInputDir = trailingslashit( get_template_directory() );
			$strOutputURL = trailingslashit( $upload_dir['baseurl'] );
			$strOutputDir = trailingslashit( $upload_dir['basedir'] );
			
			
			// get current color scheme
			//$color_schemes = cc2pro_ColorSchemes::get_color_schemes();
			//$current_scheme = cc2_get_current_color_scheme();
			//$current_scheme = cc2pro_ColorSchemes::get_current_color_scheme
			$current_scheme = cc2_get_current_color_scheme();
			
			// input path
			if( !empty( $current_scheme['scheme_path'] ) ) {
				$strInputDir = $current_scheme['scheme_path'];
			}
			
			// input file
			if( !empty( $current_scheme['file'] ) ) {
				$less_file = $current_scheme['file'];
			}
			
			// output paths
			if( !empty( $current_scheme['output_dir'] ) ) {
				$strOutputDir = $current_scheme['output_dir'];
			}
			
			if( !empty( $current_scheme['output_url'] ) ) {
				$strOutputURL = $current_scheme['output_url'];
			}
			
			// output file
			if( !empty( $current_scheme['output_file'] ) ) {
				$output_file = $current_scheme['output_file'];
			}
			
				
			// return for the stylesheet url
			$return = $strOutputURL .  $output_file;
			
			// create output paths if possible
			if( !file_exists( $strOutputDir ) ) {
				mkdir( $strOutputDir, 0777, true ); // create output path, chmod it to 0777 and do recursive directory creation
			}
			
			
			// compile less to css
			$less->compileFile(  $strInputDir . $less_file , $strOutputDir . $output_file);

			if( !empty( $echo ) ) {
				echo $return;
				return;
			}
			return $return;
		}

		/**
		 * Pass variables into all .less files
		 * Incorporates the new global theme constant CC2_THEME_CONFIG for better color handling
		 * Uses CC2_THEME_CONFIG for properly determining the current theme_mod settings that do the color handling. Should make this more future-proof
		 * 
		 * @author Fabian Wolf
		 * @author Sven Lenhert
		 */

		public static function cc2_less_vars( $vars = array() ) {
			$return = $vars;

			if( !is_array( $vars ) ) {
				$return = array();
			}

			//new __debug( $vars, 'passed to cc2_less_vars' );

			 
			// check for global config (definition)
			if( defined('CC2_THEME_CONFIG' ) ) {
				$config = maybe_unserialize( CC2_THEME_CONFIG );
				$color_schemes = $config['color_schemes'];
			}

			/**
			 * font color
			 */
			 
			if( !isset( $return['text-color'] ) ) {
				$return[ 'text-color' ] = '@gray-dark'; // default
			
				if( get_theme_mod('font_color', false) != false ) {
					$font_color = get_theme_mod('font_color'); 
					
					$return[ 'text-color' ] = self::maybe_hex( $font_color );
				}
			}

			/**
			 * Font family
			 */
			$return[ 'font-family-sans-serif'] = 'Ubuntu, "Helvetica Neue", Helvetica, Arial, sans-serif';
			
			// default
			if( !isset( $return['font-family-base' ] ) ) {
				$return[ 'font-family-base' ] = $return[ 'font-family-sans-serif' ];
				
			}
			
			if( get_theme_mod('font_family', false) != false && get_theme_mod('font_family', 'inherit') != 'inherit' ) {
				$return[ 'font-family-base' ] = get_theme_mod('font_family');	
			}

			/**
			 * link color
			 */
			$link_color = ( !empty( $color_schemes['default']['scheme']['link_color'] ) ? $color_schemes['default']['scheme']['link_color'] : '#f2694b' ); // default
			
			if( get_theme_mod('link_color', false ) != false ) {
				$link_color = get_theme_mod('link_color');
			}
			
			if( !isset( $return['brand-primary']) ) {
			
				$return[ 'brand-primary' ] = self::maybe_hex( $link_color );
			}
			

			/**
			 * link hover color
			 */
			$link_hover_color = ( !empty( $color_schemes['default']['hover_color'] ) ? $color_schemes['default']['scheme']['link_color'] : '#f2854b' ); // default
			
			if( get_theme_mod('hover_color', false) != false ) {
				$link_hover_color = get_theme_mod('hover_color');
			}
			
			if( !isset( $return['link-hover-color'] ) ) {
			
				$return[ 'link-hover-color' ] = self::maybe_hex( $link_hover_color );
			}

			//new __debug( $return, 'less custom variables' );


			// return to human-kind
			return $return;
		}
		
		public static function maybe_hex( $value = '' ) {
			$return = $value;
			
			if( !empty( $value ) && strpos( '#', ''. $value ) === false && ctype_xdigit( $value ) != false ) {
				$return = '#' . $value;
			}
			
			return $return;
		}
	}

	add_filter( 'less_vars', array( 'cc2_less_handler', 'cc2_less_vars'), 10, 2 );
endif;


if ( ! class_exists( 'cc2_wp_less' ) ) {

	/**
	 * NOTE: Adds the parsed (!) data to the stylesheet URL
	 */
    add_filter('cc2_style_css', 'cc2_add_style_less');

    function cc2_add_style_less( $url ) {
		//global $cc2_color_schemes;
		$return = $url;
		
		$upload_dir = wp_upload_dir();
		
		// fetch current mode
		$cc2_tools_options = get_option('cc2_tools_options');
		$less_mode = 'dynamic';
		
		if(!isset($cc2_tools_options['mode']) || $cc2_tools_options['mode'] != 'dynamic') {
			$less_mode = 'static';
		}
		
		// get current color scheme
		//if( 
		
		$current_scheme = cc2_get_current_color_scheme();
		
		// mode switch
		switch( $less_mode ) {
			default:
			case 'dynamic':
				if( !file_exists( $current_scheme['output_dir'] . $current_scheme['output_file'] ) != false ) {
					// push a refresh
					$return = cc2_less_handler::add_css( false, true );
			
				}
				
				$return = $current_scheme['output_url'] . $current_scheme['output_file'];
				
			
				break;

			case 'static':
				if( file_exists( $current_scheme['output_dir'] . $current_scheme['output_file'] ) != false ) {
					$return = $current_scheme['output_url'] . $current_scheme['output_file'];
				} else { // copy file from scheme dir to output dir
					if( file_exists( $current_scheme['scheme_path'] . $current_scheme['output_file'] ) ) {
						$bResult = copy( $current_scheme['scheme_path'] . $current_scheme['output_file'], $current_scheme['output_dir'] . $current_scheme['output_file'] );
						if( !empty( $bResult ) ) {
							$return = $current_scheme['output_url'] . $current_scheme['output_file'];
						}
					}
				}
				break;
		}
		
		
		
		
		
		
		
		
		/*
		if( empty( $less_mode) || $less_mode == 'static' ) { // static mode = regular style.css
			if(is_file($upload_dir['basedir'] . '/style.css')) {
				$url = $upload_dir['baseurl'] . '/style.css';
			}
			
			$return = $url;
		} else {
			
			$strReturnURL = 
			$return = 
		}
		*/

        // add on init to support theme customiser in v3.4
        //return get_template_directory_uri() . '/style.less';
		return $return;

    }

    add_action( 'init', array( 'cc2_wp_less', 'instance' ) );

    /**
     * Enables the use of LESS in WordPress
     *
     * See README.md for usage information
     *
     * @author  Robert "sancho the fat" O'Rourke
     * @link    http://sanchothefat.com/
     * @package WP LESS
     * @license MIT
     * @version 2012-06-13.1701
     */
    class cc2_wp_less {
        /**
         * @static
         * @var    \wp_less Reusable object instance.
         */
        protected static $instance = null;


        /**
         * Creates a new instance. Called on 'after_setup_theme'.
         * May be used to access class methods from outside.
         *
         * @see    __construct()
         * @static
         * @return \wp_less
         */
        public static function instance() {

            $cc2_tools_options = get_option('cc2_tools_options');

            if(isset($cc2_tools_options['mode']) && $cc2_tools_options['mode'] == 'dynamic') {
                null === self :: $instance AND self :: $instance = new self;
                return self :: $instance;

            }


        }


        /**
         * @var array Array store of callable functions used to extend the parser
         */
        public $registered_functions = array();


        /**
         * @var array Array store of function names to be removed from the compiler class
         */
        public $unregistered_functions = array();


        /**
         * @var array Variables to be passed into the compiler
         */
        public $vars = array();


        /**
         * @var string Compression class to use
         */
        public $compression = 'compressed';


        /**
         * @var bool Whether to preserve comments when compiling
         */
        public $preserve_comments = true;


        /**
         * @var array Default import directory paths for lessc to scan
         */
        public $import_dirs = array();


        /**
         * Constructor
         */
        public function __construct() {

            // every CSS file URL gets passed through this filter
            add_filter( 'style_loader_src', array( $this, 'parse_stylesheet' ), 100000, 2 );

            // editor stylesheet URLs are concatenated and run through this filter
            add_filter( 'mce_css', array( $this, 'parse_editor_stylesheets' ), 100000 );

        }

        /**
         * Lessify the stylesheet and return the href of the compiled file
         *
         * @param  string $src    Source URL of the file to be parsed
         * @param  string $handle An identifier for the file used to create the file name in the cache
         * @return string         URL of the compiled stylesheet
         */
        public function parse_stylesheet( $src, $handle ) {

            // we only want to handle .less files
            if ( ! preg_match( '/\.less(\.php)?$/', preg_replace( '/\?.*$/', '', $src ) ) )
                return $src;

            // get file path from $src
            if ( ! strstr( $src, '?' ) ) $src .= '?'; // prevent non-existent index warning when using list() & explode()

            // Match the URL schemes between WP_CONTENT_URL and $src,
            // so the str_replace further down will work
            $src_scheme = parse_url( $src, PHP_URL_SCHEME );
            $wp_content_url_scheme = parse_url( WP_CONTENT_URL, PHP_URL_SCHEME );
            if ( $src_scheme != $wp_content_url_scheme )
                $src = set_url_scheme( $src, $wp_content_url_scheme );

            list( $less_path, $query_string ) = explode( '?', str_replace( WP_CONTENT_URL, WP_CONTENT_DIR, $src ) );

            // output css file name
            $css_path = trailingslashit( $this->get_cache_dir() ) . "{$handle}.css";

            // automatically regenerate files if source's modified time has changed or vars have changed
            try {

                // initialise the parser
                $less = new lessc;

                // load the cache
                $cache_path = "{$css_path}.cache";

                if ( file_exists( $cache_path ) )
                    $cache = unserialize( file_get_contents( $cache_path ) );

                // vars to pass into the compiler - default @themeurl var for image urls etc...
                $this->vars[ 'themeurl' ] = '~"' . get_stylesheet_directory_uri() . '"';
                $this->vars[ 'lessurl' ]  = '~"' . dirname( $src ) . '"';
                
                
                $this->vars = apply_filters( 'less_vars', $this->vars, $handle );

                // If the cache or root path in it are invalid then regenerate
                if ( empty( $cache ) || empty( $cache['less']['root'] ) || ! file_exists( $cache['less']['root'] ) )
                    $cache = array( 'vars' => $this->vars, 'less' => $less_path );

                // less config
                $less->setFormatter( apply_filters( 'less_compression', $this->compression ) );
                
                
                // has no actual function with oyejorge/less.php
                //$less->setPreserveComments( apply_filters( 'less_preserve_comments', $this->preserve_comments ) );
                $less->setVariables( $this->vars );

				//new __debug( $this->vars, 'actual customized less variables' );


                // add directories to scan for imports
                $import_dirs = apply_filters( 'less_import_dirs', $this->import_dirs );
                if ( ! empty( $import_dirs ) ) {
                    foreach( (array)$import_dirs as $dir )
                        $less->addImportDir( $dir );
                }

                // register and unregister functions
                foreach( $this->registered_functions as $name => $callable )
                    $less->registerFunction( $name, $callable );

                foreach( $this->unregistered_functions as $name )
                    $less->unregisterFunction( $name );

                // allow devs to mess around with the less object configuration
                do_action_ref_array( 'lessc', array( &$less ) );

				// $less->cachedCompile only checks for changed file modification times
				// if using the theme customiser (changed variables not files) then force a compile
				$force = false;
				
				//new __debug( array( 'current_vars' => $this->vars, 'cache_vars' => $cache['vars'] ), 'testing vars' );
				
				//var_dump( $this->vars !== $cache[ 'vars' ] );
				
				if ( $this->vars !== $cache[ 'vars' ] ) {
					$force = true;
				}
				$less_cache = $less->cachedCompile( $cache[ 'less' ], apply_filters( 'less_force_compile', $force ) );

				if ( empty( $cache ) || empty( $cache[ 'less' ][ 'updated' ] ) || $less_cache[ 'updated' ] > $cache[ 'less' ][ 'updated' ] || $this->vars !== $cache[ 'vars' ] ) {
					file_put_contents( $cache_path, serialize( array( 'vars' => $this->vars, 'less' => $less_cache ) ) );
					file_put_contents( $css_path, $less_cache[ 'compiled' ] );
				}
            } catch ( exception $ex ) {
                wp_die( $ex->getMessage() );
            }

            // restore query string it had if any
            $url = trailingslashit( $this->get_cache_dir( false ) ) . "{$handle}.css" . ( ! empty( $query_string ) ? "?{$query_string}" : '' );

            // restore original url scheme
            $url = set_url_scheme( $url, $src_scheme );

            return add_query_arg( 'ver', $less_cache[ 'updated' ], $url );
        }


        /**
         * Compile editor stylesheets registered via add_editor_style()
         *
         * @param  string $mce_css Comma separated list of CSS file URLs
         * @return string $mce_css New comma separated list of CSS file URLs
         */
        public function parse_editor_stylesheets( $mce_css ) {

            // extract CSS file URLs
            $style_sheets = explode( ",", $mce_css );

            if ( count( $style_sheets ) ) {
                $compiled_css = array();

                // loop through editor styles, any .less files will be compiled and the compiled URL returned
                foreach( $style_sheets as $style_sheet )
                    $compiled_css[] = $this->parse_stylesheet( $style_sheet, $this->url_to_handle( $style_sheet ) );

                $mce_css = implode( ",", $compiled_css );
            }

            // return new URLs
            return $mce_css;
        }


        /**
         * Get a nice handle to use for the compiled CSS file name
         *
         * @param  string $url File URL to generate a handle from
         * @return string $url Sanitized string to use for handle
         */
        public function url_to_handle( $url ) {

            $url = parse_url( $url );
            $url = str_replace( '.less', '', basename( $url[ 'path' ] ) );
            $url = str_replace( '/', '-', $url );

            return sanitize_key( $url );
        }


        /**
         * Get (and create if unavailable) the compiled CSS cache directory
         *
         * @param  bool   $path If true this method returns the cache's system path. Set to false to return the cache URL
         * @return string $dir  The system path or URL of the cache folder
         */
        public function get_cache_dir( $path = true ) {

            // get path and url info
            $upload_dir = wp_upload_dir();

            if ( $path ) {
                $dir = apply_filters( 'wp_less_cache_path', path_join( $upload_dir[ 'basedir' ], 'wp-less-cache' ) );
                // create folder if it doesn't exist yet
                wp_mkdir_p( $dir );
            } else {
                $dir = apply_filters( 'wp_less_cache_url', path_join( $upload_dir[ 'baseurl' ], 'wp-less-cache' ) );
            }

            return rtrim( $dir, '/' );
        }


        /**
         * Escape a string that has non alpha numeric characters variable for use within .less stylesheets
         *
         * @param  string $str The string to escape
         * @return string $str String ready for passing into the compiler
         */
        public function sanitize_string( $str ) {

            return '~"' . $str . '"';
        }


        /**
         * Adds an interface to register lessc functions. See the documentation
         * for details: http://leafo.net/lessphp/docs/#custom_functions
         *
         * @param  string $name     The name for function used in the less file eg. 'makebluer'
         * @param  string $callable (callback) Callable method or function that returns a lessc variable
         * @return void
         */
        public function register( $name, $callable ) {
            $this->registered_functions[ $name ] = $callable;
        }

        /**
         * Unregisters a function
         *
         * @param  string $name The function name to unregister
         * @return void
         */
        public function unregister( $name ) {
            $this->unregistered_functions[ $name ] = $name;
        }


        /**
         * Add less var prior to compiling
         *
         * @param  string $name  The variable name
         * @param  string $value The value for the variable as a string
         * @return void
         */
        public function add_var( $name, $value ) {
            if ( is_string( $name ) )
                $this->vars[ $name ] = $value;
        }

        /**
         * Removes a less var
         *
         * @param  string $name Name of the variable to remove
         * @return void
         */
        public function remove_var( $name ) {
            if ( isset( $this->vars[ $name ] ) )
                unset( $this->vars[ $name ] );
        }
    } // END class

    if ( ! function_exists( 'register_less_function' ) && ! function_exists( 'unregister_less_function' ) ) {
        /**
         * Register additional functions you can use in your less stylesheets. You have access
         * to the full WordPress API here so there's lots you could do.
         *
         * @param  string $name     The name of the function
         * @param  string $callable (callback) A callable method or function recognisable by call_user_func
         * @return void
         */
        function register_less_function( $name, $callable ) {
            $less = wp_less::instance();
            $less->register( $name, $callable );
        }

        /**
         * Remove any registered lessc functions
         *
         * @param  string $name The function name to remove
         * @return void
         */
        function unregister_less_function( $name ) {
            $less = wp_less::instance();
            $less->unregister( $name );
        }
    }

    if ( ! function_exists( 'add_less_var' ) && ! function_exists( 'remove_less_var' ) ) {
        /**
         * A simple method of adding less vars via a function call
         *
         * @param  string $name  The name of the function
         * @param  string $value A string that will converted to the appropriate variable type
         * @return void
         */
        function add_less_var( $name, $value ) {
            $less = wp_less::instance();
            $less->add_var( $name, $value );
        }

        /**
         * Remove less vars by array key
         *
         * @param  string $name The array key of the variable to remove
         * @return void
         */
        function remove_less_var( $name ) {
            $less = wp_less::instance();
            $less->remove_var( $name );
        }
    }

} // endif;
