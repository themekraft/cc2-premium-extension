<?php
/** 
 * cc2pro Settings Template: LESS mode
 */
?>
	<h3><?php _e('Select the LESS compiling mode' ); ?></h3>

	<div class="settings-box settings-field-mode-dynamic<?php echo ($cc2_tools_options['mode'] == 'dynamic' ? ' settings-field-active' : '' ); ?>">
		
		<h3><label><input type="radio" id="field-design-mode-dynamic" name="cc2_tools_options[mode]" value="dynamic" <?php checked($cc2_tools_options['mode'], 'dynamic'); ?> /> <?php _e('Design Mode'); ?></label></h3>

		<p>Use this if you are customizing your theme and want to preview your changes.</p>
		
		<p class="description">This mode will generate the style.css every time the LESS files are updated, or when making changes in the <a href="customize.php">Theme Customizer</a>.<br />
		We will cache all CSS in your uploads folder under <code>wp-less-cache</code>.</p>
	</div>


	<div class="settings-box settings-field-mode-static<?php echo ($cc2_tools_options['mode'] == 'static' ? ' settings-field-active' : '' ); ?>">

		<h3><label><input type="radio" id="field-design-mode-static" name="cc2_tools_options[mode]" value="static" <?php checked($cc2_tools_options['mode'], 'static'); ?> /> <?php _e('Production Mode'); ?></label></h3>

		<p>Use this when you are finished with customizing and ready to go live.</p>
		<p style="color: #f00; opacity: 0.7;">Great for high performance - but the customizer will not be able to make changes to the main color scheme!</p>
		<p class="description">This mode will generate a <strong>static style.css</strong> in your uploads folder and only regenerate it if you trigger it manually. After any change, you need to repeat that.</p>

		<hr />

		<p><em>&raquo; So, when customizing is finished, change to <strong>&quot;Production Mode&quot;</strong> and click &quot;Save Changes&quot;.</em></p>
		
	</div>

	
	
	<?php proper_submit_button( __('Save Changes'), 'primary large', 'submit', true, array('id' => 'cc2-tools-less-submit') ); ?>
