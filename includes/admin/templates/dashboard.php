<?php
/**
 * cc2pro Template: Dashboard
 *
 * @author Fabian Wolf
 * @package cc2pro
 * @since 0.1
 */

?>
<div class="cc2pro">

	<h1 class="welcome">Custom Community <em>Premium Pack</em></h1>

	<div class="three_col">

		<div class="col">
			<h2>Advanced Theme Options</h2>
			<p>You got more theme options added now. <br />
			Check them out in the Customizer.</p>

			<div class="cc2status">
                <div class="statusball green on"></div> <span class="cc2message">All set up!</span>
			</div>

			<p><a href="<?php echo get_admin_url(); ?>customize.php" title="Set me up!" class="button button-primary">Visit Customizer</a></p>

		</div>
		<div class="col">
			<h2>New Slideshow Features</h2>
			<p>Advanced post slider options.<br />
			Unlimited slideshows.</p>

			<div class="cc2status">
                <div class="statusball green on"></div> <span class="cc2message">All set up!</span>
			</div>

			<p><a href="<?php echo get_admin_url(); ?>admin.php?page=cc2-settings&tab=slider-options" class="button button-primary">Setup Slideshow</a></p>

		</div>
		<div class="col">
			<h2>Premium Support</h2>
			<p>Get personal help by the theme authors.<br />
			Any question left right now?</p>

			<div class="cc2status">
				<div class="statusball green on"></div> <span class="cc2message">All set up!</span>
			</div>

			<p><a href="<?php echo apply_filters('cc2_get_support_url', '#'); ?>" class="button button-primary link-themekraft-support">Write Us</a></p>

		</div>
		<div class="col">
			<h2>LessPHP Compiling</h2>
			<p>Smart compiling with LessPHP. <br>For reduced output and faster pageload.</p>

			<div class="cc2status">
				<div class="statusball green on"></div> <span class="cc2message">All set up!</span>
			</div>

			<p><a href="<?php echo get_admin_url(); ?>admin.php?page=cc2-tools&tab=less-compiler" class="button button-primary">LESS Compiling Settings</a></p>

		</div>

	</div>

	<div class="clear"></div>

	<hr />

<?php /*
	<div>

		<div>
			<div class="dummy dummy-left"></div>
			<h1>Featured Extensions</h1>
			<p class="lead light">Well-selected free and premium extensions for CC2. </p>
			<p class="lead"><a href="#" class="button button-primary">Browse Extensions</a></p>
		</div>

		<div class="clear"></div>

		<hr />

		<div class="clear"></div>
		<br /><br />

	</div>*/ ?>
	
</div><!-- /.cc2pro -->

