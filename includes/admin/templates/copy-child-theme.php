<?php
/**
  * cc2pro Template: Copy child theme
  */
?>

    <p>This will copy the child theme from the premium pack plugin to your theme folder.</p>
    
    <p><a href="#" id="copy_child_theme" class="button-primary button">Copy Child Theme Now</a></p>
  
    <h3>How to start:</h3>
    
    <p><strong>Step 1</strong><br />After creating your child theme you need to go to your theme folder and rename the theme.</p>
  
    <p><strong>Step 2</strong><br />Make sure you rename the folder and the theme name in the style.css too.</p>
  
    <p><strong>Step 3</strong><br />And then just activate your new child theme!</p>
  
    <p class="description">Note: every time you click on create child theme the theme "change-my-name" will be overwritten.</p>
  
    <hr />
  
    <h3>Hey, never change theme files directly!</h3>
  
    <p>Try to do all you can with options and CSS first.</p>
  
    <p>If you need to change more, consider to create a child theme.</p>

    <p>All your changes in the theme files would be lost with the next update.</p>
