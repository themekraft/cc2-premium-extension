<?php
/** 
 * cc2pro Settings Template: Change LESS compilation settings
 */
?>

	<div class="section-box less-compilation-settings">
		<h3>Fine-tune compilation settings</h3>
		

		<p><label><?php _e('Output directory:', 'cc2pro'); ?> <input type="text" class="<?php echo ( strlen( $cc2_tools_options['output_path'] ) > 50  ? 'large' : 'regular' ); ?>-text" size="80" name="cc2_tools_options[output_path]" value="<?php _notempty( $cc2_tools_options['output_path'] ); ?>" /> <span class="description"><?php printf( __('Defaults to <code>%s</code>.'), $default_output_path ); ?></span></label></p>
		
		<p><label><?php _e('Output file name:', 'cc2pro'); ?> <input type="text" class="regular-text" name="cc2_tools_options[output_file]" value="<?php _notempty( $cc2_tools_options['output_file'] ); ?>" /> <span class="description"><?php printf( __('Defaults to <code>%s</code>.'), $default_output_file ); ?></span></label></p>
		
		
	</div>
