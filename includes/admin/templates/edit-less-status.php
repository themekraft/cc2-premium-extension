<?php
/**
 * cc2pro Template: Edit LESS status (less_disabled)
 */
 
?>
	<h3>Ready for going live?</h3>

	<p>Do this if you have your design finished to save memory, or if you have memory problems.</p>

	<p>Turn compiling off: <input type="checkbox" <?php checked($less_disabled, true ) ?> name="cc2_tools_options[less_disabled]" /></p>

	<hr />
