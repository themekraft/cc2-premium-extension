<?php
/**
 * cc2pro Template: Tools overview
 */
 
?>

<div class="cc2pro">

    <h1 class="welcome">Custom Community <em>Tools</em></h1>
    <?php
    $active_tab = 'less-compiler';
    if( isset( $_GET[ 'tab' ] ) ) {
        $active_tab = $_GET[ 'tab' ];
    } // end if
    ?>

    <div class="cc2-support-wrap message updated" style="padding: 10px 2% 12px 2%; overflow:auto; float: none; clear: both; width: 96%; max-width: 96%;">

        <div id="cc2_documentation" style="float: left; overflow: auto; border-right: 1px solid #ddd; padding: 0 20px 0 0;">
            <h3><span><?php echo apply_filters( 'cc2_support_add_title', __('Get Support.', 'cc2pro') ); ?></span></h3>
            <p><?php do_action( 'cc2_support_add' ); ?> <a href="https://themekraft.zendesk.com/hc/en-us/categories/200037911-Custom-Community-2" class="button secondary" target="_new" title="Custom Community 2 Documentation">Documentation <small><i style="color: #999;">following!</i></small></a></p>
        </div>

        <div id="cc2_ideas" style="float: left; overflow: auto; padding: 0 20px 0 20px; border-right: 0px solid #ddd;">
            <h3>Contribute Your Ideas.</h3>
            
            <p><a class="button button-secondary" href="https://themekraft.zendesk.com/hc/communities/public/topics/200001362-Custom-Community-2-0-Ideas" target="_new">Visit Ideas Forums</a></p>
        </div>

        <div id="cc2_forums" class="wpforums" style="display: none; float: left; overflow: auto; padding: 0 20px 0 20px;">
            <h3>Learn, Share, Discuss.</h3>
            <p><a class="button button-secondary" href="http://wordpress.org/support/theme/custom-community" target="_new">Visit WordPress Forum</a></p>
        </div>

    </div>

	<?php 
	/*
	$admin_file = 'admin.php';
	$admin_page = 'cc2-tools';
	$setting_sections = $this->arrSections;*/
	
	include( get_template_directory() . '/includes/admin/templates/part-tab-switch.php' ); ?>

    <h2 class="nav-tab-wrapper">
        <a href="?page=cc2-tools&tab=less-compiler" class="nav-tab<?php echo ( $active_tab == 'less-compiler' ? ' nav-tab-active' : '' ); ?>">Compiling Settings</a>
     <!--   <a href="?page=cc2-tools&tab=child-theme" class="nav-tab <?php /*echo $active_tab == 'child-theme' ? 'nav-tab-active' : ''; */?>">Create Child Theme</a>-->
    </h2>


    <form method="post" action="options.php">
        <?php
		if( $active_tab == 'less-compiler' ) {
			wp_nonce_field( 'update-options' );
			settings_fields( 'cc2_tools_options' );
			do_settings_sections( 'cc2_tools_options' );

		} elseif( $active_tab == 'scheme-generator' ) {
			
			

		} else {
			wp_nonce_field( 'update-options' );
			settings_fields( 'cc2_child_theme_options' );
			do_settings_sections( 'cc2_child_theme_options' );
		} // end if/else
        ?>
    </form>

</div>
