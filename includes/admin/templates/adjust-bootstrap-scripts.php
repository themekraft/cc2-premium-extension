<?php
/**
 * cc2pro Template: Adjust which Bootstrap scripts are being loaded
 * @author Fabian Wolf
 * @package cc2pro
 * @since 2.0.1
 */

$strAddMessage = ( !empty( $message ) ? $message : '' );
?>
	<p><?php printf( __('Select which Bootstrap Javascript files shall actually be loaded. Useful if you don\t need them all and want to reduce overhead.') );
	if( !empty( $strAddMessage ) ) {
		echo nl2br( ' '. $strAddMessage );
	}
	?></p>

	
<?php if( !empty( $arrBootstrapScripts) ) { ?>
	<ul>
	<?php foreach( $arrBootstrapScripts as $strScriptHandle => $strDescription ) { ?>
		<li><label><input type="checkbox" name="cc2_bootstrap_load_script[]" value="<?php echo $strScriptHandle; ?>"<?php 
		if( in_array( $strScriptHandle, $advanced_settings[ $strFieldName ] ) != false ) {
			echo ' checked="checked"';
		}
		?>/> <?php echo $strDescription; ?></label></li>
	<?php } ?>
	</ul>
<?php } ?>

