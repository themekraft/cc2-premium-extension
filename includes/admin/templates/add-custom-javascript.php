<?php
/**
 * cc2 Template: Edit custom CSS
 * @author Fabian Wolf
 * @package cc2
 * @since 2.0
 */
$strFieldName = 'custom_script_' . $position;

$strAddMessage = ( !empty( $message ) ? $message : '' );
?>
	<p><?php printf( __('Add, edit or remove custom Javascript to the <strong>%s</strong>. Will be added <strong>after</strong> just about everything else.'), $position);
	if( !empty( $strAddMessage ) ) {
		echo nl2br( ' '. $strAddMessage );
	}
	?></p>

	<p><textarea name="<?php echo $strFieldName; ?>" id="edit-custom-script-<?php echo $position; ?>" rows="10" cols="30" class="large-text"><?php if( !empty( $advanced_settings[ $strFieldName ] ) ) {
		echo stripslashes_deep( $advanced_settings[ $strFieldName ] );
	} ?></textarea></p>

