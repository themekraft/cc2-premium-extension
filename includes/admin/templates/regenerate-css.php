<?php
/**
 * cc2pro Template: Regenerate style.css 
 */
?>
	<h3>Regenerate CSS</h3>
	
	<p><?php _e('This will create your setup as a static style.css in your uploads folder.', 'cc2pro'); ?><br />
	<span class="description"><?php _e('(We use the uploads folder to have your custom CSS file update save)', 'cc2pro'); ?></span></p>
	
	<?php proper_submit_button( sprintf( __('Create new %s now'), $cc2_tools_options['output_file'] ), 'delete', 'regenerate-css', true, array('id' => 'cc2-regenerate-css', 'name' => 'tools_action', 'value' => 'regenerate_css') ); ?>
