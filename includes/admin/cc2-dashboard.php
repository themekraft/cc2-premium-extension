<?php
if( !class_exists('__debug') ) {
	@include_once( CC_PLUGIN_PATH . '/includes/debug.class.php' );
}

if( !defined('CC2_THEME_CONFIG' ) ) {
	if( file_exists( get_template_directory() . '/includes/theme-config.php') ) {
		include_once( get_template_directory() . '/includes/theme-config.php' );
	}
}


/**
 * @hook after_setup_theme - is _REQUIRED_ to properly load the plugin right AFTER the theme and thus being able to overwrite / remove existing action hooks and functions set by the theme.
 */

//add_action( 'after_setup_theme', array( 'cc2_Admin_CustomStyling', 'get_instance'), 12 );
add_action( 'after_setup_theme', array( 'cc2pro_Admin', 'get_instance'), 11 );

//add_action( 'plugins_loaded', array( 'cc2pro_Admin', 'get_instance'), 15 );


class cc2pro_Admin {

	var $classPrefix = 'cc2_',
		$pluginPrefix = 'cc2_pro_',
		$arrSections = array(
			'getting-started' => array(
				'title' => 'Getting Started',
				'settings_slug' => 'cc2_options',
			),
			'slider-options' => array(
				'title' => 'Slideshow',
				'settings_slug' => 'cc2_slider_options',
			),
			'custom-styling' => array(
				'title' => 'Custom Styling',
				'settings_slug' => 'cc2_custom_styling_options',
			),
		),
		$arrKnownProSuffixes = array(
			'cc2-settings',
			'cc2-premium',
			'cc2-tools',
		);
	
	
	/**
	 * Plugin instance.
	 *
	 * @see get_instance()
	 * @type object
	 */
	protected static $instance = NULL;
		
	/**
	 * Implements Factory pattern
	 * Strongly inspired by WP Maintenance Mode of Frank Bueltge ^_^ (@link https://github.com/bueltge/WP-Maintenance-Mode)
	 * 
	 * Access this plugins working instance
	 *
	 * @wp-hook after_setup_theme
	 * @return object of this class
	 */
	public static function get_instance() {

		NULL === self::$instance and self::$instance = new self;

		return self::$instance;
	}
	
	function __construct() {
		// avoid future fuck-ups
		if( defined('CC2_THEME_CONFIG' ) ) {
			$config = maybe_unserialize( CC2_THEME_CONFIG );
			if( isset( $config['admin_sections'] ) ) {
				$this->arrSections = $config['admin_sections'];
			}
		}
		
		// filter
		add_filter('cc2_tab_admin_url', array( $this, 'filter_tab_admin_url' ) );
		add_filter('cc2_admin_settings_url', array( $this, 'filter_admin_settings_admin_url' ) );
		
		// register admin settings
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );

		// Adding the CC2 Premium Admin Styles
		add_action( 'admin_enqueue_scripts', array( $this, 'init_admin_js'), 10 );
		add_action( 'admin_enqueue_scripts', array( $this, 'init_admin_styles' ), 10 );
		add_action( 'admin_enqueue_scripts', array( $this, 'load_admin_js'), 11, 1);
		
		//new __debug('cc2 premium extension loaded');
		
	}

	function init_admin_js($hook) {

	   // if( 'cc2-premium_page_cc2-tools' != $hook )
		 //   return;
		 
		
		 
		wp_register_script( $this->pluginPrefix . 'admin-js', plugins_url( 'js/admin.js', __FILE__ ), array('jquery', 'cc-admin-js') );
		
	}
	
	function load_admin_js( $page_suffix = null ) {
		//new __debug( $page_suffix, 'cc2pro: load_admin_js - suffix' );
		
		if( is_cc2_admin_page( $page_suffix ) != false ) {
		//wp_enqueue_script( $this->pluginPrefix . 'css-generator' );
			//new __debug( $page_suffix, 'cc2pro: load_admin_js - suffix' );
			wp_enqueue_script( $this->pluginPrefix . 'admin-js' );
		}
		
		// try to fix the wp.svgpainter random appearing error
		wp_enqueue_script( 'svg-painter' );
		
	}

	

	function init_admin_styles($hook) {

		/*
		if( 'toplevel_page_cc2-premium' != $hook )
		return;*/

		// load the admin.css
		wp_register_style( $this->pluginPrefix . 'admin-css', plugins_url( 'css/admin.css', __FILE__ ) );
		wp_enqueue_style( $this->pluginPrefix . 'admin-css' );

	}
	
	
	/**
	 * Helper function
	 */
	
	/**
	 * Adding the CC2 Premium Admin Area
	 *
	 * @author Konrad Sroka
	 * @package cc2_pro
	 * @since 0.0.1
	 */


	function add_admin_menu() {
		remove_submenu_page('themes.php','cc2-settings');
		$strParentMenuSlug = 'cc2-premium';
		
		add_menu_page( 'Custom Community 2 Premium Dashboard', 'CC Premium', 'manage_options', $strParentMenuSlug, array( $this, 'admin_page_screen') );
		
		/**
		 * TODO: Add check to theme to add submenu_page if premium extension is set
		 * add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function 
		 */
		add_submenu_page( $strParentMenuSlug, 'CC Settings', 'CC Settings', 'manage_options', 'cc2-settings', array( $this, 'admin_page_settings') );
		add_submenu_page( $strParentMenuSlug ,'CC Tools','CC Tools','edit_theme_options','cc2-tools', 'cc2_pro_screen_tools' );


	}

	function admin_page_settings() {
		// prepare variables
		$setting_sections = $this->arrSections;
		$current_tab = 'getting-started';
			
		if( !empty( $_GET['tab'] ) && isset( $setting_sections[$_GET['tab']] ) ) {
			$current_tab = $_GET['tab'];
		}
		
		
		//new __debug('cc2pro: page settings fire');
		
		// fetch concerned tab / template
		include_once( get_template_directory() . '/includes/admin/templates/dashboard.php' );
		
		
	}
	
	function admin_page_screen() {
		global $pagenow, $wp_version, $page, $plugin_page;
		
		//new __debug( array('pagenow' => $pagenow, 'plugin_page' => $plugin_page, '$wp_version' => $wp_version), 'a few globals' );
		
		require_once( plugin_dir_path(__FILE__) . '/templates/dashboard.php');
		
	}
	
	/**
	 * Hooks into the cc2_admin_url filter and replaces themes.php with admin.php, because the plugin menu docks at some different place than the regular theme settings
	 */
	function filter_themes_admin_url( $url ) {
		$return = $url;
		
		if( strpos( $return, 'themes.php') !== false ) {
			$return = str_replace( 'themes.php', 'admin.php', $return );
		}
		
		return $return;
	}
	
	/**
	 * Simple wrappers per filter hook (just in case we want to change this in future ^_^)
	 */
	 
	function filter_tab_admin_url( $url ) {
		return $this->filter_themes_admin_url( $url );
	}
	
	function filter_admin_settings_admin_url( $url ) {
		return $this->filter_themes_admin_url( $url );
	}
}

