<?php
//if( !class_exists( 'cc2_Pasteur


/**
 * Registering the Premium Options for the WordPress Customizer
 *
 * @author Konrad Sroka
 * @package cc2_pro
 * @since 2.0
 */

function cc2pro_customizer_support( $wp_customize ) {

    // Loading global arrays
    global $cc2_text_align;
    global $cc2_font_family;
    global $cc2_h_positions;
    global $cc2_animatecss_start_moves;
   

	// check on the font family situation
	if( function_exists('cc2_customizer_load_fonts' ) ) {
		$cc2_font_family = apply_filters('cc2_premium_load_font_family', cc2_customizer_load_fonts() );
	}

	// fetch advanced settings
	$advanced_settings = get_option('cc2_advanced_settings', false );
	
	// fetch tools settings
	$tools_settings = get_option('cc2_tools_options', false );

	if( !empty( $tools_settings['mode'] ) ) {
		$less_mode = $tools_settings['mode'];
	}

	$theme_customizer_priorities = cc2_CustomizerLoader::get_section_priorities();

    // Now creating new sections and options.
    // Lets do it.





    // Site Title & Tagline



    // Header Text Hover Color (=Site Title)
//    $wp_customize->add_setting( 'header_text_hover_color', array(
//        'default'           	=> '',
//        'capability'        	=> 'edit_theme_options',
//        'transport'   			=> 'refresh',
//        'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
//        'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
//    ) );
//    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'header_text_hover_color', array(
//        'label'    				=> __('Header Text Hover Color', 'cc2'),
//        'section'  				=> 'title_tagline',
//        'priority'				=> 160,
//    ) ) );

	// Colors / Color Scheme
	//$color_scheme_section_priority = cc2_CustomizerLoader::get_customizer_section_priority('section_color_schemes');
	if( !empty( $theme_customizer_priorities['section_color_schemes'] ) ) {
		$color_scheme_section_priority = $theme_customizer_priorities['section_color_schemes'];
	}
	
	
	if( empty( $color_scheme_section_priority ) != false ) {
		$color_scheme_section_priority = 41;
		//__debug::log('color_scheme_section_priority is empty', __METHOD__);
	}	 else {
		//__debug::log( $color_scheme_section_priority, __METHOD__ . ': color_scheme_section_priority');
	}
	
	// LESS Mode Notice
	if( !empty( $less_mode ) && $less_mode != 'dynamic' ) {
	
		 $wp_customize->add_setting( 'alert_less_mode', array(
			 'capability'   => 'edit_theme_options',
		 ) );
		 
		$wp_customize->add_control( 
			new Description( $wp_customize, 'alert_less_mode', array(
				'label' 		=> 	sprintf( __('LESS compiler mode is currently set to &quot;%1$s&quot;. Switch to &quot;dynamic&quot; (via the <a href="%2$s">CC2 Tools settings section</a>) to activate the automated stylesheet compilation.', 'cc2pro'), $less_mode, get_admin_url() . 'admin.php?page=cc2-tools' ),
				'type' 			=> 	'description',
				'section' 		=> 	'colors',
				'class'			=> 'alert-message',
				'priority'		=> 	$color_scheme_section_priority-5,
				
			) 
		) );
		
	}
	
	
	    // Font Color
    $wp_customize->add_setting( 'font_color', array(
        'default'           	=> '#666666',
        'capability'        	=> 'edit_theme_options',
        'transport'   			=> 'refresh', /** ajax-refresh! */
        'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
        'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'font_color', array(
        'label'    				=> __('Font Color', 'cc2'),
        'section'  				=> 'colors',
        'priority'				=> $color_scheme_section_priority+=10,
    ) ) );

    // Link Color
    $wp_customize->add_setting('link_color', array(
        'default'           	=> '#f2694b',
        'capability'        	=> 'edit_theme_options',
        'transport'   			=> 'refresh',
        'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
        'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'link_color', array(
        'label'    				=> __('Link Color', 'cc2'),
        'section'  				=> 'colors',
        'priority'				=> $color_scheme_section_priority+=10,
    ) ) );

    // Hover Color
    $wp_customize->add_setting('hover_color', array(
        'default'           	=> '#f2854b',
        'capability'        	=> 'edit_theme_options',
        'transport'   			=> 'refresh',
        'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
        'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'hover_color', array(
        'label'    				=> __('Hover Color', 'cc2'),
        'section'  				=> 'colors',
        'priority'				=> $color_scheme_section_priority+=10,
    ) ) );






	// Navigation
	$nav_section_priority = 160;
	
	 // Headline Font Family
		$wp_customize->add_setting( 'top_nav_font_family', array(
		'default'       => 	'inherit',
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'postMessage',
		) );
		$wp_customize->add_control( 'top_nav_font_family', array(
		'label'   		=> 	__('Top Navigation Font Family', 'cc2'),
		'section' 		=> 	'nav',
		'priority'		=> 	$nav_section_priority,
		'type'    		=> 	'select',
		'choices'    	=> 	$cc2_font_family
		) );
		$nav_section_priority++;

		// Headline Font Family
		$wp_customize->add_setting( 'top_nav_font_size', array(
		'default'       => 	'18px',
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control( 'top_nav_font_size', array(
		'label'   		=> 	__('Top Navigation Font Size', 'cc2'),
		'section' 		=> 	'nav',
		'priority'		=> 	$nav_section_priority,
		) );
		$nav_section_priority++;

      
	
	$nav_section_priority = 173; // taken from customizer-options.php

	// top nav hover background color
		$wp_customize->add_setting('top_nav_hover_background_color', array(
		'default'           	=> '#F2854B',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'top_nav_hover_background_color', array(
		'label'    				=> __('Top Nav Hover Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		
		// top nav active background color 
		$wp_customize->add_setting('top_nav_active_background_color', array(
		'default'           	=> '#F2694B',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'top_nav_active_background_color', array(
		'label'    				=> __('Top Nav Active Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		// top nav active text color 
		$wp_customize->add_setting('top_nav_active_text_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'top_nav_active_text_color', array(
		'label'    				=> __('Top Nav Active Font Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
	// Navigation: Dropdown
		
		// top nav dropdown background color 
		$wp_customize->add_setting('top_nav_dropdown_background_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'top_nav_dropdown_background_color', array(
		'label'    				=> __('Top Nav Dropdown Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		// top nav active text color 
		$wp_customize->add_setting('top_nav_dropdown_text_color', array(
		'default'           	=> '#F2694B',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'top_nav_dropdown_text_color', array(
		'label'    				=> __('Top Nav Dropdown Font Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		
		// top nav dropdown HOVER / ACTIVE background color 
		$wp_customize->add_setting('top_nav_dropdown_hover_background_color', array(
		'default'           	=> '#F2854B',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'top_nav_dropdown_hover_background_color', array(
		'label'    				=> __('Top Nav Dropdown Hover &amp; Active Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		// top nav active text color 
		$wp_customize->add_setting('top_nav_dropdown_hover_text_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'top_nav_dropdown_hover_text_color', array(
		'label'    				=> __('Top Nav Dropdown Hover &amp; Active Font Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority+=10;
		

	// Navigation: Secondary Nav
	// top nav hover background color
		
	
		$wp_customize->add_setting('secondary_nav_hover_background_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'secondary_nav_hover_background_color', array(
		'label'    				=> __('Secondary Nav Hover Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		
		// secondary nav active background color 
		$wp_customize->add_setting('secondary_nav_active_background_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'secondary_nav_active_background_color', array(
		'label'    				=> __('Secondary Nav Active Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		// secondary nav active text color 
		$wp_customize->add_setting('secondary_nav_active_text_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'secondary_nav_active_text_color', array(
		'label'    				=> __('Secondary Nav Active Font Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
	// Navigation: Dropdown
		
		 
		// secondary nav dropdown background color 
		$wp_customize->add_setting('secondary_nav_dropdown_background_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'secondary_nav_dropdown_background_color', array(
		'label'    				=> __('Secondary Nav Dropdown Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		// secondary nav active text color 
		$wp_customize->add_setting('secondary_nav_dropdown_text_color', array(
		'default'           	=> '#F2694B',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'secondary_nav_dropdown_text_color', array(
		'label'    				=> __('Secondary Nav Dropdown Font Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		
		// secondary nav dropdown HOVER / ACTIVE background color 
		$wp_customize->add_setting('secondary_nav_dropdown_hover_background_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'secondary_nav_dropdown_hover_background_color', array(
		'label'    				=> __('Secondary Nav Dropdown Hover &amp; Active Background Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		
		$nav_section_priority++;
		
		// secondary nav active text color 
		$wp_customize->add_setting('secondary_nav_dropdown_hover_text_color', array(
		'default'           	=> '#fff',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'secondary_nav_dropdown_hover_text_color', array(
		'label'    				=> __('Secondary Nav Dropdown Hover &amp; Active Font Color', 'cc2'),
		'section'  				=> 'nav',
		'priority'				=> $nav_section_priority,
		) ) );
		



    // Typography


    // Font Family
    $wp_customize->add_setting( 'font_family', array(
        'default'       => 	'Lato',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'postMessage',
    ) );
    $wp_customize->add_control( 'font_family', array(
        'label'   		=> 	__('Font Family', 'cc2'),
        'section' 		=> 	'typography',
        'priority'		=> 	21,
        'type'    		=> 	'select',
        'choices'    	=> 	$cc2_font_family
    ) );

    // TK Google Fonts Ready! - A Quick Note
    $wp_customize->add_setting( 'google_fonts_note', array(
        'capability'    => 	'edit_theme_options',
    ) );
    $wp_customize->add_control( new Description( $wp_customize, 'google_fonts_note', array(
        'label' 		=> 	__('Add Google Fonts and make them available in the theme options with <a href="http://themekraft.com/store/tk-google-fonts-wordpress-plugin/" target="_blank">TK Google Fonts</a>.', 'cc2'),
        'type' 			=> 	'description',
        'section' 		=> 	'typography',
        'priority'		=> 	31,
    ) ) );


    // Link Underline
    $wp_customize->add_setting( 'link_underline', array(
        'default'       => 	'just for mouse over',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'postMessage',
    ) );
    $wp_customize->add_control( 'link_underline', array(
        'label'   		=> 	__('Underline Links', 'cc2'),
        'section' 		=> 	'typography',
        'priority'		=> 	101,
        'type'    		=> 	'radio',
        'choices'    	=> 	array(
            'just for mouse over' 	=> 'just for mouse over',
            'never' 				=> 'never',
            'just when normal' 		=> 'just when normal',
            'always' 				=> 'always'
        ),
    ) );







    // Widgets


    // Widgets Section
   
   /*
    $wp_customize->add_section( 'widgets', array(
        'title'         => 	'Widgets',
        'priority'      => 	140,
    ) );
    
    */
    
    // remove the notices
    
    $wp_customize->remove_setting('widget_title_attributes_note');
    $wp_customize->remove_control('widget_title_attributes_note');
    

    // Widget Title Font Family
    $wp_customize->add_setting( 'widget_title_font_family', array(
        'default'       => 	'Pacifico',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widget_title_font_family', array(
        'label'   		=> 	__('Widget Title Font Family', 'cc2'),
        'section' 		=> 	'widgets',
        'priority'		=> 	141,
        'type'    		=> 	'select',
        'choices'    	=> 	$cc2_font_family
    ) );

    // Widget Title Background Color
    /*
    $wp_customize->add_setting( 'widget_title_background_color', array(
        'default'       => 	'',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
        'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
        'sanitize_js_callback' 	=> 'maybe_hash_hex_color'
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widget_title_background_color', array(
        'label'   		=> 	__('Widget Title Background Color', 'cc2'),
        'section' 		=> 	'widgets',
        'priority'		=> 	142
    ) ) );*/


	/**
	 * Add extra panel for Widget Styling ;-)
	 * 
	 * @requires WP 4.0
	 * @since 2.0.25
	 */

	$strWidgetPanelID = '';
	
	$current_wp_version = get_bloginfo('version' );
	
	if( intval($current_wp_version[0] ) >= 4 ) {
		$strWidgetPanelID = 'widgets_styling';
		
		$wp_customize->add_panel( $strWidgetPanelID, array(
			'title' => __('Widgets Styling', 'cc2pro'),
			'priority' => ( !empty( $customizer_section_priorities['section_widgets'] ) ? $customizer_section_priorities['section_widgets'] : 110 ),
		) );
		
		
		if( isset( $GLOBALS['cc2_customizer'] ) ) {
		
		//$wp_customize->get_section('widget_settings')->panel = $strWidgetPanelID;
			$wp_customize->remove_section( 'widget_settings');
			
			
			$GLOBALS['cc2_customizer']->section_widgets( $wp_customize, array('panel_id' => $strWidgetPanelID ) );
		}

	}



	/**
	 * Header Widgets:
	 * - header widget title font size, family, color, background color 
	 * - header widget background color, link color, link hover color  
	 */

		$header_widgets_priority = 580;
		
		
		
		
		$wp_customize->add_section( 'widgets_header', array(
			'title'         => 	sprintf( __('Widget Styling: %s', 'cc2' ), __('Header', 'cc2' ) ),
			'priority'      => 	$header_widgets_priority++,
			'panel'		=> $strWidgetPanelID,
		) );

	// Header Widget Title Font Family
    $wp_customize->add_setting( 'widgets_header[title_font_family]', array(
        'default'       => 	'Pacifico',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_header[title_font_family]', array(
        'label'   		=> 	__('Widget Title Font Family', 'cc2'),
        'section' 		=> 	'widgets_header',
        'priority'		=> 	$header_widgets_priority++,
        'type'    		=> 	'select',
        'choices'    	=> 	$cc2_font_family
    ) );


	// Header Widget Title Font Family
    $wp_customize->add_setting( 'widgets_header[title_font_size]', array(
        'default'       => 	'',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_header[title_font_size]', array(
        'label'   		=> 	__('Widget Title Font Size', 'cc2'),
        'section' 		=> 	'widgets_header',
        'priority'		=> 	$header_widgets_priority++,
    ) );
    
    // Header Widget Title Font Weight
    $wp_customize->add_setting( 'widgets_header[title_font_weight]', array(
        'default'       => 	'',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_header[title_font_weight]', array(
        'label'   		=> 	__('Widget Title Font Weight', 'cc2'),
        'section' 		=> 	'widgets_header',
        'priority'		=> 	$header_widgets_priority++,
    ) );


	
	// widget title Font Color
	$wp_customize->add_setting('widgets_header[title_text_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_header[title_text_color]', array(
		'label'    				=> __('Title Font Color', 'cc2'),
		'section'  				=> 'widgets_header',
		'priority'				=> $header_widgets_priority++,
	) ) );
	
	// widget title background color
	$wp_customize->add_setting('widgets_header[title_background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_header[title_background_color]', array(
		'label'    				=> __('Title Background Color', 'cc2'),
		'section'  				=> 'widgets_header',
		'priority'				=> $header_widgets_priority,
	) ) );
	
	
	// widget container attributes:  background color, link color, link color hover 
	
	// widget background color 
	$wp_customize->add_setting('widgets_header[background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_header[background_color]', array(
		'label'    				=> __('Widget Background Color', 'cc2'),
		'section'  				=> 'widgets_header',
		'priority'				=> $header_widgets_priority++,
	) ) );
	

	// widget link color 
	$wp_customize->add_setting('widgets_header[link_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_header[link_color]', array(
		'label'    				=> __('Widget Link Color', 'cc2'),
		'section'  				=> 'widgets_header',
		'priority'				=> $header_widgets_priority++,
	) ) );

	
	// widget link hover color
	$wp_customize->add_setting('widgets_header[link_hover_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_header[link_hover_color]', array(
		'label'    				=> __('Widget Link Hover Color', 'cc2'),
		'section'  				=> 'widgets_header',
		'priority'				=> $header_widgets_priority++,
	) ) );
	
		// widget link hover background color
	$wp_customize->add_setting('widgets_header[link_hover_background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_header[link_hover_background_color]', array(
		'label'    				=> __('Widget Link Hover Background Color', 'cc2'),
		'section'  				=> 'widgets_header',
		'priority'				=> $header_widgets_priority++,
	) ) );

/**
 * footer_fullwidth Widgets:
 * - footer_fullwidth widget title font size, family, color, background color 
 * - footer_fullwidth widget background color, link color, link hover color  
 */

		$footer_fullwidth_widgets_priority = $header_widgets_priority+=2;
		
		
		$wp_customize->add_section( 'widgets_footer_fullwidth', array(
			'title'         => 	sprintf( __('Widget Styling: %s', 'cc2' ), __('Footer Fullwidth', 'cc2' ) ),
			'priority'      => 	$footer_fullwidth_widgets_priority++,
			'panel'		=> $strWidgetPanelID,
		) );

	// footer_fullwidth Widget Title Font Family
    $wp_customize->add_setting( 'widgets_footer_fullwidth[title_font_family]', array(
        'default'       => 	'Pacifico',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_footer_fullwidth[title_font_family]', array(
        'label'   		=> 	__('Widget Title Font Family', 'cc2'),
        'section' 		=> 	'widgets_footer_fullwidth',
        'priority'		=> 	$footer_fullwidth_widgets_priority++,
        'type'    		=> 	'select',
        'choices'    	=> 	$cc2_font_family
    ) );


	// footer_fullwidth Widget Title Font Size
    $wp_customize->add_setting( 'widgets_footer_fullwidth[title_font_size]', array(
        'default'       => 	'',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_footer_fullwidth[title_font_size]', array(
        'label'   		=> 	__('Widget Title Font Size', 'cc2'),
        'section' 		=> 	'widgets_footer_fullwidth',
        'priority'		=> 	$footer_fullwidth_widgets_priority++,
    ) );
    
    // Footer Widget Title Font Weight
    $wp_customize->add_setting( 'widgets_footer_fullwidth[title_font_weight]', array(
        'default'       => 	'',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_footer_fullwidth[title_font_weight]', array(
        'label'   		=> 	__('Widget Title Font Weight', 'cc2'),
        'section' 		=> 	'widgets_footer_fullwidth',
        'priority'		=> 	$footer_fullwidth_widgets_priority++,
    ) );

	
	// widget title Font Color
	$wp_customize->add_setting('widgets_footer_fullwidth[title_text_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_fullwidth[title_text_color]', array(
		'label'    				=> __('Title Font Color', 'cc2'),
		'section'  				=> 'widgets_footer_fullwidth',
		'priority'				=> $footer_fullwidth_widgets_priority++,
	) ) );
	
	// widget title background color
	$wp_customize->add_setting('widgets_footer_fullwidth[title_background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_fullwidth[title_background_color]', array(
		'label'    				=> __('Title Background Color', 'cc2'),
		'section'  				=> 'widgets_footer_fullwidth',
		'priority'				=> $footer_fullwidth_widgets_priority,
	) ) );
	
	
	// widget container attributes:  background color, link color, link color hover 
	
	// widget background color 
	$wp_customize->add_setting('widgets_footer_fullwidth[background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_fullwidth[background_color]', array(
		'label'    				=> __('Widget Background Color', 'cc2'),
		'section'  				=> 'widgets_footer_fullwidth',
		'priority'				=> $footer_fullwidth_widgets_priority++,
	) ) );
	

	// widget link color 
	$wp_customize->add_setting('widgets_footer_fullwidth[link_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_fullwidth[link_color]', array(
		'label'    				=> __('Widget Link Color', 'cc2'),
		'section'  				=> 'widgets_footer_fullwidth',
		'priority'				=> $footer_fullwidth_widgets_priority++,
	) ) );

	
	// widget link hover color
	$wp_customize->add_setting('widgets_footer_fullwidth[link_hover_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_fullwidth[link_hover_color]', array(
		'label'    				=> __('Widget Link Hover Color', 'cc2'),
		'section'  				=> 'widgets_footer_fullwidth',
		'priority'				=> $footer_fullwidth_widgets_priority++,
	) ) );
	
		// widget link hover background color
	$wp_customize->add_setting('widgets_footer_fullwidth[link_hover_background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_fullwidth[link_hover_background_color]', array(
		'label'    				=> __('Widget Link Hover Background Color', 'cc2'),
		'section'  				=> 'widgets_footer_fullwidth',
		'priority'				=> $footer_fullwidth_widgets_priority++,
	) ) );


/**
 * footer_column Widgets:
 * - footer_column widget title font size, family, color, background color 
 * - footer_column widget background color, link color, link hover color  
 */

		$footer_column_widgets_priority = $footer_fullwidth_widgets_priority+=2;
		
		
		$wp_customize->add_section( 'widgets_footer_column', array(
			'title'         => 	sprintf( __('Widget Styling: %s', 'cc2' ), __('Footer Column', 'cc2' ) ),
			'priority'      => 	$footer_column_widgets_priority++,
			'panel'		=> $strWidgetPanelID,
		) );

	// footer_column Widget Title Font Family
    $wp_customize->add_setting( 'widgets_footer_column[title_font_family]', array(
        'default'       => 	'Pacifico',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_footer_column[title_font_family]', array(
        'label'   		=> 	__('Widget Title Font Family', 'cc2'),
        'section' 		=> 	'widgets_footer_column',
        'priority'		=> 	$footer_column_widgets_priority++,
        'type'    		=> 	'select',
        'choices'    	=> 	$cc2_font_family
    ) );


	// footer_column Widget Title Font Size
    $wp_customize->add_setting( 'widgets_footer_column[title_font_size]', array(
        'default'       => 	'',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_footer_column[title_font_size]', array(
        'label'   		=> 	__('Widget Title Font Size', 'cc2'),
        'section' 		=> 	'widgets_footer_column',
        'priority'		=> 	$footer_column_widgets_priority++,
    ) );
    
      
    // Header Widget Title Font Weight
    $wp_customize->add_setting( 'widgets_footer_column[title_font_weight]', array(
        'default'       => 	'',
        'capability'    => 	'edit_theme_options',
        'transport'   	=> 	'refresh',
    ) );
    $wp_customize->add_control( 'widgets_footer_column[title_font_weight]', array(
        'label'   		=> 	__('Widget Title Font Weight', 'cc2'),
        'section' 		=> 	'widgets_footer_column',
        'priority'		=> 	$footer_column_widgets_priority++,
    ) );

	
	// widget title Font Color
	$wp_customize->add_setting('widgets_footer_column[title_text_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_column[title_text_color]', array(
		'label'    				=> __('Title Font Color', 'cc2'),
		'section'  				=> 'widgets_footer_column',
		'priority'				=> $footer_column_widgets_priority++,
	) ) );
	
	// widget title background color
	$wp_customize->add_setting('widgets_footer_column[title_background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_column[title_background_color]', array(
		'label'    				=> __('Title Background Color', 'cc2'),
		'section'  				=> 'widgets_footer_column',
		'priority'				=> $footer_column_widgets_priority,
	) ) );
	
	
	// widget container attributes:  background color, link color, link color hover 
	
	// widget background color 
	$wp_customize->add_setting('widgets_footer_column[background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_column[background_color]', array(
		'label'    				=> __('Widget Background Color', 'cc2'),
		'section'  				=> 'widgets_footer_column',
		'priority'				=> $footer_column_widgets_priority++,
	) ) );
	

	// widget link color 
	$wp_customize->add_setting('widgets_footer_column[link_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_column[link_color]', array(
		'label'    				=> __('Widget Link Color', 'cc2'),
		'section'  				=> 'widgets_footer_column',
		'priority'				=> $footer_column_widgets_priority++,
	) ) );

	
	// widget link hover color
	$wp_customize->add_setting('widgets_footer_column[link_hover_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_column[link_hover_color]', array(
		'label'    				=> __('Widget Link Hover Color', 'cc2'),
		'section'  				=> 'widgets_footer_column',
		'priority'				=> $footer_column_widgets_priority++,
	) ) );


	// widget link hover background color
	$wp_customize->add_setting('widgets_footer_column[link_hover_background_color]', array(
		'default'           	=> '',
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
		'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
		'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'widgets_footer_column[link_hover_background_color]', array(
		'label'    				=> __('Widget Link Hover Background Color', 'cc2'),
		'section'  				=> 'widgets_footer_column',
		'priority'				=> $footer_column_widgets_priority++,
	) ) );






$footer_section_priority = 330;
 
	/*$wp_customize->add_section( 'footer', array(
		'title'         => 	'Footer',
		'priority'      => 	$footer_section_priority,
	) );
	$footer_section_priority++;*/

	// Option to disable the ENTIRE footer
	 $wp_customize->add_setting('display_footer', array(
		'default'           	=> true,
		'capability'        	=> 'edit_theme_options',
		'transport'   			=> 'refresh',
	) );
	
	$wp_customize->add_control( 'display_footer', array(
		'label'    				=> __('Display footer', 'cc2'),
		'section'  				=> 'footer',
		'type'					=> 'checkbox',
		'priority'				=> $footer_section_priority++,
	) );

	

	// fullwidth footer
	/*
	 * - footer fullwidth background image (for the wrap!)  
		- footer fullwidth background color (with possibility for transparency) 
		- footer fullwidth border top color (with possibility for transparency) 
		- footer fullwidth border bottom color (with possibility for transparency) 
	*/
		
			
		
		
            // A Quick Note on Bootstrap Variables
            $wp_customize->add_setting( 'footer_column_note', array(
                'capability'    => 	'edit_theme_options',
            ) );
            $wp_customize->add_control( new Labeled_Description( $wp_customize, 'footer_column_note', array(
				'label' 		=> 	 array(
					'title' 		=> __('Footer Columns', 'cc2'), 
					'description' 	=> __('Attributes of the column-based footer', 'cc2'),
				),
				'type' 			=> 	'description',
				'section' 		=> 	'footer',
				'priority'		=> 	$footer_section_priority,
			) ) );
            $footer_section_priority++;
		
		// footer column background image (footer column-wrap)
		$wp_customize->add_setting('footer_column_background_image', array(
            'default'           	=> '',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
        ) );
        
	    $wp_customize->add_control( new  WP_Customize_Image_Control($wp_customize, 'footer_column_background_image', array(
            'label'    				=> __('Background Image', 'cc2'),
            'section'  				=> 'footer',
            'priority'				=> $footer_section_priority,
        ) ) );
        $footer_section_priority++;
        
	
		// footer column background color
		$wp_customize->add_setting('footer_column_background_color', array(
            'default'           	=> '#eee',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
            'sanitize_callback' 	=> array('cc2_Sanitizer', 'sanitize_hex_color_no_hash_with_transparency' ),
            /*'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',*/
            'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
        ) );
	    $wp_customize->add_control( new cc2_Customize_Color_Control($wp_customize, 'footer_column_background_color', array(
            'label'    				=> __('Background Color', 'cc2'),
            'section'  				=> 'footer',
            'priority'				=> $footer_section_priority,
        ) ) );
        $footer_section_priority++;
        
	
		// footer column border top color
		$wp_customize->add_setting('footer_column_border_top_color', array(
            'default'           	=> '#ddd',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
            'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
            'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
        ) );
	    $wp_customize->add_control( new cc2_Customize_Color_Control($wp_customize, 'footer_column_border_top_color', array(
            'label'    				=> __('Color of upper border', 'cc2'),
            'section'  				=> 'footer',
            'priority'				=> $footer_section_priority,
        ) ) );
        $footer_section_priority++;
        
        
        // footer column border bottom color (it's actually the branding top color ^_^)
		$wp_customize->add_setting('footer_column_border_bottom_color', array(
            'default'           	=> '#333',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
            'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
            'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
        ) );
	    $wp_customize->add_control( new cc2_Customize_Color_Control ($wp_customize, 'footer_column_border_bottom_color', array(
            'label'    				=> __('Color of lower border', 'cc2'),
            'section'  				=> 'footer',
            'priority'				=> $footer_section_priority++,
        ) ) );
	

	// Branding
	$branding_section_priority = 80;
	
		// Branding footer section - Small Heading
		$wp_customize->add_setting( 'headline_footer_credits', array(
            'capability'    => 	'edit_theme_options',
        ) );
		$wp_customize->add_control( new Label( $wp_customize, 'headline_footer_credits', array(
            'label' 		=> 	__('Footer Credits'),
            'type' 			=> 	'label',
            'section' 		=> 	'branding',
            'priority'		=> 	$branding_section_priority++,
        ) ) );
	
	
	//- show credit links in footer? (checkbox to hide) 
		 $wp_customize->add_setting('footer_branding_show_credits', array(
            'default'           	=> true,
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
        ) );
        
	    $wp_customize->add_control( 'footer_branding_show_credits', array(
            'label'    				=> __('Show credit links', 'cc2'),
            'section'  				=> 'branding',
            'type'					=> 'checkbox',
            'priority'				=> $branding_section_priority++,
        ) );

	
	// - add your own footer credit links (textarea for your own html) 
		$wp_customize->add_setting('footer_branding_custom_credits', array(
            'default'           	=> '',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
        ) );
        
	    $wp_customize->add_control( new  Textarea_Custom_Control($wp_customize, 'footer_branding_custom_credits', array(
            'label'    				=> __('Custom credits', 'cc2'),
            'section'  				=> 'branding',
            'priority'				=> $branding_section_priority++
        ) ) );
        

/**
 * Scroll-to-top Button Section: only active, if button is enabled
 * 
 * - colors and transparency
- icon / symbol (font awesome / glyphicons / etc.)
- size
- position (left / right bottom)
- when to display the button (ie. absolute Y value) => offset: 100 (100px)
- fade-out/in time (none; doesnt fadein/out)
* scroll-up speed (jQuery: slow)
 */
	//if( $advanced_settings['load_scroll_top'] == '1' ) :
 
		$bootstrap_scroll_to_top_priority = 540;
		
		
		$wp_customize->add_section( 'bootstrap_scroll_top', array(
			'title'         => 	__('Bootstrap: Scroll to top button', 'cc2' ),
			'priority'      => 	$bootstrap_scroll_to_top_priority,
		) );
		
		// Link text color
        $wp_customize->add_setting( 'bootstrap_scroll_top[link_color]', array(
            'default'           	=> '#F2694B',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
            'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
            'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
        ) );
        $wp_customize->add_control( new cc2_Customize_Color_Control($wp_customize, 'bootstrap_scroll_top[link_color]', array(
            'label'    				=> __('Link color', 'cc2'),
            'section'  				=> 'bootstrap_scroll_top',
            'priority'				=> $bootstrap_scroll_to_top_priority++,
        ) ) );

		// Hover text color
        $wp_customize->add_setting( 'bootstrap_scroll_top[hover_text_color]', array(
            'default'           	=> '#F2854B',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
            'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
            'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
        ) );
        $wp_customize->add_control( new cc2_Customize_Color_Control($wp_customize, 'bootstrap_scroll_top[hover_text_color]', array(
            'label'    				=> __('Hover font color', 'cc2'),
            'section'  				=> 'bootstrap_scroll_top',
            'priority'				=> $bootstrap_scroll_to_top_priority++,
        ) ) );

		// Hover background color
        $wp_customize->add_setting( 'bootstrap_scroll_top[hover_background_color]', array(
            'default'           	=> 'transparent',
            'capability'        	=> 'edit_theme_options',
            'transport'   			=> 'refresh',
            'sanitize_callback' 	=> 'sanitize_hex_color_no_hash',
            'sanitize_js_callback' 	=> 'maybe_hash_hex_color',
        ) );
        $wp_customize->add_control( new cc2_Customize_Color_Control($wp_customize, 'bootstrap_scroll_top[hover_background_color]', array(
            'label'    				=> __('Hover background color', 'cc2'),
            'section'  				=> 'bootstrap_scroll_top',
            'priority'				=> $bootstrap_scroll_to_top_priority++,
        ) ) );
	
	
		// Scroll Top Button: Icon Font Size
		$wp_customize->add_setting( 'bootstrap_scroll_top[icon_size]', array(
		'default'       => 	'18px',
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control( 'bootstrap_scroll_top[icon_size]', array(
		'label'   		=> 	__('Icon Size', 'cc2'),
		'section' 		=> 	'bootstrap_scroll_top',
		'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );
		
		// Scroll Top Button: Change Button Icon
		$wp_customize->add_setting( 'bootstrap_scroll_top[button_icon_class]', array(
		'default'       => 	'glyphicon glyphicon-chevron-up',
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control( 'bootstrap_scroll_top[button_icon_class]', array(
		'label'   		=> 	__('Button icon HTML class(es)', 'cc2'),
		'section' 		=> 	'bootstrap_scroll_top',
		'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );
		
		// Scroll Top Button: Text Font Size
		$wp_customize->add_setting( 'bootstrap_scroll_top[font_size]', array(
		'default'       => 	'18px',
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control( 'bootstrap_scroll_top[font_size]', array(
		'label'   		=> 	__('Text Font Size', 'cc2'),
		'section' 		=> 	'bootstrap_scroll_top',
		'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );

		// Scroll Top Button: Button Text
		$wp_customize->add_setting( 'bootstrap_scroll_top[button_text]', array(
		'default'       => 	__('Back to Top', 'cc2'),
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control( 'bootstrap_scroll_top[button_text]', array(
		'label'   		=> 	__('Button text', 'cc2'),
		'section' 		=> 	'bootstrap_scroll_top',
		'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );
		
		// Scroll Top Button: Link
		$wp_customize->add_setting( 'bootstrap_scroll_top[button_link]', array(
		'default'       => 	'#masthead',
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control( 'bootstrap_scroll_top[button_link]', array(
		'label'   		=> 	__('Button link (internal anchor)', 'cc2'),
		'section' 		=> 	'bootstrap_scroll_top',
		'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );
		
		// Scroll Top Button: Button Height
		$wp_customize->add_setting( 'bootstrap_scroll_top[height]', array(
		'default'       => 	'18',
		'capability'    => 	'edit_theme_options',
		'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control( 'bootstrap_scroll_top[height]', array(
		'label'   		=> 	__('Button height (in pixels)', 'cc2'),
		'section' 		=> 	'bootstrap_scroll_top',
		'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );

		// Scroll Top Button: Position ( right / left bottom )
		$wp_customize->add_setting( 'bootstrap_scroll_top[position]', array(
			'default'		=>	'right',
			'capability'    => 	'edit_theme_options',
			'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control('bootstrap_scroll_top[position]', array(
			'label'    		=> 	__('Position (bottom)'),
			'section'  		=> 	'bootstrap_scroll_top',
			'type'     		=> 	'select',
			'choices'		=> array(
				'left' => __('Left'),
				'right' => __('Right'),
			),
			'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );
		
		// Scroll Top Button: Position left / right in pixels
		$wp_customize->add_setting( 'bootstrap_scroll_top[position_offset]', array(
			'default'		=>	10,
			'capability'    => 	'edit_theme_options',
			'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control('bootstrap_scroll_top[position_offset]', array(
			'label'    		=> 	__('Position offset from window border (in pixels)'),
			'section'  		=> 	'bootstrap_scroll_top',
			'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );
		
		// Scroll Top Button: Offset
		$wp_customize->add_setting( 'bootstrap_scroll_top[offset]', array(
			'default'		=>	'100',
			'capability'    => 	'edit_theme_options',
			'transport'   	=> 	'refresh',
		) );
		$wp_customize->add_control('bootstrap_scroll_top[offset]', array(
			'label'    		=> 	__('Footer offset (in pixels)', 'cc2'),
			'section'  		=> 	'bootstrap_scroll_top',
			'priority'		=> 	$bootstrap_scroll_to_top_priority++,
		) );

}


/**
 * WordPress Customizer initialization
 *
 * @author Konrad Sroka
 * @package cc2_pro
 * @since 2.0
 */

add_action( 'init', 'cc2pro_customizer_init', 12 );

function cc2pro_customizer_init(){

    add_action( 'customize_register', 'cc2pro_customizer_support', 33 );

}


/**
 * WordPress Customizer Preview init
 *
 * @author Konrad Sroka
 * @package cc2_pro
 * @since 2.0
 */
/*
add_action( 'customize_preview_init', 'cc2pro_customizer_preview_init');

function cc2pro_customizer_preview_init(){

    $cc2pro_customizer_options = get_option('cc2pro_customizer_options');

    wp_enqueue_script('jquery');
    
    // maybe load required JS / CSS stuff?
    

//    wp_enqueue_script(
//        'tk_customizer_preview_js',
//        get_template_directory_uri() . '/includes/admin/js/customizer.js',
//        array( 'jquery', 'customize-preview' ), '', true
//    );

}

// that's it! we're done!
*/
