<?php

/**
 * TODO: Turn this into a proper class
 * @author Fabian Wolf
 */
if( !defined( 'CC2_TOOLS_TEMPLATE_PATH' ) ) {
	define( 'CC2_TOOLS_TEMPLATE_PATH', plugin_dir_path(__FILE__) . '/templates/' );
}



function cc2_pro_screen_tools() { 
	include( CC2_TOOLS_TEMPLATE_PATH . 'tools.php' );

}
/**
 * Register the admin settings
 *
 * @author Sven Lehnert
 * @package TK Google Fonts
 * @since 1.0
 */

add_action( 'admin_init', array('cc2proToolsAdmin', 'get_instance' ) );



class cc2proToolsAdmin {
	var $classPrefix = 'cc2pro_tools_',
		$arrDefaultSettings = array(
			'output_file' => 'style.css',
			'mode' => 'dynamic',
		);
	/**
	 * Plugin instance.
	 *
	 * @see get_instance()
	 * @type object
	 */
	protected static $instance = NULL;
		
	/**
	 * Implements Factory pattern
	 * Strongly inspired by WP Maintenance Mode of Frank Bueltge ^_^ (@link https://github.com/bueltge/WP-Maintenance-Mode)
	 * 
	 * Access this plugins working instance
	 *
	 * @wp-hook after_setup_theme
	 * @return object of this class
	 */
	public static function get_instance() {

		NULL === self::$instance and self::$instance = new self;

		return self::$instance;
	}
	
	function __construct() {
		//add_action('admin_init', array( $this, 'init_settings' ) );
		
		$upload_dir = wp_upload_dir();
	
		$this->arrDefaultSettings = array(
			'output_path' => $upload_dir['basedir'] . '/cc2/',
			'output_file' => 'style.css',
		);
		
		// add actions + co.
		
		$this->add_ajax_calls();
		
		// AFTER adding the ajax calls
		$this->init_settings();
		
	}
	
	
	function init_settings() {
		$strSettingsGroup = 'cc2_tools_options';
		$strOptionName = $strSettingsGroup;

		register_setting( $strSettingsGroup, $strOptionName );

		
		// Settings fields and sections
		$strSectionID = 'section_less';
		
		add_settings_section(
			$strSectionID, 
			'', '', 
			$strSettingsGroup
		);

		/**
		 * NOTE: Not yet.
		 */

		/*
		
		add_settings_field(
			'less-compile-settings', 
			'<h4>Compiling Settings</h4>', 
			
			array( $this, 'admin_setting_less_settings' ), // callback //
			
			$strOptionName,
			$strSectionID
		);*/

		add_settings_field(
			'less-compile-mode', 
			'<h4>Compiling Mode</h4>', 
			
			array( $this, 'admin_setting_less_compiler_mode' ), /** callback */
			
			$strOptionName,
			$strSectionID
		);
		//add_settings_field(		'less-compiler', '<h4>Minifire CSS </h4><br />', 'cc2_tools_less_compiler'	, 'cc2_tools_options'	, 'section_less' );
		add_settings_field(	
			'less-generate-now',
			'<h4>Generate CSS Manually</h4>', 
			
			array( $this, 'admin_setting_regenerate_css' ), 
			
			$strOptionName,
			$strSectionID
		);
	}



		function admin_setting_less_settings() {
			$cc2_tools_options = get_option('cc2_tools_options' );
			
			extract( $this->arrDefaultSettings, EXTR_PREFIX_ALL, 'default' );
			
			// set defaults
			$cc2_tools_options['output_file'] = _switchdefault( $cc2_tools_options['output_file'], $default_output_file );
			$cc2_tools_options['output_path'] = _switchdefault( $cc2_tools_options['output_path'], $default_output_path );
		

			include( plugin_dir_path(__FILE__) . '/templates/edit-less-settings.php' );
		}


		function admin_setting_less_compiler_mode() {	
			$cc2_tools_options = get_option('cc2_tools_options', array('mode' => 'dynamic') );
			$cc2_tools_options['mode'] = _switchdefault( $cc2_tools_options['mode'], 'dynamic' );
			
			
			
			if( !empty( $_POST['cc2_tools_options']['mode'] ) && $_POST['cc2_tools_options']['mode'] == 'static' && $cc2_tools_options['mode'] == 'dynamic' ) {
				cc2pro_ActivationKit::prepare_less_static_mode();
			}
			

			include( plugin_dir_path(__FILE__) . '/templates/edit-less-mode.php' );

		}
			
		function admin_setting_regenerate_css() {
			$cc2_tools_options = get_option('cc2_tools_options' );
			
			extract( $this->arrDefaultSettings, EXTR_PREFIX_ALL, 'default' );
			
			// set defaults
			$cc2_tools_options['output_file'] = _switchdefault( $cc2_tools_options['output_file'], $default_output_file );
			$cc2_tools_options['output_path'] = _switchdefault( $cc2_tools_options['output_path'], $default_output_path );
		
			
			include( plugin_dir_path(__FILE__) . '/templates/regenerate-css.php' );
		}
		
	// minify settings
	/*function cc2_tools_less_compiler() {
		$cc2_tools_options = get_option('cc2_tools_options', false );

		


		$less_disabled = false;

		if( !empty( $cc2_tools_options ) && isset( $cc2_tools_options['less_disabled'] ) ) {
			$less_disabled = $cc2_tools_options['less_disabled'];
		}
		
		include( plugin_dir_path(__FILE__) . '/templates/edit-less-status.php' );
	}*/

	/**
	 * Add all required ajax calls and processing stuff
	 *  
	 * @author Fabian Wolf
	 * @package cc2pro
	 * @since 2.0
	 */
	
	function add_ajax_calls() {
		/**
		 * TODO: We might want to add a user_can() check in here, to avoid possible security holes
		 * @see http://codex.wordpress.org/AJAX_in_Plugins#Ajax_on_the_Viewer-Facing_Side
		 */
		
		// save settings
		add_action( 'wp_ajax_' . $this->classPrefix . 'regenerate_css', array( $this, 'ajax_regenerate_css') );
		add_action( 'wp_ajax_nopriv_' . $this->classPrefix . 'regenerate_css', array( $this, 'ajax_regenerate_css') );

	}


		function ajax_regenerate_css() {
			$return = array('type' => 'error', 'message' => 'Error regenerating the CSS file');
			global $cc2_color_schemes;
			// disable NOTICES - needs no stinking NOTICES in the AJAX response!
			error_reporting( E_ALL ^ E_NOTICE );

			
			
			$handler = new cc2pro_Less_Handler();
			$current_scheme = $cc2_color_schemes->get_current_color_scheme();
			
			//new __debug( $current_scheme, 'current color scheme');
			
			/**
			 * NOTE: Add scheme variables
			 */
			$arrParseParams = $current_scheme;
			
			
			/**
			 * TODO: Reorder Less Handler params
			 */
			
			
			
			$arrParseParams['source_path'] = $current_scheme['scheme_path'] . $current_scheme['file'];
			$arrParseParams['add_vars'] = cc2pro_Less_Handler::get_theme_settings();
				
	
			
			
			
			//new __debug( $arrParseParams, 'arrParseParams' );
			
			$result = $handler->parse_stylesheet( $arrParseParams );
			
			//new __debug( $result, 'build result' );
			if( $result != false ) {
				$return = array(
					'type' => 'success',
					'message' => 'Successfully regenerated the CSS file',
					'details' => $result
				);
			}
			
			
			wp_send_json( $return );
			exit;
		}
	
}


/**
 * Ajax call
 */ 
/*
add_action( 'wp_ajax_cc2_style_css_generate', 'cc2_style_css_generate' );
add_action( 'wp_ajax_nopriv_cc2_style_css_generate', 'cc2_style_css_generate' );
function cc2_style_css_generate($vars) {
    cc2_less_handler::add_css($vars);
    die();
}
*/
