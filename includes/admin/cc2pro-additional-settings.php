<?php
/**
 * Testing post-implementation abilities of the admin page settings
 */

class cc2pro_AddSettings {
	var $classPrefix = 'cc2pro_add_settings_';
		
	/**
	 * Plugin instance.
	 *
	 * @see get_instance()
	 * @type object
	 */
	protected static $instance = NULL;
	
	/**
	 * Implements Factory pattern
	 * Strongly inspired by WP Maintenance Mode of Frank Bueltge ^_^ (@link https://github.com/bueltge/WP-Maintenance-Mode)
	 * 
	 * Access this plugins working instance
	 *
	 * @wp-hook after_setup_theme
	 * @return object of this class
	 */
	public static function get_instance() {

		NULL === self::$instance and self::$instance = new self;

		return self::$instance;
	}
	
	function __construct() {
		// init variables
		//$this->upgrade_styling_settings();
		$this->strDestOptionName = 'cc2_advanced_settings';
		$this->strDestPrefix = $this->strDestOptionName . '_';
		
		// add more data for theme admin.js
		add_action('admin_enqueue_scripts', array( $this, 'init_admin_js' ) );
		
		// add filter: {classPrefix}known_fields
		add_filter( $this->strDestPrefix . 'known_fields', array( $this, 'add_more_save_fields' ) );
	
		// add filter: {classPrefix}button_save_changes
		add_filter( $this->strDestPrefix . 'button_save_changes', array( $this, 'filter_save_button' ) );
		
		
		// register required settings, sections etc.
		add_action( 'admin_init', array( $this, 'register_admin_settings' ), 12 );
		
		
		
	}
	
	function init_admin_js() {
		// adds an empty script file which is just used to add additional data (mostly form field settings) for the theme admin.js
		$arrOptions = array(
			'custom_script_header' => 'textarea',
			'custom_script_footer' => 'textarea',
		);
		
		wp_enqueue_script('cc2pro-dummy-js', plugins_url( 'js/empty.js', __FILE__ ), array('jquery' ), 1 );
		wp_localize_script('cc2pro-dummy-js','admin_js_add_options', $arrOptions);
		
	}
	
	function filter_save_button( $content = '' ) {
		$return = $content;
		
		if( stripos( $content, ' id="save-advanced-settings"' ) !== false ) {
			$return = ''; // remove button (and re-add it later on)
		}
		
		return $return;
	}
	
	
	function add_more_save_fields( $fields = array() ) {
		$return = $fields;
		
		$return['custom_script_footer'] = 'text';
		$return['custom_script_header'] = 'text';
		
		return $return;
	}
	
	
	function register_admin_settings() {
		
		$strSettingsGroup = $this->strDestPrefix . 'options';
		$strSettingsPage = $strSettingsGroup;
		
		add_settings_field(
			$this->strDestPrefix . 'select_bootstrap_scripts',
			'<strong>Choose Bootstrap scripts loading</strong>',
			array( $this, 'admin_setting_select_bootstrap_scripts' ),
			$strSettingsPage,
			'section_general',
			array('message' => 'Adjust which Bootstrap scripts are being loaded')
		);
		
		add_settings_field(
			$this->strDestPrefix . 'custom_script_header',
			'<strong>Add Javascript to header</strong>',
			array( $this, 'admin_setting_custom_script' ), /** method callback */
			$strSettingsPage,
			'section_general',
			array( 'position' => 'header' )
		);
		
		
		add_settings_field(
			$this->strDestPrefix . 'custom_script_footer',
			'<strong>Add Javascript to footer</strong>',
			array( $this, 'admin_setting_custom_script' ), /** method callback */
			$strSettingsPage,
			'section_general',
			array( 'position' => 'footer', 'message' => 'Mostly used for adding Google Analytics and similar scripts.' )
		);
	
		add_settings_field(
			$this->classPrefix . 'save',
			'',
			array( $this, 'admin_setting_save' ), /* method callback  */
			$strSettingsPage,
			'section_general'
		);
		
			

	}
	
	function admin_setting_custom_script( $arrParams = array() ) {
		//new __debug( $arrParams, 'params for ' . basename( __FILE__ ) );
		extract( $arrParams );
				
		if( !empty( $position ) ) {
			$advanced_settings = get_option( $this->strDestOptionName, false );

			include( plugin_dir_path( __FILE__ ) . '/templates/add-custom-javascript.php' );
		}
		
	}
	
	/**
	 * TODO: Hook this into the yet-to-be rewritten tab / admin page implementation
	 */
	
	function admin_setting_select_bootstrap_scripts( $arrParams = array() ) {
		extract( $arrParams );
		
		/**
		 * load bootstrap js
		 * NOTE: Improves adjustability, while trying to nail down the bloody random appearing "customizer preview = blank page" bug
		 * Descriptions taken directly from @link http://getbootstrap.com/javascript/
		 */
		
		$arrKnownBootstrapScripts = array(
			'cc-bootstrap-tooltip' => 'Tooltip.js; Inspired by the excellent jQuery.tipsy plugin written by Jason Frame; Tooltips is an updated version, which doesn\'t rely on images, uses CSS3 for animations, and data-attributes for local title storage.',
			'cc-bootstrap-transition' => 'Transition.js is a basic helper for transitionEnd events as well as a CSS transition emulator. It\'s used by the other plugins to check for CSS transition support and to catch hanging transitions.',
			
			'cc-bootstrap-affix' => 'Affix; required eg. for fixed navbars.',
			'cc-bootstrap-alert' => 'Add dismiss functionality to all alert messages with this plugin.',
			'cc-bootstrap-button' => 'Do more with buttons. Control button states or create groups of buttons for more components like toolbars.',
			
			'cc-bootstrap-carousel' => 'Simple slideshow function; required if you want to use the default Custom Community slideshow script',
			'cc-bootstrap-collapse' => 'Get base styles and flexible support for collapsible components like accordions and navigation. Requires the transitions plugin to be included in your version of Bootstrap.',
			'cc-bootstrap-dropdown' => 'Add dropdown menus to nearly anything with this simple plugin, including the navbar, tabs, and pills. Required by the navbar dropdowns.',
			'cc-bootstrap-modal' => 'Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.',
			'cc-bootstrap-popover' => 'Add small overlays of content, like those on the iPad, to any element for housing secondary information. Popovers require the tooltip plugin to be included.',
			'cc-bootstrap-scrollspy' => 'The ScrollSpy plugin is for automatically updating nav targets based on scroll position. Scroll the area below the navbar and watch the active class change. The dropdown sub items will be highlighted as well.',
			'cc-bootstrap-tab' => 'Add quick, dynamic tab functionality to transition through panes of local content, even via dropdown menus.',
		);
		
		
		$advanced_settings = get_option( $this->strDestOptionName, false );
		
		
		
		$strFieldName = 'enqueue_bootstrap_scripts';
		
		include( plugin_dir_path( __FILE__ ) . '/templates/adjust-bootstrap-scripts.php' );
		
	}
	
	// new save button
	
	function admin_setting_save() {
		$strSaveChangesButton = get_submit_button( __('Save Changes', 'cc2'), 'primary', 'save-advanced-settings', false );
		
		echo apply_filters( $this->classPrefix . 'button_save_changes', $strSaveChangesButton );
	}
	
	
}



add_action( 'after_setup_theme', array( 'cc2pro_AddSettings', 'get_instance'), 12 );
