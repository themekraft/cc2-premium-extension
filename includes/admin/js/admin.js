/**
 * cc2 premium JS
 *
 * @author Fabian Wolf
 * @since 2.0
 * @package cc2pro
 */
 
jQuery( function() {
	var bodyClass = document.getElementsByTagName('body')[0].getAttribute('class');
	if( bodyClass.indexOf('wp-admin') > -1 ) {
	
		if( bodyClass.indexOf('cc2-tools') > -1 ) {
			var strClassPrefix = 'cc2pro_tools_';
			
			var arrSettings = {
				'output_file': jQuery('input[name="cc2_tools_options[output_file]"]').val(),
				'output_path': jQuery('input[name="cc2_tools_options[output_path]"]').val(),
			};
			
			
			// check if mode has changed
			jQuery( document ).on('click', '#cc2-tools-less-submit', function(e) {
				// if mode changed to production = static, write the style.css once (or however the output file is called), and then continue
				
				
			} )
			
			
			jQuery( document ).on('click', '#cc2-regenerate-css', function(e) {
				e.preventDefault();
				
				// display spinning wheel
	
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						'action': strClassPrefix + 'regenerate_css',
						'settings': arrSettings
					},
					success: function( result ) {
						console.log(result);
					
						// hide spinning wheel
						
						
						
						
						
						// build result
						strResult = '<table class="table-result" style="width: 70%">' +
						'<tr>' +
							'<th colspan="3" class="result-title-row">Cache file</th>' +
						'</tr>' +
						'<tr>' + 
							'<th class="row-title">Path:</th>' + '<td>' + result.details.cache_file.path + '</td>' +
						'</tr>' +
						'<tr>' + 
							'<th class="row-title">Timestamp:</th>' + '<td>' + result.details.cache_file.timestamp + '</td>' +
						'</tr>' +
						'<tr>' + 
							'<th class="row-title">Status:</th>' + '<td class="col-status-'+(result.details.css_file.success ? 'success' : 'error')+'">' + (result.details.cache_file.success ? 'success' : 'failure') + '</td>' +
						'</tr>' +
						
						'<tr>' +
							'<th colspan="3" class="result-title-row">CSS file</th>' +
						'</tr>' +
						'<tr>' + 
							'<th class="row-title">Path:</th>' + '<td>' + result.details.css_file.path + '</td>' +
						'</tr>' +
						'<tr>' + 
							'<th class="row-title">Timestamp:</th>' + '<td>' + result.details.css_file.timestamp + '</td>' +
						'</tr>' +
						'<tr>' + 
							'<th class="row-title">Status:</th>' + '<td class="col-status-'+(result.details.css_file.success ? 'success' : 'error')+'">' + (result.details.css_file.success ? 'success' : 'failure') + '</td>' +
						'</tr>' +
						
						'</table>';
										
										
						// append new or alternatively replace existing message
						
						
						if( document.getElementById('cc2-css-generation-result' ) ) {
							jQuery('#cc2-css-generation-result').html( strResult );
						} else {
							jQuery('#cc2-regenerate-css').after('<p><a href="#cc2-css-generation-result">Show generation details</a></p><div class="result" id="cc2-css-generation-result" style="display:none">' + strResult + '</div>');
						}
						//jQuery('#cc2-generate-css').after('<p><a href="#">Show generation details</a></p><div class="result" id="cc2-css-generation-result" style="display:none">' 
						jQuery(document).on('click', 'a[href="#cc2-css-generation-result"]', function( ev ) {
							ev.preventDefault();
							
							if( jQuery( this ).css('display') != 'block' ) {
								jQuery( this.hash ).show();
							} else {
								jQuery( this.hash ).hide();
							}
							
							
						});
					
						// output simple alert notice
						alert( result.message );
						

					},
					error: function( result ) {
						// hide spinning wheel
						
						console.log('Something went wrong.. :-(sorry)');
					}
				});
				
			
			} );
		}
		
		// generic support link action
		jQuery(document).on('click', '.link-themekraft-support', function( e ) {
			e.preventDefault();
			
			if( typeof Zenbox != 'undefined' ) {
				Zenbox.show();
			}
			
		});
		
	}
} );
