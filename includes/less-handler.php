<?php
/**
 * Base WPLessPHP handling class with custom output file
 * Based on wp-less by Robert "sancho the fat" O'Rourke (@link http://sanchothefat.com/)
 * NOTE: Customizer handling has been outsourced to less-customizer-preview.php
 * 
 * @author Fabian Wolf
 * @package cc2pro
 * @since 2.0.11
 * 
 * 
 */
if( !function_exists('is_customizer_preview' ) ) :
	function is_customizer_preview() {
		$return = false;
		
		if( !is_admin() && isset($_POST['wp_customize']) && $_POST['wp_customize'] == 'on' && is_user_logged_in() ) {
			$return = true;
		}
		
		
		return $return;
	}
endif;

if ( ! class_exists( 'cc2pro_Less_Handler' ) ) {

class cc2pro_Less_Handler {


	/**
	* @var array Array store of callable functions used to extend the parser
	*/
	public $registered_functions = array();


	/**
	* @var array Array store of function names to be removed from the compiler class
	*/
	public $unregistered_functions = array();


	/**
	* @var array Variables to be passed into the compiler
	*/
	public $vars = array();


	/**
	* @var string Compression class to use
	*/
	public $compression = 'compressed';


	/**
	* @var bool Whether to preserve comments when compiling
	*/
	public $preserve_comments = true;


	/**
	* @var array Default import directory paths for lessc to scan
	*/
	public $import_dirs = array();

	public $arrKnownPlaceholders = array();

	/**
	 * @static
	 * @var    \wp_less Reusable object instance.
	 */
	protected static $instance = null;

	public $customizer_preview_style_url;

	/**
	 * Creates a new instance. Called on 'after_setup_theme'.
	 * May be used to access class methods from outside. Both global AND once-instanced
	 *
	 * @see    __construct()
	 * @static
	 * @return \wp_less
	 */
	public static function init() {
		global $less_handler;
		null === self::$instance AND self::$instance = new self;
		
		$less_handler = self::$instance;
		
		return self::$instance;
	}

	/**
	* Constructor
	*/
	public function __construct( ) {
		$this->arrKnownPlaceholders = self::get_placeholders();
		
		
		// add actions
		// fires every time the page is reloaded => should fix the Customizer Preview
		//add_action( 'init', array( $this, 'customizer_refresh_less' ) );
		
		//add_action('wp_loaded', array( $this, 'customizer_refresh_less' ) );
		/**
		 * NOTE: Hooks into init. Because of this, adding customizer_refresh_less to init lets its firing FAIL (ie. @hook init is alreay done!). Thus, we consequently use the constructor call instead (as its says: hooks into init).
		 */
		 
		// only latch onto the hook if we are NOT inside the customizer preview!
		if( is_customizer_preview() == false ) {
		
			// add filters
			add_filter('cc2_style_css', array( $this, 'cc2_add_style_less' ) );
		}
		
		/**
		 * TODO: Convert this into a proper implementation
		if( self::is_customizer_preview() ) {
			remove_filter( 'cc2_style_css',  array( $this, 'cc2_add_style_less' ) );
			add_filter( 'cc2_style_css', array( $this, 'cc2_customizer_preview_url' ) );
		}*/
	}
	
	
	public static function parse_placeholders( $path = '', $isForceURL = false ) {
		$return = $path;
		
		if( !empty( $path ) ) {
			
			// check if only placeholders are in $path
			$isPath = false;
			
			if( empty( $isForceURL ) ) {
				$isPath = ( stripos( parse_url( $path, PHP_URL_SCHEME), 'http' ) === false ? true : false );
			}
			
			$arrPlaceholders = self::get_placeholders( $isPath );
			
			foreach( $arrPlaceholders as $strKeyword => $strReplacement ) {
				$return = str_replace( '%' . $strKeyword . '%', $strReplacement, $return );
			}
		}
		
		return $return;
	}

	public static function get_placeholders( $isPath = true ) {
		$upload_dir = wp_upload_dir();
		
		$arrReturn = array(
			'current_theme' => ( $isPath != false ? get_stylesheet_directory() : get_stylesheet_directory_uri() ),
			'wp_less' => ( $isPath != false ? trailingslashit( $upload_dir['basedir'] ) . 'wp-less/' : trailingslashit( $upload_dir['baseurl'] . 'wp-less/' ) ),
			'cc2_uploads' => ( $isPath != false ? trailingslashit( $upload_dir['basedir'] ) . 'cc2/' : trailingslashit( $upload_dir['baseurl'] ) . 'cc2/' ),
			'upload_dir' => ( $isPath != false ? trailingslashit(  $upload_dir['basedir'] ) : trailingslashit($upload_dir['baseurl']) ),
		);
		
		// cc2 color schemes
		$arrReturn['cc2_schemes'] = trailingslashit( $arrReturn[ 'cc2_uploads' ] ) . 'schemes/';
			
		// detect parent / child theme usage
		
		$arrReturn['parent_theme'] = $arrReturn['current_theme'];
		
		if( is_child_theme() ) {
			$arrReturn['parent_theme'] = ( $isPath != false ? get_template_directory() : get_template_directory_uri() );
		}
		
		
		// filter before the return
		
		if( $isPath != false ) {
			$arrReturn = apply_filters('cc2_less_handler_known_path_placeholders', $arrReturn );
		} else {
			$arrReturn = apply_filters( 'cc2_less_handler_known_url_placeholders', $arrReturn );
		}
		
		// lets return the fun ;-)
		$return = $arrReturn;
		
		return $return;
	}
	
	/**
	 * Theme hook
	 */

	public function filter_stylesheet_url( $style_url = '' ) {
		$return = $style_url;
		
		//__debug::log( __METHOD__ . ' fires' );
		
		$current_scheme = cc2_get_current_color_scheme();
		$add_vars = self::get_theme_settings();
		
		$arrParams = $current_scheme;
		$arrParams['add_vars'] = $add_vars;
		
		$this->parse_stylesheet( $arrParams );
		
		
		$return .= (strpos( $return, '?' ) !== false ? '&' : '?' ) . 'parsed_with=less-handler.class.php';
		
		return $return;
	}


	/**
	 * Adds the parsed (!) data to the stylesheet URL
	 * 
	 * @hook cc2_style_css
	 * 
	 * NOTE: IF customizer preview is enabled, the compilation process is BYPASSED!
	 */
    function cc2_add_style_less( $url ) {
		//global $cc2_color_schemes;
		
		$return = $url;
		
		
		
		// fetch current mode
		$cc2_tools_options = get_option('cc2_tools_options');
		$less_mode = 'dynamic';
		$current_scheme = cc2_get_current_color_scheme();
		
		if(!isset($cc2_tools_options['mode']) || $cc2_tools_options['mode'] != 'dynamic') {
			$less_mode = 'static';
		}
		
		/**
		 * NOTE: do not run the compiler if current scheme is a backup!
		 */
			
		if( !empty($current_scheme['_modified'] ) && stripos( $current_scheme['output_file'], 'backup.' ) !== false ) {
			$less_mode = 'backup';
		}
		
		//new __debug( $current_scheme, __METHOD__ );
		
		
		/**
		 * NOTE: ignore the customizer preview completely, as its being handled elsewhere
		 * TODO: Add the customizer_preview check only for dynamic generation; the static mode should work completely independent, using just the regular theme data
		 * 
		 */
		
		if( is_customizer_preview() == false ) {
			$upload_dir = wp_upload_dir();
			
			
			
			//new __debug( $current_scheme, __METHOD__ . ': $current_scheme' );
			
			
		
		
			// mode switch
			switch( $less_mode ) {
				case 'backup':
					$return = $current_scheme['output_url'];
				
				
				
					break;
				default:
				case 'dynamic':
					
					$strAppendQuery = '?state=dynamic' . '&ts=' . time();

					$arrCompileParams = $current_scheme;
					
					
					$arrCompileParams['source_path'] = $current_scheme['scheme_path'] . $current_scheme['file'];

					
					$arrCompileParams['add_vars'] = $this->get_theme_settings();
					
					
					// add scheme info to the environment			
					
					$arrCompileParams['add_vars']['scheme_slug'] = $current_scheme['slug'];
					//$arrCompileParams['add_vars']['scheme_title'] = $current_scheme['title'];
					
					$result = $this->parse_less_file( $arrCompileParams );
					
					/*
					if( !empty( $result ) && is_array( $result ) ) {
						// happy, now?
						$return = $current_scheme['output_url'] . $current_scheme['output_file'] . '?state=dynamic' . '&ts=' . time();
					} else {
						$return = get_stylesheet_directory_uri() . '/style.css?state=error';
					}*/
				//}
				
					if( !empty( $result ) && is_array( $result ) ) {
						$return = $current_scheme['output_url'] . $current_scheme['output_file'] . $strAppendQuery;
						
					} else {
						$return = get_stylesheet_directory_uri() . '/style.css?state=error';
					}
				
					
					
					
				
					break;

				case 'static':
					if( file_exists( $current_scheme['output_dir'] . $current_scheme['output_file'] ) != false ) {
						$return = $current_scheme['output_url'] . $current_scheme['output_file']  . '?state=static';
					} else { // copy file from scheme dir to output dir
						if( file_exists( $current_scheme['scheme_path'] . $current_scheme['output_file'] ) ) {
							$bResult = copy( $current_scheme['scheme_path'] . $current_scheme['output_file'], $current_scheme['output_dir'] . $current_scheme['output_file'] );
							if( !empty( $bResult ) ) {
								$return = $current_scheme['output_url'] . $current_scheme['output_file'];
								// . '?state=static';
							}
						}
					}
					
					
					break;
			}
		} 
			
		
		return $return;

    }


	/**
	* Lessify the stylesheet and return the href of the compiled file
	*
	* @param  string $src			Source URL of the file to be parsed
	* @param  string $output_file	An identifier for the file used to create the file name in the cache (usually output_file)
	* @param  string $output_path	Where to write the parsed data into
	* @return string         		URL of the compiled stylesheet
	*/
	public function parse_stylesheet( $arrParams = array() ) {
		$return = false; 
		
		$arrDefaultParams = array(
			'source_url' => '',
			'source_path' => '',
			'output_file' => null, /** cc2_color_schemes => output_file */
			'output_dir' => '%cc2_uploads%', /** cc2_color_schemes => output_path */
			'add_vars' => array(),
		);
		
		$arrParams = wp_parse_args( $arrParams, $arrDefaultParams );
		
		//extract( $arrParams );
		
		//new __debug( $arrParams, 'parse_stylesheet: params' );
		
		
		/**
		 * FIXME: parse_stylesheet_url DOES NOT EXIST!
		 */
		
		if( !empty( $source_url ) ) {
			
			$return = $this->parse_stylesheet_url( $source_url, $output_file, $output_dir );
		} else {
			$return = $this->parse_less_file( $arrParams );
		}
		
		return $return;
	}
	

	
	//public function parse_less_file( $source_path, $output_file = null, $output_path = '%cc2_uploads%' ) {
	public function parse_less_file( $arrParams = array() ) {
		$return = false;
		$force = false;
		
		$arrDefaultParams = array(
			'source_path' => null, /** full path + filename of the source LESS file */
			'output_file' => null, /** cc2_color_scheme => output_file */
			'output_dir' => '%cc2_uploads%', /** cc2_color_scheme => output_path */
			'use_cache' => true,
			'add_vars' => array(),
		);
		
		//new __debug( CC2_COLOR_SCHEMES_CLASS, 'active color_schemes class' );
		
		if( empty( $use_cache ) != false ) { // false = empty => FORCE refresh
			$force = true;
		}		
		
		$arrCustomParams = wp_parse_args( $arrParams, $arrDefaultParams );
		
		//new __debug( $arrParams, __METHOD__ . ': custom parameters' );
		
		extract( $arrCustomParams );
		
		//new __debug( $arrCustomParams, 'parse_less_files: params' );
		if( empty( $source_path ) && !empty( $arrCustomParams['scheme_path'] ) ) {
			$source_path = $arrCustomParams['scheme_path'] . $arrCustomParams['file'];
		}
	
	
		$strFileExt = pathinfo( $source_path, PATHINFO_EXTENSION );
		
		if( !empty( $strFileExt ) && strtolower( $strFileExt ) == 'less' ) {
			$strOutputFile = ( empty( $output_file ) ? 'style.css' : $output_file );
			$strBaseName = basename( $strOutputFile, '.css' );
			
			//list( $less_path, $query_string ) = explode( '?', str_replace( WP_CONTENT_URL, WP_CONTENT_DIR, $src ) );
			
			// output css file name
			//$css_path = trailingslashit( $this->get_output_directory( $output_dir ) ) . "{$strBaseName}.css";
			$css_path = trailingslashit( $this->get_output_directory( $output_dir ) ) . $output_file;


			/**
			 * NOTE: Path to the less file!
			 */
			if( !empty( $source_path ) ) {
				$less_path = $source_path;
			}

			// automatically regenerate files if source's modified time has changed or vars have changed
			try {

				// initialise the parser
				$less = new lessc;

				// load the cache
				$cache_path = "{$css_path}.cache";

				//new __debug ( array( 'cache_path' => $cache_path, 'css_path' => $css_path, ), 'file and caching parameters' );

				if ( file_exists( $cache_path ) ) {
					$cache = maybe_unserialize( file_get_contents( $cache_path ) );
				}
				
				
				// vars to pass into the compiler - default @themeurl var for image urls etc...
				//$this->vars[ 'themeurl' ] = '~"' . get_stylesheet_directory_uri() . '"';
				//$this->vars[ 'lessurl' ]  = '~"' . dirname( $src ) . '"';

				/*
				if( is_array( $add_vars ) && !empty( $add_vars ) ) {
					$this->vars = apply_filters('less_vars', $add_vars, $strFileName );
				}
				
				$this->vars = apply_filters( 'less_vars', $this->vars, $strFileName );
				*/
				
				
				if( is_array( $add_vars ) && !empty( $add_vars ) ) {
					$this->vars = apply_filters('less_vars', $add_vars, $strBaseName );
				}
				
				$this->vars = apply_filters( 'less_vars', $this->vars, $strBaseName );
				
				
				//__debug::log( $this->vars, __METHOD__ );
				
			
				// If the cache or root path in it are invalid then regenerate
				if ( empty( $cache ) || empty( $cache['less']['root'] ) || ! file_exists( $cache['less']['root'] ) ) {
					$cache = array( 'vars' => $this->vars, 'less' => $less_path );
					
				}

				//__debug::log( $cache, __METHOD__ . ' - $cache' );

				// less config
				$less->setFormatter( apply_filters( 'less_compression', $this->compression ) );
				
				
				// preserveComments: has no actual function with oyejorge/less.php
				//$less->setPreserveComments( apply_filters( 'less_preserve_comments', $this->preserve_comments ) );
				
				//new __debug( $this->vars, 'actual customized less variables' );
				
				// customized less variables
				$less->setVariables( $this->vars );

				// add directories to scan for imports
				$import_dirs = apply_filters( 'less_import_dirs', $this->import_dirs );
				if ( ! empty( $import_dirs ) ) {
					foreach( (array)$import_dirs as $dir )
						$less->addImportDir( $dir );
				}

				// register and unregister functions
				foreach( $this->registered_functions as $name => $callable ) {
					$less->registerFunction( $name, $callable );
				}

				foreach( $this->unregistered_functions as $name ) {
					$less->unregisterFunction( $name );
				}

				// allow devs to mess around with the less object configuration
				do_action_ref_array( 'lessc', array( $less ) );

				// $less->cachedCompile only checks for changed file modification times
				// if using the theme customiser (changed variables not files) then force a compile
				//$force = false;
				
				//new __debug( array( 'current_vars' => $this->vars, 'cache_vars' => $cache['vars'] ), 'testing vars' );
				
				//var_dump( $this->vars !== $cache[ 'vars' ] );
				
				if ( $this->vars !== $cache[ 'vars' ] ) {
					$force = true;
				}
				
				
				/**
				 * TODO: Use cache / use it not (use_cache != false)
				 */

				
				
				$less_cache = $less->cachedCompile( $cache[ 'less' ], apply_filters( 'less_force_compile', $force ) );
			
	

				if ( empty( $cache ) || empty( $cache[ 'less' ][ 'updated' ] ) || $less_cache[ 'updated' ] > $cache[ 'less' ][ 'updated' ] || $this->vars !== $cache[ 'vars' ] || !empty($force) ) {
				
				
					
					// add cache dir, if not already existing
					$cache_dir = str_replace( basename( $cache_path ), '', $cache_path );
					
					if( !file_exists( $cache_dir ) ) {
						mkdir( $cache_dir, 0777, true );
					}

					file_put_contents( $cache_path, serialize( array( 'vars' => $this->vars, 'less' => $less_cache ) ) );
					
					// successfully cached
					if( file_exists( $cache_path ) ) {
						$arrReturn['cache_file'] = array(
							'success' => true,
							'timestamp' => filemtime( $cache_path ),
							'path' => $cache_path,
						);
					}
				
				
					// write css
					
					// add css path if not already existing
					$css_dir = str_replace( basename( $css_path ), '', $css_path );
					
					if( !file_exists( $css_dir ) ) {
						mkdir( $css_dir, 0777, true );
					}

					//new __debug( $css_path, __METHOD__ . ': css_path' );

					file_put_contents( $css_path, $less_cache[ 'compiled' ] );
					
					if( file_exists( $css_path ) ) {
						$arrReturn['css_file'] = array(
							'success' => true,
							'timestamp' => filemtime( $css_path ),
							'path' => $css_path,
						);
					}
					
				}
				
				if( !empty( $arrReturn ) ) {
					$return = $arrReturn;
				}
			 
					
				
			} catch ( exception $ex ) {
				wp_die( $ex->getMessage() );
			}

			
			//return add_query_arg( 'ver', $less_cache[ 'updated' ], $url );
			
			
		} else {
			//__debug::log( $arrCustomParams, __METHOD__  . ': wrong file name' );
		}
		
		return $return;
	}


	public static function get_theme_settings() {
		$return = self::prepare_theme_variables();
		
		return $return;
	}



	/**
	 * Additional variables to add to the less compilation
	 * 
	 * TODO: Turn this into something proper, for quicker access and enhancement, not the shoddy hardcoded excuse it is right now
	 * TODO: Add custom variables!
	 * 
	 * @param array $custom_theme_mod_keys	Optional. Use your own theme mod values. Mostly used by @method customizer_refresh_less().
	 */
	 
	public static function prepare_theme_variables( $arrThemeMods = array(), $color_scheme_override = false ) {
		$return = array();

		// use a different color scheme instead of the current one
		if( empty( $color_scheme_override ) != false || !is_array( $color_scheme_override ) ) {
			$color_scheme = cc2_get_current_color_scheme();
		} else {
			$color_scheme = $color_scheme_override;
		}
	
		
		
		// fetch theme mods
		foreach( $color_scheme['scheme'] as $strThemeModKey => $schemeDefaultValue ) {
			$strCurrentThemeModKey = $strThemeModKey;
			
			if( substr( $strThemeModKey, 0, 1) == '_' ) { // internal variables
				$strCurrentThemeModKey = substr( $strThemeModKey, 1);
			}
			
			if( empty( $arrThemeMods[ $strCurrentThemeModKey ] ) ) { // fallback
				$arrThemeMods[ $strCurrentThemeModKey ] = get_theme_mod( $strCurrentThemeModKey, false );
			}
		}
		
		//new __debug( $arrThemeMods, __METHOD__ . ': $arrThemeMods' );
		
	
		/**
		 * font color
		 */
		if( !empty( $color_scheme['scheme']['font_color'] ) ) {
			//$return[ 'text-color' ] = $color_scheme['scheme']['font_color']; // default
			$font_color = $color_scheme['scheme']['font_color']; // default
		}
		
		if( $arrThemeMods['font_color'] != false ) {
			$font_color = $arrThemeMods['font_color']; 
		}
		
		$return[ 'text-color' ] = self::maybe_hex( $font_color );

		/**
		 * Font family
		 */
		 
		// get available fonts
		if( empty( $return[ 'font-family-sans-serif'] ) ) {
			$return[ 'font-family-sans-serif'] = 'Ubuntu, "Helvetica Neue", Helvetica, Arial, sans-serif';
		}
		
		// default
		/**
		 * FIXME: Possibly wrong value; might to be a variable name instead of an explicit font stack string
		 */
		if( !empty( $color_scheme['scheme']['font_family'] ) ) {
			$return[ 'font-family-base' ] = $color_scheme['scheme']['font_family']; // default
		}
		
		if( $arrThemeMods['font_family'] != false && $arrThemeMods['font_family'] != 'inherit' ) {
			$return[ 'font-family-base' ] = $arrThemeMods['font_family'];
		}

		/**
		 * link color
		 */
		//$link_color = ( !empty( $color_scheme['scheme']['link_color'] ) ? $color_scheme['scheme']['link_color'] : '#f2694b' ); // default
		if( !empty( $color_scheme['scheme']['link_color'] ) ) {
			$link_color = $color_scheme['scheme']['link_color'];
		}
		
		if( $arrThemeMods['link_color'] != false ) {
			$link_color = $arrThemeMods['link_color'];
		}
		
		$return[ 'brand-primary' ] = self::maybe_hex( $link_color );
		
		/**
		 * link hover color
		 */
		//$link_hover_color = ( !empty( $color_scheme['scheme']['hover_color'] ) ? $color_scheme['scheme']['link_color'] : '#f2854b' ); // default
		if( !empty( $color_scheme['scheme']['hover_color'] ) ) {
			$link_hover_color = $color_scheme['scheme']['hover_color'];
		}
		
		
		if( $arrThemeMods['hover_color'] != false ) {
			$link_hover_color = $arrThemeMods['hover_color'];
		}
		
		$return[ 'link-hover-color' ] = self::maybe_hex( $link_hover_color );

		return $return;
	}

	
	public static function maybe_hex( $value = '' ) {
		$return = $value;
		
		if( !empty( $value ) && strpos( '#', ''. $value ) === false && ctype_xdigit( $value ) != false ) {
			$return = '#' . $value;
		}
		
		return $return;
	}


	/**
	* Get a nice handle to use for the compiled CSS file name
	*
	* @param  string $url File URL to generate a handle from
	* @return string $url Sanitized string to use for handle
	*/
	public function url_to_handle( $url ) {

		$url = parse_url( $url );
		$url = str_replace( '.less', '', basename( $url[ 'path' ] ) );
		$url = str_replace( '/', '-', $url );

		return sanitize_key( $url );
	}


	/**
	 * Get output directory
	 * 
	 * @param 	mixed 	$output_path	Optional. If set, has to be the path where the css file is going to be placed. Else it's using the one returned by get_cache_dir()
	 * @return 	string	$dir			The 
	 * 
	 */
		public function get_output_directory( $output_path = false ) {
			$return = null;
			
			$return = $this->get_output_path( $output_path );
			
			return $return;
		}
		
		public function get_output_url( $output_url = false ) {
			$return = false;
			
			$return = $this->get_output_path( $output_url, false );
			
			return $return;
		}

	public function get_output_path( $output_path = false, $isDir = true ) {
		$return = null;
		
		$isPath = !empty($isDir) ? true : false;
		
		if( !empty( $output_path ) ) {
			$return = $output_path;
			
			
			$arrPlaceholders = self::get_placeholders( $isPath );
			//new __debug( $arrPlaceholders, __METHOD__ . ': placeholders' );
			
			if( !empty( $arrPlaceholders ) ) {
				
				foreach( $arrPlaceholders as $strKeyword => $strReplacement ) {
					$return = str_replace( '%' . $strKeyword . '%', $strReplacement, $return);
				}
			}
		} else {
			$return = $this->get_cache_dir( $isPath );
		}
		
		return $return;
	}

	/**
	* Get (and create if unavailable) the compiled CSS cache directory
	*
	* @param  bool   $path If true this method returns the cache's system path. Set to false to return the cache URL
	* @return string $dir  The system path or URL of the cache folder
	*/
	public function get_cache_dir( $path = true ) {

		// get path and url info
		$upload_dir = wp_upload_dir();

		if ( $path ) {
			$dir = apply_filters( 'wp_less_cache_path', path_join( $upload_dir[ 'basedir' ], 'wp-less-cache' ) );
			// create folder if it doesn't exist yet
			wp_mkdir_p( $dir );
		} else {
			$dir = apply_filters( 'wp_less_cache_url', path_join( $upload_dir[ 'baseurl' ], 'wp-less-cache' ) );
		}

		return rtrim( $dir, '/' );
	}


	/**
	* Escape a string that has non alpha numeric characters variable for use within .less stylesheets
	*
	* @param  string $str The string to escape
	* @return string $str String ready for passing into the compiler
	*/
	public function sanitize_string( $str ) {

		return '~"' . $str . '"';
	}


	/**
	* Adds an interface to register lessc functions. See the documentation
	* for details: http://leafo.net/lessphp/docs/#custom_functions
	*
	* @param  string $name     The name for function used in the less file eg. 'makebluer'
	* @param  string $callable (callback) Callable method or function that returns a lessc variable
	* @return void
	*/
	public function register( $name, $callable ) {
		$this->registered_functions[ $name ] = $callable;
	}

	/**
	* Unregisters a function
	*
	* @param  string $name The function name to unregister
	* @return void
	*/
	public function unregister( $name ) {
		$this->unregistered_functions[ $name ] = $name;
	}


	/**
	* Add less var prior to compiling
	*
	* @param  string $name  The variable name
	* @param  string $value The value for the variable as a string
	* @return void
	*/
	public function add_var( $name, $value ) {
		if ( is_string( $name ) ) {
			$this->vars[ $name ] = $value;
		}
	}

	/**
	* Removes a less var
	*
	* @param  string $name Name of the variable to remove
	* @return void
	*/
	public function remove_var( $name ) {
		if ( isset( $this->vars[ $name ] ) )
			unset( $this->vars[ $name ] );
	}
} // END class



    //add_filter('cc2_style_css', 'cc2_add_style_less');


/**
 * Init class for auto-parsing
 */
add_action('init', array( 'cc2pro_Less_Handler', 'init' ) );

} // endif;
