<?php
/**
 * Generic utility / helper function library
 *
 * @author Fabian Wolf
 * @since 2.0.11
 * @package cc2pro
 */
if( !function_exists('is_customizer_preview' ) ) :
	function is_customizer_preview() {
		$return = false;
		
		if( !is_admin() && isset($_POST['wp_customize']) && $_POST['wp_customize'] == 'on' && is_user_logged_in() ) {
			$return = true;
		}
		
		
		return $return;
	}
endif;

/**
 * Returns the transferred variables from the Customizer AJAX request
 */

if( !function_exists('get_customizer_request' ) ) : 
	function get_customizer_request( $specific_variable = false ) {
		$return = false;
		
		if( !empty( $_POST['customized'] ) ) {
			$customizer_request = json_decode( stripcslashes( $_POST['customized'] ), true );
			
			if( !empty( $specific_variable ) && isset( $customizer_request[ $specific_variable ] ) != false ) {
				$return = $customizer_request[ $specific_variable ];
			} else {
				$return = $customizer_request;
			}
		}
		
		return $return;
	}


endif;
