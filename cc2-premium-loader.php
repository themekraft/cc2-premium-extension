<?php
/**
 * Plugin Name:     CC2 Premium Extension
 * Plugin URI:      http://themekraft.com/store/custom-community-2-premium-features/
 * Description:     Custom Community 2 Premium settings, LESS compilation and more.
 * Version:         2.0.27
 *
 * Author:          Sven Lehnert, Konrad Sroka, Fabian Wolf
 * Author URI:      http://themekraft.com
 *
 * Text Domain:     cc2pro
 * Domain Path:     /cc2pro-languages/
 *
 * Licence:         GPLv3 (see license.txt)
 */


/**
 * do a fail-over if the theme is not activated
 * 
 * Strongy inspired by the implementation in WP Maintenance Mode by Frank Bültge (@link https://github.com/bueltge/WP-Maintenance-Mode)
 * @author Fabian Wolf
 * @since 0.1
 */
 


// Define the CC2 constant
if( !defined('CC2_PREMIUM_EXTENSION' ) ) {
	define( 'CC2_PREMIUM_EXTENSION', '2.0.27' );
}

// Define the LESSPHP constant to show that CC2 LESSPHP is working
if( !defined( 'CC2_LESSPHP' ) ) {
	define( 'CC2_LESSPHP', true );

}



define('CC2_PLUGIN_PATH', plugin_dir_path( __FILE__ ));

if( !defined('CC2_THEME_CONFIG') ) {
	if( file_exists( get_template_directory() . '/includes/theme-config.php' ) ) {
		include_once get_template_directory() . '/includes/theme-config.php';
	}
}

// include new enhanced debug class
include( trailingslashit( CC2_PLUGIN_PATH ) . 'includes/debug.class.php' );

/**
 * Redirect to CC2 Premium admin page on plugin activation
 *
 * @author Konrad Sroka
 * @package cc2_pro
 * @since 0.0.1
 */

// only when plugin is just activated, call the function cc2pro_activate()
register_activation_hook(__FILE__, array( 'cc2pro_Premium', 'plugin_activation' ) );
register_deactivation_hook(__FILE__, array( 'cc2pro_Premium', 'plugin_deactivation' ) );

/**
 * FIXME: Is going to fail, because the redirect happens right afterwards - thou admin_init does still run, redirect changes seem to be fruitless.
 * @see http://codex.wordpress.org/Function_Reference/register_activation_hook#Process_Flow
 */


//add_action('after_setup_theme', array( 'cc2pro_Premium', 'theme_check' ), 10);
add_action('plugins_loaded', array( 'cc2pro_Premium', 'theme_check' ), 10);

// hook the redirect function to init...
add_action('admin_init', array( 'cc2pro_Premium', 'after_activation_redirect' ));

// add hooks
//if( cc2pro_Premium::is_theme_active() != false ) {
require_once( plugin_dir_path(__FILE__ ) . 'hooks.php' );
//}


// helper class

class cc2pro_Premium {
	public static $message_count = 0;
	public static $_is_theme_active = false;
	public static $error_message_set = false;
	
	/**
	 * Test, whether the theme is active or not.
	 */
	public static function theme_check() {
		static $theme_tested = false;
		if( !self::is_theme_active( false ) ) {
			if( !get_option('cc2pro_do_activation_redirect', false) ) {
				self::add_error_message();
			}
			return;
		} else {
			self::$_is_theme_active = true;
		}
		
		self::load_required_resources();
		self::init_assets();
	}
	
		/**
	 * @static
	 * @var    \wp_less Reusable object instance.
	 */
	protected static $instance = null;


	/**
	 * Creates a new instance. Called on 'after_setup_theme'.
	 * May be used to access class methods from outside. Both global AND once-instanced
	 *
	 * @see    __construct()
	 * @static
	 * @return \wp_less
	 */
	public static function init() {
		null === self::$instance AND self::$instance = new self;
		
		return self::$instance;
	}
	
	

	function __construct() {
		if( self::is_theme_active() != false ) {
			self::load_required_resources();
		}
		
	}
	
	public static function init_assets() {
		add_filter('is_cc2_admin_page_known_suffixes', array( 'cc2pro_Premium', 'filter_known_admin_pages') );
		
	}
	
	public static function filter_known_admin_pages( $known_suffixes ) {
		$return = $known_suffixes;
		
		$arrKnownProSuffixes = array(
			'cc2-tools',
			'cc2-premium',
		);
		
		
		if( !empty( $known_suffixes ) ) {
			if( !is_array( $known_suffixes ) ) {
				$return = array( $known_suffixes );
			}
		} else {
			$return = array();
		}
		
		$return = array_merge( $return, $arrKnownProSuffixes );
		
		return $return;
	}
	
	public static function is_admin_page( $admin_page = null ) {
		$return = false;
		
		if( !empty( $admin_page ) ) {
			if( is_admin() && basename( $_SERVER['PHP_SELF'] ) == $admin_page ) {
				$return = true;
			}
		} else {
			$return = is_admin();
		}
		
		return $return;
	}
	
	public static function _load_deactivation_resources( $load_less = true ) {
		
		
		// load generic helper functions
		include_once ( plugin_dir_path( __FILE__ ) . '/includes/helpers.php' );
		include_once( get_template_directory() . '/includes/extras.php' );
		
		// load extended color scheme class
		require_once( plugin_dir_path(__FILE__ ). '/includes/color-schemes.class.php' );
		
		
		// load related functions
		require_once( get_template_directory() . '/includes/schemes/libs/functions.php' );
		
		// load LESS-related libs
	
		if( !empty( $load_less ) ) {
			if( !class_exists('lessc') ) {
				require plugin_dir_path( __FILE__ ) . '/includes/resources/oyejorge-lessphp/lessc.inc.php';
			}
			
			if( !class_exists('cc2pro_Less_Handler') ) {
				require plugin_dir_path( __FILE__ ) . '/includes/less-handler.php';
			}
		}
	}
	
	public static function load_required_resources( ) {
		//new __debug( $page_suffix, 'cc2pro_loader: page_suffix' );
		
		//new __debug( $_SERVER['PHP_SELF'], 'load_required_resources' ); //=> /themekraft/wp-admin/customize.php
		if( empty( self::$_is_theme_active ) || self::$_is_theme_active === false ) {
			return;
		}
		
		// load generic helper functions
		include_once ( get_template_directory(). '/includes/extras.php' );
		include_once ( plugin_dir_path( __FILE__ ) . '/includes/helpers.php' );
		
		// load extended color scheme class
		require_once( plugin_dir_path(__FILE__ ). '/includes/color-schemes.class.php' );
		
		// load related functions
		require_once( get_template_directory() . '/includes/schemes/libs/functions.php' );
		
		
		// load LESS-related libs
	
		require plugin_dir_path( __FILE__ ) . '/includes/resources/oyejorge-lessphp/lessc.inc.php';
		//require plugin_dir_path( __FILE__ ) . '/includes/lessphp.php';
		require plugin_dir_path( __FILE__ ) . '/includes/less-handler.php';
		
		/*
		if( is_customizer_preview() != false ) {
			require plugin_dir_path( __FILE__ ) . '/includes/less-customizer-preview.php';
		}*/
		
		/**
		 * TODO: Test if is_customizer_preview() is required as a condition or if it needs to be included no matter what
		 */
		
		//require plugin_dir_path( __FILE__ ) . '/includes/less-customizer-preview.php';
		//require plugin_dir_path( __FILE__ ) . '/includes/resources/lessphp/lessc.inc.php';
		
		
		
		
		// load the basic admin settings
		require_once( get_template_directory() . '/includes/admin/base.php' );
		
		// load extended customizer controls
		require_once( plugin_dir_path(__FILE__ ) . '/includes/admin/customizer-extensions.php' );
		
		// Load the premium theme options for the Customizer
		require_once( plugin_dir_path( __FILE__ ) . '/includes/admin/cc2-dashboard.php');

		// Load the Customizer options
		require plugin_dir_path( __FILE__ ) . '/includes/admin/customizer-premium-options.php';

		

		// Load the Customizer generated styles for the frontend & customizer preview
		require plugin_dir_path( __FILE__ ) . '/style.php';

		// Load the TK Extentions
		
		if( is_admin() ) {
			require plugin_dir_path( __FILE__ ) . '/includes/admin/cc2-tools.php';

			// load additional admin page settings
			require_once( plugin_dir_path( __FILE__ ) . '/includes/admin/cc2pro-additional-settings.php' );
		}
			

		// suggest https://github.com/filipstefansson/bootstrap-3-shortcodes instead

	}

	
	// now set a variable so we can check later if it was really just activated
	/**
	 * TODO: Add a theme activation check and pester the user if the theme is NOT activated.
	 */
	
	public static function plugin_activation() {
		//__debug::log( 'plugin is being activated' );
		
		
		//echo '<h1>Plugin is being activated!</h1>';
		if( !class_exists( 'cc2_Helper' )  ) {
			require_once( get_template_directory() . '/includes/extras.php' );
		}
		
		// check + import backed up settings
		if( class_exists( 'cc2_Helper' ) != false && method_exists( 'cc2_Helper', 'has_settings_backup') != false ) {
			if( cc2_Helper::has_settings_backup() != false ) {
				$arrSettingsBackup = cc2_Helper::get_settings_backup();
				
				/**
				 * FIXME: Missing import!
				 */
				
				if( !empty( $arrSettingsBackup ) && !empty( $arrSettingsBackup['color_scheme'] ) ) { // import color scheme backup
					
				}
			}
		}
		
		
		self::prepare_color_scheme_enviroment();
		
		add_option('cc2pro_do_activation_redirect', true);
		
	}
	
	public static function plugin_deactivation() {
		//__debug( 'plugin deactivation fires' );
		//error_log( 'plugin deactivation fires', 3 );
		$bIsThemeActive = self::is_theme_active();
		
		self::_load_deactivation_resources( $bIsThemeActive );
		
		if( !empty($bIsThemeActive) ) {
			//error_log( 'theme seems still to be active!', 3 );
			self::prepare_less_static_mode( true );
			
			// backup theme + plugin settings
			
		} else {
			//error_log( 'theme IS NOT active!', 3 );
		}
		
		//__debug::log( 'plugin deactivation shutdown');
	}
	
	

	/**
	 * @link http://de1.php.net/manual/en/function.is-writable.php#115437
	 */

	public static function is_writable_directory( $path ) {
		$return = false;
		$path = trim($path);
		
		if ( !empty($path) && is_dir($path) && is_writable($path) ) {
			$tmpfile = 'mPC_'.uniqid(mt_rand()).'.writable';
			
			$fullpathname = str_replace('//','/', $path . '/' . $tmpfile);
			
			$fp = @fopen($fullpathname,'w');
			if ($fp !== false) {
				$return = true;
			}
			@fclose($fp);
			
			if( file_exists( $fullpathname ) ) {
				@unlink($fullpathname);
			}
		}
		return $return;

	}


	// also add check for file permissions

	public static function check_file_permissions() {
		$return = false;
		
		$upload_dir = wp_upload_dir();
		
		$strCheckDir = trailingslashit($upload_dir['basedir']) . 'cc2/schemes'; // might not exist at that point
		
		if( self::is_writable_directory( $strCheckDir ) != false ) {
			$return = true;
		} else { // does not exist or is not writable, so check the base upload dir instead
			$return = self::is_writable_directory( $upload_dir['basedir'] );
		}
			
		return $return;
		
	}

	public function _check_file_permissions() {
		$return = false;

		$upload_dir = wp_upload_dir();
		$strCheckDir = trailingslashit($upload_dir['basedir']) . 'cc2/schemes';
		
		// get file permissions for the upload directory
		if( !file_exists( $strCheckDir ) && file_exists( $upload_dir['basedir']) != false ) {
			$strCheckDir = $upload_dir['basedir'];
		} elseif( !file_exists( $upload_dir['basedir'] ) ) {
			$strCheckDir = false;
		}
		
		
		if( !empty( $strCheckDir ) ) {

			$strFilePerms = substr( sprintf('%o', fileperms( $strCheckDir) ), -4 );

		}
		
		
		return $return;
	}

	public static function init_options() {
		$upload_dir = wp_upload_dir();
		
		// cc2-tools
		$arrDefaultToolsSettings = array(
			'mode' => 'static',
			'output_path' => $upload_dir['basedir'] . '/cc2/',
			'output_file' => 'style.css',
		);
		
		if( get_option( 'cc2_tools_options', false ) == false ) {
			update_option( 'cc2_tools_options', $arrDefaultToolsSettings );
		}
		
		// theme mods
		if( get_theme_mod('color_scheme', false ) == false ) { // or empty
			set_theme_mod('color_scheme', 'default');
		}
		
		// disable compiled stylesheet redirect
		if( get_option( 'cc2_stylesheet_file', false ) != false ) {
			delete_option( 'cc2_stylesheet_file' );
		}
	}
	
	public static function update_setting( $strOptionName = false, $strSetting = false, $value ) {
		$return = false;
		
		if( !empty( $strOptionName ) && !empty( $strSetting ) ) {
		
			$option = get_option( $strOptionName, false );
			
			if( !empty( $option ) ) { // false == empty
				$arrOption = $option;
			}
			
			$arrOption[ $strSetting ] = $value;
			
			$return = update_option( $strOptionName, $arrOption );
		}
		
		return $return;
	}
	
	/**
	 * Load the initial theme settings for the premium plugin
	 * NOTE: Located at (plugin_directory)/includes/settings
	 * FIXME: Doesn't do anything at this point. Obsolete?
	 */
	
	/*
	function load_initial_theme_settings() {
		$return = false;
		
		// load theme_mods first
		if( file_exists( plugin_dir_path( __FILE__ ) . '/includes/settings/default-customizer.php' ) ) {
			include( plugin_dir_path( __FILE__ ) . '/includes/settings/default-customizer.php' );
			
			if( !empty( $arrSettings ) ) {
				$arrCustomizerSettings = $arrSettings;
				
				// check if there are already theme mods
				if( get_option( 'color_scheme', false ) == false || get_option('cc2_theme_options', false) == false ) { // probably none yet
					
					// check the rest
					
					 //Syntax: array('theme_mod_key' => array('default' => value, 'description' => 'short description', 'type' => 'optional type' )
					 
					
					
					
					foreach( $arrCustomizerSettings as $strModKey => $arrSettingMeta ) {
						
					}
					
				}
			}
			
		}
		
		// then the regular options (ie. advanced settings, less generator settings, etc.)
		
		
	}*/
	
	// initialize default scheme + add the required directories on plugin activation (if required)
	
	
	
	public static function prepare_color_scheme_enviroment() {
		
		
		self::init_options();

		
		$upload_dir = wp_upload_dir();
		$strDefaultDir = trailingslashit($upload_dir['basedir']) . 'cc2/schemes';
		
		if( self::check_file_permissions() != false ) {
			// create required directories
			if( !file_exists( $strDefaultDir ) ) {
				mkdir( $strDefaultDir, 0755, true );
			} 
			
			// copy base style.css to color schemes directory
			if( self::is_writable_directory( $strDefaultDir ) == false ) {
				chmod( $strDefaultDir, 0777 );
			}
			
			if( self::is_writable_directory( $strDefaultDir ) != false ) {
			
				copy( trailingslashit( get_stylesheet_directory() ) . 'style.css', $strDefaultDir . '/style.css' );
				copy( trailingslashit( get_stylesheet_directory() ) . 'style.css', $strDefaultDir . '/style.css.bak' );
				
				// enable dynamic mode
				self::update_setting( 'cc2_tools_options', 'mode', 'dynamic' );
				
				// compile initial less file
				
				
				
				
			} else {
				self::add_message( sprintf( __('Warning: Wrong <a href="%s">file permissions</a> for the CC2 color schemes directory (%s). The LESS compiler is probably not going to work. Please set them to something sane, like 0755, or if fails, to 0777.', 'cc2pro' ), 'http://codex.wordpress.org/Changing_File_Permissions', $upload_dir['basedir'] ) );
			}
			
		} else {
		
			
			self::add_message( sprintf( 'Warning: Wrong <a href="%s">file permissions</a> for the upload directory (%s). The LESS compiler will not be able to work. Please set them to something sane, like 0755, or 0777 - if all fails.', 'http://codex.wordpress.org/Changing_File_Permissions', $upload_dir['basedir'] )  );
			
			// exit
		}
	}
	
	/**
	 * Fires both when deactivating the plugin as well as when switching from design to productive mode.
	 * 
	 * Struct:
	 * - parse_less_file()
	 * - move style.css to style.css.bak
	 * - add @import (url .. ) to style.css
	 * - check option "use_parsed_css" => if not set, enable it
	 */
	 
	public static function prepare_less_static_mode( $plugin_deactivation = false ) {
		//__debug::log( 'prepare_less_static_mode fires' );
		
		if( !class_exists( 'cc2_Helper' ) ) {
			require( trailingslashit( get_template_directory() ) . 'includes/extras.php' );
		}
		
		global $cc2_color_schemes;
		//#1: parse_less_file()
	
	
		if( self::_refresh_css_file() != false ) {
			$current_scheme = $cc2_color_schemes->get_current_color_scheme();
			$upload_dir = wp_upload_dir();
			$strDestFilePrefix = '';
			
			
			$strSchemeDir = trailingslashit( $upload_dir['basedir']  ) . 'cc2/schemes/';
			$strThemeDir = trailingslashit( get_template_directory() );
			
			$strSchemeURL = trailingslashit( $upload_dir['baseurl'] ) . 'cc2/schemes/';
			
			$strOutputURL = trailingslashit( $current_scheme['output_url'] ) . $current_scheme['output_file'];
			
			if( !empty($plugin_deactivation) ) {
				$strDestFilePrefix = 'backup.';
			}
			
			
			$strDestSchemePath = trailingslashit( $current_scheme['output_dir'] ) . $strDestFilePrefix . $current_scheme['output_file'];
			$strDestSchemeURL = trailingslashit( $current_scheme['output_url'] ) . $strDestFilePrefix . $current_scheme['output_file'];
			
			
			// #2: backup generated scheme file
			copy( $current_scheme['output_dir'] . $current_scheme['output_file'], $strDestSchemePath );
			
			
			/**
			 * FIXME: Obsolete!
			 */
			
			// #2: backup existing style.css in theme directory
			
			/*
			if( file_exists( $strThemeDir . 'style.css' ) ) {
				if( self::is_writable_directory( $strThemeDir ) == false ) {
					chmod( $strThemeDir, 0777 );
				}
				
				copy( $strThemeDir . 'style.css', $strThemeDir . 'style.css.bak' );
			
				
				
			
				// #3a: add @import (url .. ) to style.css
				// check whether there IS already THAT import
				$strImportCSS = '@import url("' . $strOutputURL . '");';
				
				$strThemeStylesheet = file_get_contents( $strThemeDir . 'style.css' );
				
				
				if( strpos( $strThemeStylesheet, $strImportCSS ) === false ) {
					$strThemeStylesheet = $strImportCSS . "\n" . $strThemeStylesheet;
					file_put_contents( $strThemeDir . 'style.css', $strThemeStylesheet );
				}
				
			} else {
				// #3b: create new style.css + add @import (url .. ) to it
				
				$rStyleFile = fopen( $strThemeDir . 'style.css', 'w' );
				fwrite( $rStyleFile, '@import url("' . $strOutputURL . '");' );
				fclose( $rStyleFile );
			}*/


			
		}
		
		// #4: backup color scheme (path to color schemes directory + style.css or however its being called )
		$arrThemeMods = get_theme_mods();
			
	
		$arrCurrentColorScheme = $current_scheme;
		$arrCurrentColorScheme['static_stylesheet_url'] = $strDestSchemeURL;
		$arrCurrentColorScheme['static_stylesheet_path'] = $strDestSchemePath;
		
		// these are not being in use .. maybe we should reuse them for static mode?
		$arrCurrentColorScheme['style_url'] = $strDestSchemeURL;
		$arrCurrentColorScheme['style_path'] = $strDestSchemePath;
		
		$arrCurrentColorScheme['output_url'] = $strDestSchemeURL;
		//$arrCurrentColorScheme['output_file'] = $strDestSchemePath;
		$arrCurrentColorScheme['output_path'] = $strDestSchemePath;
		//$arrCurrentColorScheme['output_file'] = 
		
		$arrCurrentColorScheme['settings'] = $arrThemeMods;
		$arrCurrentColorScheme['slug'] = ( empty( $plugin_deactivation ) ? 'static__' : 'backup__' ) . $current_scheme['slug'];

		cc2_Helper::update_settings_backup( 'scheme', $arrCurrentColorScheme );
		
		
		// #5: backup (advanced) customizer settings
		if( !empty($plugin_deactivation) ) { // false = empty ^_^
			cc2_Helper::update_settings_backup( 'advanced_theme_mods', $arrThemeMods );
		
			//update_option( 'cc2_stylesheet_file', $strOutputURL );
		}
		
		// #6: change to newly created scheme
		if( cc2_Helper::has_settings_backup( 'scheme' ) != false ) {
			
			
			set_theme_mod( 'color_scheme', $arrCurrentColorScheme['slug'] );
			
			// #6.1: set generation mode to static
			
		}
		
	}
	
		public static function _refresh_css_file() {
			global $cc2_color_schemes;
			$return = false;
			
			// disable NOTICES
			error_reporting( E_ALL ^ E_NOTICE );

			if( class_exists( 'cc2pro_Less_Handler' ) ) {


				$handler = new cc2pro_Less_Handler();
				$current_scheme = $cc2_color_schemes->get_current_color_scheme();
				
				$arrParseParams = array(
					'source_path' => $current_scheme['scheme_path'] . $current_scheme['file'],
					'add_vars' => cc2pro_Less_Handler::get_theme_settings(),
				);
				
				//new __debug( $arrParseParams, 'arrParseParams' );
				
				$result = $handler->parse_stylesheet( $arrParseParams );
				
				//__debug::log( $result, 'build result' );
				if( $result != false ) {
					$return = true;
				}
			}
			
			return $return;
		}
	
	
	/**
	 * Check whether the theme is active or not, and display an appropriate error message.
	 * 
	 * @param bool $show_error 		Enables multi-purpose use. Defaults to true. Set to false to simply get a theme check.
	 * @return bool $theme_check	Returns false if theme is not active, otherwise true.
	 */
	
	public static function is_theme_active( $show_error = true ) {
		$return = true;
		$bThemeActive = false;
		
		// check if theme options are set
		
		$cc2_theme_status = get_option( 'cc2_theme_status', false );
		//new __debug( ( empty($cc2_theme_status) ? 'is empty' : $cc2_theme_status ), __METHOD__ . ': cc2_theme_status' );

		if( !empty( $cc2_theme_status ) && $cc2_theme_status == 'enabled' ) {
			$bThemeActive = true;
		} else {
			$bThemeActive = false;
		}
		
		
		/**
		 * Base values for the file check
		 * 
		 * FIXME: extras.php might DISAPPEAR in the near future!
		 */
		 
		$arrKnownThemeFiles = array(
			'/includes/extras.php',
			'/includes/schemes/libs/functions.php'
		);
		
		// check if wp_get_theme returns known values
		$current_theme = wp_get_theme();
		
		/**
		 * NOTE: version_compare fails with version like "2.0b1" or "2.0-r1", so its just a crude "check for the first 3 chars" test
		 * We also stick to the explizit "2.0" scheme, because THIS plugin version works with the current 2.0, but we wont know if it would still work with like the 2.1 or mayb 3 or whatever.
		 */
		if( is_object($current_theme) ) {
			// no child theme
			if( !self::is_child_theme() && substr( $current_theme->Version, 3 ) == '2.0' && strtolower( $current_theme->Name) == 'custom community' ) {
				$bThemeActive = true;
			} else { // possible child theme situation (sounds very much like "hostage situation" xD)
				$arrKnownThemeFiles[] = '/includes/theme-config.php';
			}
		}
		
		
		/**
		 * NOTE: file checks go only so far ..
		 */
		
		for( $n = 0; $n < sizeof( $arrKnownThemeFiles ); $n++ ) {
			if( !file_exists( get_template_directory() . $arrKnownThemeFiles[ $n ] ) ) {
				$bThemeActive = false;
			}
		}
		/*
		if( !file_exists( get_template_directory() . '/includes/extras.php' ) || 
			!file_exists( get_template_directory() . '/includes/schemes/libs/functions.php' )
		) {
			$bThemeActive = false;
		}*/
		

		// enabled = true, disabled = false
		
		//if( !defined('CC2_VERSION' ) ) {
		if( empty( $bThemeActive ) ) { // false = empty
			self::$_is_theme_active = false;
			
			if( $show_error ) {
				self::add_error_message();
			}
			
			$return = false;
		}
		
		return $return;
	}
	
	public static function add_message( $message, $type = 'error' ) {
		$strMessage = $message;
		self::$message_count++;
		$strBefore = '<p>';
		$strAfter = '</p>';
	
		// set style + classes based upon type
		switch( $type ) {
			case 'error':
				$strClass = 'error fade';
				$strMessageStyle = 'background-color: #FFEBE8 !important;';
				$strParagraphStyle = 'font-size: 1.2em; padding: 5px 10px;';
				break;
			default:
				$strClass = 'update fade';
				$strStyle = '';
				$strParagraphStyle = '';
				break;
		}
		
		$strMessage = '<div id="cc2-message' . self::$message_count . '" class="' . $strClass . '"';
		
		if( !empty( $strStyle ) ) {
			' style="' . $strStyle . '"';
		}
		
		if( !empty( $strParagraphStyle  ) ) {
			$strBefore = '<p style="' . $strParagraphStyle . '">';
			
		}
		
		
		$strMessage .= '>' . $strBefore .  $message . $strAfter . '</div>';
	
	
		// set up message
		if ( is_admin() && is_multisite() && is_plugin_active_for_network( plugin_basename( __FILE__ ) ) ) {
			
			add_action( 'network_admin_notices', create_function( '', "echo '$strMessage';" ) );
			
		}
		
	
		add_action( 'admin_notices', create_function( '', "echo '$strMessage';" ) );
		

		
	}


	public static function add_error_message() {
		if( self::$error_message_set != false ) {
			return;
		}
		
		
		// compile error message
		
		
		$message = '<div id="cc2-message" class="error fade" style="background-color: #FFEBE8 !important;"><p style="font-size: 1.2em; padding: 5px 10px;">' . __( '<strong>CC2 Premium functions are disabled</strong> - please activate CC2 before enabling this plugin!', 'cc2pro' );
		
		$message .= sprintf( 
			' &nbsp; <a href="%1$s">' . __('Change Theme', 'cc2pro' ) . '</a>' .
			' &nbsp; <a href="%2$s" style="color: #a00;">' . __('Disable Plugin', 'cc2pro' ) . '</a>', admin_url('/themes.php'), admin_url('/plugins.php' )
		);
		
		$message .= '</p></div>';
		
	
		// set uo error message
		if ( is_admin() && is_multisite() && is_plugin_active_for_network( plugin_basename( __FILE__ ) ) ) {
			add_action( 'network_admin_notices', create_function( '', "echo '$message';" ) );
		}
		
		add_action( 'admin_notices', create_function( '', "echo '$message';" ) );
	
		self::$error_message_set = true;
	}
	
	/**
	 * Reinventing the wheel .. again. Thank you very MUCH, dear WP. Late inits and similar crap :P
	 */

	public static function is_child_theme() {
		$return = false;
		
		if( defined('TEMPLATEPATH' ) && defined('STYLESHEETPATH') ) {
			$return = is_child_theme();
			
		} else {
			$parent = get_template_directory();
			$child = get_stylesheet_directory();
			
			if( $parent !== $child ) {
				$return = true;
			}
			
		}
		
		
		return $return;
	}



	// ...and here comes the redirect function
	public static function after_activation_redirect() {

		// now check if it was really just activated, otherwise redirect
		if (get_option('cc2pro_do_activation_redirect', false)) {
			delete_option('cc2pro_do_activation_redirect');
			
			$is_theme_active = self::is_theme_active();
			
			// check if it's no bulk update
			if( !isset( $_GET['activate-multi'] ) && !empty($is_theme_active) ) {
				//__debug::log('activation redirect fires');
				
				exit( wp_redirect("admin.php?page=cc2-premium") );
			}
			
			/**
			 * NOTE: Disabled for now, so that the plugin functions that rely on the theme are just not being included.
			 */
			 
			/*
			 if( !$is_theme_active ) {
				self::disable_plugin();
			}*/
		}

	}

	/**
	 * Direct update of the stored active plugins (in wp_options table)
	 * @see http://perishablepress.com/quickly-disable-or-enable-all-wordpress-plugins-via-the-database/
	 */

	public function disable_plugin() {
		$active_plugins = get_option( 'active_plugins', array() );
		
		/*echo '<pre>'.print_r( array( 'active_plugins' => $active_plugins, 'plugin_basename' => plugin_basename(__FILE__) ) , true).'</pre>';
		exit;*/

		if( in_array(  plugin_basename(__FILE__), $active_plugins ) != false ) {
			$update_active_plugins = array();
			for( $n = 0; $n < sizeof( $active_plugins ); $n++ ) {
				if( plugin_basename(__FILE__) != $active_plugins[ $n ] ) {
					$update_active_plugins[] = $active_plugins[ $n ];
				}
			}
			
			
			//print_r( $update_active_plugins );
			//exit;
			update_option( 'active_plugins', $update_active_plugins );
		}

	}
	

}


/**
 * Displays an inactive message if the API License Key has not yet been activated
 */
if ( get_option( 'api_manager_cc2_activated' ) != 'Activated' ) {
    add_action( 'admin_notices', 'API_Manager_cc2::am_cc2_inactive_notice' );
}

class API_Manager_cc2 {

    /**
     * Self Upgrade Values
     */
    // Base URL to the remote upgrade API Manager server. If not set then the Author URI is used.
    public $upgrade_url = 'http://themekraft.com/';

    /**
     * @var string
     */
    public $version = CC2_PREMIUM_EXTENSION;

    /**
     * @var string
     * This version is saved after an upgrade to compare this db version to $version
     */
    public $api_manager_cc2_version_name = 'api_manager_cc2_version';

    /**
     * @var string
     */
    public $plugin_url;

    /**
     * @var string
     * used to defined localization for translation, but a string literal is preferred
     *
     * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/issues/59
     * http://markjaquith.wordpress.com/2011/10/06/translating-wordpress-plugins-and-themes-dont-get-clever/
     * http://ottopress.com/2012/internationalization-youre-probably-doing-it-wrong/
     */
    public $text_domain = 'cc2';

    /**
     * Data defaults
     * @var mixed
     */
    private $ame_software_product_id;

    public $ame_data_key;
    public $ame_api_key;
    public $ame_activation_email;
    public $ame_product_id_key;
    public $ame_instance_key;
    public $ame_deactivate_checkbox_key;
    public $ame_activated_key;

    public $ame_deactivate_checkbox;
    public $ame_activation_tab_key;
    public $ame_deactivation_tab_key;
    public $ame_settings_menu_title;
    public $ame_settings_title;
    public $ame_menu_tab_activation_title;
    public $ame_menu_tab_deactivation_title;

    public $ame_options;
    public $ame_plugin_name;
    public $ame_product_id;
    public $ame_renew_license_url;
    public $ame_instance_id;
    public $ame_domain;
    public $ame_software_version;
    public $ame_plugin_or_theme;

    public $ame_update_version;

    public $ame_update_check = 'am_cc2_plugin_update_check';

    /**
     * Used to send any extra information.
     * @var mixed array, object, string, etc.
     */
    public $ame_extra;

    /**
     * @var The single instance of the class
     */
    protected static $_instance = null;

    public static function instance() {

        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Cloning is forbidden.
     *
     * @since 1.2
     */
    private function __clone() {}

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.2
     */
    private function __wakeup() {}

    public function __construct() {

        // Run the activation function
        register_activation_hook( __FILE__, array( $this, 'activation' ) );

        // Ready for translation
        //load_plugin_textdomain( $this->text_domain, false, dirname( untrailingslashit( plugin_basename( __FILE__ ) ) ) . '/languages' );

        if ( is_admin() ) {

            // Check for external connection blocking
            add_action( 'admin_notices', array( $this, 'check_external_blocking' ) );

            /**
             * Software Product ID is the product title string
             * This value must be unique, and it must match the API tab for the product in WooCommerce
             */
            $this->ame_software_product_id = 'CC-2-Premium';

            /**
             * Set all data defaults here
             */
            $this->ame_data_key 				= 'cc2-premium-extension';
            $this->ame_api_key 					= 'api_key';
            $this->ame_activation_email 		= 'activation_email';
            $this->ame_product_id_key 			= 'api_manager_cc2_product_id';
            $this->ame_instance_key 			= 'api_manager_cc2_instance';
            $this->ame_deactivate_checkbox_key 	= 'api_manager_cc2_deactivate_checkbox';
            $this->ame_activated_key 			= 'api_manager_cc2_activated';

            /**
             * Set all admin menu data
             */
            $this->ame_deactivate_checkbox 			= 'am_deactivate_cc2_checkbox';
            $this->ame_activation_tab_key 			= 'api_manager_cc2_dashboard';
            $this->ame_deactivation_tab_key 		= 'api_manager_cc2_deactivation';
            $this->ame_settings_menu_title 			= 'CC2 License';
            $this->ame_settings_title 				= 'CC2 Premium License';
            $this->ame_menu_tab_activation_title 	= __( 'License Activation', 'api-manager-cc2' );
            $this->ame_menu_tab_deactivation_title 	= __( 'License Deactivation', 'api-manager-cc2' );

            /**
             * Set all software update data here
             */
            $this->ame_options 				= get_option( $this->ame_data_key );
            $this->ame_plugin_name 			= untrailingslashit( plugin_basename( __FILE__ ) ); // same as plugin slug. if a theme use a theme name like 'twentyeleven'
            $this->ame_product_id 			= get_option( $this->ame_product_id_key ); // Software Title
            $this->ame_renew_license_url 	= 'http://themekraft.com/my-account'; // URL to renew a license. Trailing slash in the upgrade_url is required.
            $this->ame_instance_id 			= get_option( $this->ame_instance_key ); // Instance ID (unique to each blog activation)
            /**
             * Some web hosts have security policies that block the : (colon) and // (slashes) in http://,
             * so only the host portion of the URL can be sent. For cc2 the host portion might be
             * www.cc2.com or cc2.com. http://www.cc2.com includes the scheme http,
             * and the host www.cc2.com.
             * Sending only the host also eliminates issues when a client site changes from http to https,
             * but their activation still uses the original scheme.
             * To send only the host, use a line like the one below:
             *
             * $this->ame_domain = str_ireplace( array( 'http://', 'https://' ), '', home_url() ); // blog domain name
             */
            $this->ame_domain 				= str_ireplace( array( 'http://', 'https://' ), '', home_url() ); // blog domain name
            $this->ame_software_version 	= $this->version; // The software version
            $this->ame_plugin_or_theme 		= 'plugin'; // 'theme' or 'plugin'

            // Performs activations and deactivations of API License Keys
            require_once( plugin_dir_path( __FILE__ ) . 'am/classes/class-wc-key-api.php' );

            // Checks for software updatess
            require_once( plugin_dir_path( __FILE__ ) . 'am/classes/class-wc-plugin-update.php' );

            // Admin menu with the license key and license email form
            require_once( plugin_dir_path( __FILE__ ) . 'am/admin/class-wc-api-manager-menu.php' );

            $options = get_option( $this->ame_data_key );

            /**
             * Check for software updates
             */
            if ( ! empty( $options ) && $options !== false ) {

                $this->update_check(
                    $this->upgrade_url,
                    $this->ame_plugin_name,
                    $this->ame_product_id,
                    $this->ame_options[$this->ame_api_key],
                    $this->ame_options[$this->ame_activation_email],
                    $this->ame_renew_license_url,
                    $this->ame_instance_id,
                    $this->ame_domain,
                    $this->ame_software_version,
                    $this->ame_plugin_or_theme,
                    $this->text_domain
                );

            }

        }

        /**
         * Deletes all data if plugin deactivated
         */
        register_deactivation_hook( __FILE__, array( $this, 'uninstall' ) );

    }

    /** Load Shared Classes as on-demand Instances **********************************************/

    /**
     * API Key Class.
     *
     * @return Api_Manager_cc2_Key
     */
    public function key() {
        return Api_Manager_cc2_Key::instance();
    }

    /**
     * Update Check Class.
     *
     * @return API_Manager_cc2_Update_API_Check
     */
    public function update_check( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme, $text_domain, $extra = '' ) {

        return API_Manager_cc2_Update_API_Check::instance( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme, $text_domain, $extra );
    }

    public function plugin_url() {
        if ( isset( $this->plugin_url ) ) {
            return $this->plugin_url;
        }

        return $this->plugin_url = plugins_url( '/', __FILE__ );
    }

    /**
     * Generate the default data arrays
     */
    public function activation() {
        global $wpdb;

        $global_options = array(
            $this->ame_api_key 				=> '',
            $this->ame_activation_email 	=> '',
        );

        update_option( $this->ame_data_key, $global_options );

        require_once( plugin_dir_path( __FILE__ ) . 'am/classes/class-wc-api-manager-passwords.php' );

        $api_manager_cc2_password_management = new API_Manager_cc2_Password_Management();

        // Generate a unique installation $instance id
        $instance = $api_manager_cc2_password_management->generate_password( 12, false );

        $single_options = array(
            $this->ame_product_id_key 			=> $this->ame_software_product_id,
            $this->ame_instance_key 			=> $instance,
            $this->ame_deactivate_checkbox_key 	=> 'on',
            $this->ame_activated_key 			=> 'Deactivated',
        );

        foreach ( $single_options as $key => $value ) {
            update_option( $key, $value );
        }

        $curr_ver = get_option( $this->api_manager_cc2_version_name );

        // checks if the current plugin version is lower than the version being installed
        if ( version_compare( $this->version, $curr_ver, '>' ) ) {
            // update the version
            update_option( $this->api_manager_cc2_version_name, $this->version );
        }

    }

    /**
     * Deletes all data if plugin deactivated
     * @return void
     */
    public function uninstall() {
        global $wpdb, $blog_id;

        $this->license_key_deactivation();

        // Remove options
        if ( is_multisite() ) {

            switch_to_blog( $blog_id );

            foreach ( array(
                          $this->ame_data_key,
                          $this->ame_product_id_key,
                          $this->ame_instance_key,
                          $this->ame_deactivate_checkbox_key,
                          $this->ame_activated_key,
                      ) as $option) {

                delete_option( $option );

            }

            restore_current_blog();

        } else {

            foreach ( array(
                          $this->ame_data_key,
                          $this->ame_product_id_key,
                          $this->ame_instance_key,
                          $this->ame_deactivate_checkbox_key,
                          $this->ame_activated_key
                      ) as $option) {

                delete_option( $option );

            }

        }

    }

    /**
     * Deactivates the license on the API server
     * @return void
     */
    public function license_key_deactivation() {

        $activation_status = get_option( $this->ame_activated_key );

        $api_email = $this->ame_options[$this->ame_activation_email];
        $api_key = $this->ame_options[$this->ame_api_key];

        $args = array(
            'email' => $api_email,
            'licence_key' => $api_key,
        );

        if ( $activation_status == 'Activated' && $api_key != '' && $api_email != '' ) {
            $this->key()->deactivate( $args ); // reset license key activation
        }
    }

    /**
     * Displays an inactive notice when the software is inactive.
     */
    public static function am_cc2_inactive_notice() { ?>
        <?php if ( ! current_user_can( 'manage_options' ) ) return; ?>
        <?php if ( isset( $_GET['page'] ) && 'api_manager_cc2_dashboard' == $_GET['page'] ) return; ?>
        <div id="message" class="error">
            <p><?php printf( __( 'The API Manager cc2 API License Key has not been activated, so the plugin is inactive! %sClick here%s to activate the license key and the plugin.', 'api-manager-cc2' ), '<a href="' . esc_url( admin_url( 'options-general.php?page=api_manager_cc2_dashboard' ) ) . '">', '</a>' ); ?></p>
        </div>
    <?php
    }

    /**
     * Check for external blocking contstant
     * @return string
     */
    public function check_external_blocking() {
        // show notice if external requests are blocked through the WP_HTTP_BLOCK_EXTERNAL constant
        if( defined( 'WP_HTTP_BLOCK_EXTERNAL' ) && WP_HTTP_BLOCK_EXTERNAL === true ) {

            // check if our API endpoint is in the allowed hosts
            $host = parse_url( $this->upgrade_url, PHP_URL_HOST );

            if( ! defined( 'WP_ACCESSIBLE_HOSTS' ) || stristr( WP_ACCESSIBLE_HOSTS, $host ) === false ) {
                ?>
                <div class="error">
                    <p><?php printf( __( '<b>Warning!</b> You\'re blocking external requests which means you won\'t be able to get %s updates. Please add %s to %s.', 'api-manager-cc2' ), $this->ame_software_product_id, '<strong>' . $host . '</strong>', '<code>WP_ACCESSIBLE_HOSTS</code>'); ?></p>
                </div>
            <?php
            }

        }
    }

} // End of class

function cc2_AME() {
    return API_Manager_cc2::instance();
}

// Initialize the class instance only once
cc2_AME();


