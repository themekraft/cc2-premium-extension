<?php
/**
 * Additional filters and actions for the premium extension
 */
 
 
if( !class_exists('cc2pro_HookAdditions' ) ) :
	class cc2pro_HookAdditions {
		
		/**
		 * Plugin instance.
		 *
		 * @see get_instance()
		 * @type object
		 */
		protected static $instance = NULL;
			
		/**
		 * Implements Factory pattern
		 * Strongly inspired by WP Maintenance Mode of Frank Bueltge ^_^ (@link https://github.com/bueltge/WP-Maintenance-Mode)
		 * 
		 * Access this plugins working instance
		 *
		 * @wp-hook after_setup_theme
		 * @return object of this class
		 */
		public static function get_instance() {

			NULL === self::$instance and self::$instance = new self();

			return self::$instance;
		}
		
		function __construct( $arrCustomParams = false ) {
			$this->arrSettings = array(
				'scroll_top' => get_theme_mod('bootstrap_scroll_top', false ),
			);
			
			// admin hooks
			$this->init_admin_hooks();
			
			// do NOT fire admin hooks if the theme is disabled
			if( cc2pro_Premium::is_theme_active() != false ) {
			
				// customizer hooks
				$this->init_customizer_hooks();
				
				
				// frontend + generic hooks
				$this->init_hooks();

			}
			// add custom customizer frontend hook (only active when customizer is in use)
			//add_action( 'init', array( $this, 'customizer_frontend_init' ) );
			
			// customizer frontend hooks
			//add_action( 'customizer_frontend_parse_query', array( $this, 'parse_customizer_ajax_request' ) );
			
		}
		
		function init_customizer_hooks() {
			// scripts
			add_action( 'customize_controls_enqueue_scripts', array( $this, 'load_customizer_scripts' ) );
			
			// set up actions
			add_action( 'customize_preview_init', array( $this, 'customizer_preview_init' ) );	
		}
		
		function init_hooks() {
			/**
			 * hook into init for customizer preview (frontend)
			 * NOTE: less_handler hooks also into init, but uses none - aka the default - priority (10). We want to run afterwards to replace the stylesheet URL if required.
			 */
	
			add_action( 'init', array( $this, 'customizer_preview_frontend_init' ), 11 );
			add_action( 'cc2_style_css', array( $this, 'customizer_preview_style_url' ), 11 );
			
			
			// others
			add_filter( 'cc2_display_footer', array( $this, 'filter_display_footer' ) );
			
			add_filter( 'cc2_bootstrap_scripts_handlers', array( $this, 'adjust_bootstrap_scripts_queue' ) );
			
			add_filter( 'cc2_scroll_top_link_container_height', array( $this, 'scroll_top_link_container_height' ) );
			add_filter( 'cc2_scroll_top_link_container_offset', array( $this, 'scroll_top_link_container_offset' ) );
			add_filter( 'cc2_scroll_top_link_container_position', array( $this, 'scroll_top_link_container_position' ) );
			
			add_filter( 'cc2_scroll_top_button_icon_class', array( $this, 'scroll_top_button_icon_class' ) );
			add_filter( 'cc2_scroll_top_button_text', array( $this, 'scroll_top_button_text' ) );
			
			// actual link
			add_filter( 'cc2_scroll_top_button_link', array( $this, 'scroll_top_button_link' ) );
			
			// custom script hooks
			add_action( 'wp_head', array( $this, 'custom_script_wp_head' ), 999 );
			add_action( 'wp_footer', array( $this, 'custom_script_wp_footer'), 999 );
				
		}
		
		/**
		 * These hooks should fire only on admin_init
		 */
		
		function init_admin_hooks() {
			// ajax hooks
			/**
			 * NOTE: is_admin() DOES NOT WORK properly with ajax hooks!
			 */
			
			
			// regular admin hooks
			if( !is_admin() ) {
				return;
			}
			add_action( 'cc2_support_add', array( $this, 'add_support_button') );
			
			add_filter( 'cc2_get_support_url', array( $this, 'filter_get_themekraft_support_url' ) );
		}
		
		
		/**
		 * NOTE: fires too late
		 */
		 
		/*
		function customizer_frontend_init() {
			if( isset($_POST['wp_customize']) && $_POST['wp_customize'] == 'on' ) {
				__debug::log( __METHOD__ 'fires' );
				
				do_action( 'customizer_frontend_parse_query' );
			
			}
		}
		
		function parse_customizer_ajax_request() {
			//new __debug( $_POST, __METHOD__ .' fires');
			
			if(  !empty( $_POST['customized'] ) ) {
				$customizer_vars = apply_filters( 'customizer_frontend_query_vars', json_decode( stripcslashes( $_POST['customized'] ), true ) );
			}
			
			
		}*/
		
		function load_customizer_scripts() {
			
		}
		
		function customizer_preview_init() {
		
			$tk_customizer_options = get_option('tk_customizer_options');
			
			if(isset( $tk_customizer_options['customizer_disabled'])) {
				return;
			}
			
			/*wp_enqueue_script('consoledummy');
			
			wp_enqueue_script('jquery');

			// load animate.css
			wp_enqueue_style( 'cc-animate-css');
			wp_enqueue_script( 'tk_customizer_preview_js',	get_template_directory_uri() . '/includes/admin/js/customizer.js', array( 'jquery', 'customize-preview' ), '', true );*/
		}
		
		/**
		 * This should happily hook into the frontend view of the customizer preview. And hopefully help us to get the LESS compiler jump-started. With all that variables transferred via AJAX, of corpse.
		 * 
		 * $_POST['customized'] should contain all important variables
		 */
		
		function customizer_preview_frontend_init() {
			//$menus = wp_get_nav_menus();
			
			//new __debug( $menus, 'all available menus' );
			 
		}
		
		function customizer_preview_style_url( $url = '' ) {
			global $less_handler;
			$return = $url;
			//$is_dynamic = true; // fallback for static mode AND backup schemes (ie. DO NOT process backup schemes)
			$less_mode = 'dynamic';
			
			$cc2_tools_options = get_option( 'cc2_tools_options', false);
			
			
			if( !empty( $cc2_tools_options ) && !empty( $cc2_tools_options['mode'] ) ) {
				$less_mode = $cc2_tools_options['mode'];
			}
			
			$strParsedToken = 'bypassed=' . __FUNCTION__;
			
			// get current compiler mode
			
			
			
			if( is_customizer_preview() != false && method_exists($less_handler, 'parse_less_file' ) ) {
				//new __debug( 'less_handler should be functioning' );
				
				// req(ested) Color Scheme
				
				//new __debug( json_decode( stripcslashes( $_POST['customized'] ), true ), '$customized' );
				
				
				if( empty( $_POST['customized'] ) != false ) { // initial call usually does NOT contain the $customized variables
					$requested_color_scheme = cc2_get_current_color_scheme();
					if( !isset( $requested_color_scheme['scheme'] ) ) { // either default scheme or not yet set
						$requested_color_scheme = cc2_get_color_scheme_by_slug('default');
					}
					
					$method = 'cc2_get_current_color_scheme';
				} else {
					/**
					 * FIXME: Missing initial settings when doing the FIRST call ever.
					 */
					
					$arrCustomizerVars = get_customizer_request();
					
					if( empty( $arrCustomizerVars['color_scheme'] ) != false ) {
						$current_color_scheme = cc2_get_current_color_scheme();
						
						$current_color_scheme_slug = get_theme_mod( 'color_scheme', ( empty($current_color_scheme['slug']) ? false : $current_color_scheme['slug'] ) );
						
						
						//new __debug( $current_color_scheme, 'is_customized: current color scheme' );
						
						if( !empty( $current_color_scheme_slug ) ) {
							$arrCustomizerVars['color_scheme'] = $current_color_scheme_slug;
						} else { // if ALL should fail ..
							$arrCustomizerVars['color_scheme'] = 'default';
						}
					}
					
					$requested_color_scheme = cc2_get_color_scheme_by_slug( $arrCustomizerVars['color_scheme'] );

					$method = 'cc2_get_color_scheme_by_slug';
				}
				
				
				
				
				// is stylesheet of a backup?
				if( !empty($requested_color_scheme['_modified']) || stripos( $requested_color_scheme['output_file'], 'backup.' ) !== false ) {
					//$is_dynamic = false;
					$less_mode = 'backup';
					
					if( isset( $requested_color_scheme['style_url'] ) ) {
						$return = $requested_color_scheme['style_url'];
					}
					
				}
					
				
				
				//new __debug( array( 'color_scheme_data' => $requested_color_scheme, 'method' => $method, 'slug' => $slug ), 'requested color_scheme' );
				
				
				if( $less_mode == 'dynamic' ) {
				
				
					$arrCompileParams = $requested_color_scheme;
					
					$arrCompileParams['source_path'] = $requested_color_scheme['scheme_path'] . $requested_color_scheme['file'];
					
					/**
					 * FIXME: Breaks when using the backup scheme
					 *
					 */
					 // http://mintoo/wp-plain-vanilla/wp-content/uploads/cc2/schemes/backup.greenthumb.csscustomizer./opt/lampp/htdocs/wp-plain-vanilla/wp-content/uploads/cc2/schemes/backup.greenthumb.css?processed_with=customizer_preview_style_url&ver=3.9.2
					
					$arrCompileParams['output_file'] = 'customizer.'.$requested_color_scheme['output_file'];
					
					
					// prepare theme_mods override
					$arrThemeModsOverride = array(); 
					//new __debug( $requested_color_scheme, __METHOD__ . ': before foreach for preparing the ThemeModsOverride' );
					
					foreach( $requested_color_scheme['scheme'] as $strBaseVar => $value ) {
					
						if( isset( $arrCustomizerVars[ $strBaseVar ] ) ) {
							$arrThemeModsOverride[ $strBaseVar ] = $arrCustomizerVars[ $strBaseVar ];
						}
					}
					
					
					// prepare the additional variables
					$arrCompileParams['add_vars'] = cc2pro_Less_Handler::prepare_theme_variables( $arrThemeModsOverride, $requested_color_scheme );
					
					//$arrCompileParams['add_vars'] = $this->get_theme_settings();
					
					
					// add scheme info to the environment			
					
					$arrCompileParams['add_vars']['scheme_slug'] = $requested_color_scheme['slug'];
					
					
					$result = $less_handler->parse_less_file( $arrCompileParams );
				
					//new __debug( $arrCompileParams, 'compile parameters' );
				
					if( !empty( $result ) && is_array( $result ) ) {
						$return = $arrCompileParams['output_url'] . $arrCompileParams['output_file'];
					}
					
					
					
					//new __debug( array( 'return' => $return, 'arrQueryParams' => $arrCompileParams ) );
					
					$strParsedToken = 'processed_with=' . __FUNCTION__;
					$return .= (strpos( $return, '?' ) !== false ? '&' : '?' ) . $strParsedToken;
				}
			}
				
			// $return .= (strpos( $return, '?' ) !== false ? '&' : '?' ) . $strParsedToken;
			
			return $return;
		}
		
		function filter_get_themekraft_support_url( $url ) {
			$return = $url;
			
			
			return $return;
		}
		
		
		

		public function add_support_button() {
			
			if( !file_exists( plugin_dir_path(__FILE__ ) . 'includes/admin/templates/button-support.php' ) ) :
			?><a class="button button-primary" onclick="script: Zenbox.show(); return false;" href="#" title="Submit an email support ticket and get personal help now!" style="margin-right: 3px;"><?php _e('Submit Support Ticket', 'cc2pro' ); ?></a><?php
			else:
				include( plugin_dir_path(__FILE__ ) . 'includes/admin/templates/button-support.php' );
			endif;
		}

		public function filter_display_footer( $value ) {
			$return = $value;
			
			$display_footer = get_theme_mod( 'display_footer', 'nope' );
			
			if( $display_footer != 'nope' && is_bool( $display_footer ) ) {
				$return = $display_footer;
				
				//$return = ( !is_bool( $display_footer ) ? (boolean) $display_footer : $display_footer );
			}
			
			return $return;
		}

		
		function adjust_bootstrap_scripts_queue( $arrQueue ) {
			$return = $arrQueue;
			
			$cc2_advanced_settings = get_option( 'cc2_advanced_settings', false );
			
			if( !empty( $cc2_advanced_settings ) && isset( $cc2_advanced_settings['enqueue_bootstrap_scripts'] ) ) {
				foreach( $return as $iCount => $strScriptHandler ) {
					if( !in_array( $strScriptHandler, $cc2_advanced_settings['enqueue_bootstrap_scripts'] ) ) {
						unset( $return[ $iCount ] );
					}
				}
			}
			
			return $return;
		}
		
		
		/**
		 * Custom scripts in several places
		 */
		function custom_script_wp_head() {
			$this->load_custom_script_data( 'header');
		}
		
		function custom_script_wp_footer() {
			$this->load_custom_script_data( 'footer');
		}
		
		function load_custom_script_data( $position ) {
			$advanced_settings = get_option( 'cc2_advanced_settings', false);
			
			if( !empty( $advanced_settings ) && !empty( $advanced_settings['custom_script_' . $position] ) ) { ?>
<!-- Custom Script: <?php echo $position; ?> -->
<script type="text/javascript" charset="utf-8">

<?php echo stripslashes_deep($advanced_settings['custom_script_' . $position]); ?>

</script>

<!-- /Custom script: <?php echo $position; ?> -->

<?php
			}
		
		}
		
		/*
		* Scroll-to-top button customization
		*/
		
	
		function scroll_top_link_container_height( $data ) {
			$return = $data;
			
			if( isset( $this->arrSettings['scroll_top']['height'] ) ) {
				$return = $this->arrSettings['scroll_top']['height'];
			}
			
			return $return;
		}
		
		function scroll_top_link_container_offset( $data ) {
			$return = $data;
			
			if( isset( $this->arrSettings['scroll_top']['offset'] ) ) {
				$return = $this->arrSettings['scroll_top']['offset'];
			}
			
			return $return;
			
		}
		
		function scroll_top_link_container_position( $data ) {
			$return = $data;
			$position_offset = 10;
			
			if( isset( $this->arrSettings['scroll_top']['position_offset'] ) ) {
				$position_offset = $this->arrSettings['scroll_top']['position_offset'];
			}
			
			switch( $this->arrSettings['scroll_top']['position'] ) {
				case 'left':
					$return = 'left: '. $position_offset . 'px;';
					break;
				case 'right':
					$return = 'right: ' . $position_offset . 'px;';
				default:
					break;
			}
			
			return $return;
			
		}
		
		/**
		 * Top link data
		 */
		 
		function scroll_top_button_icon_class( $content ) {
			$return = $content;
			
			if( isset( $this->arrSettings['scroll_top']['button_icon_class'] ) ) {
				$return = $this->arrSettings['scroll_top']['button_icon_class'];
				
				if( strpos( $return, '.' ) !== false || strpos( $return, '#') !== false ) { // no selectors allowed (sanity check)
					$return = str_replace( array('.', '#'), '', $return );
				}
			}
			
			return $return;
		}
	
		function scroll_top_button_text( $content ) {
			$return = $content;
			
			if( isset( $this->arrSettings['scroll_top']['button_text'] ) ) {
				$return = $this->arrSettings['scroll_top']['button_text'];
			}
			
			return $return;
		}

		function scroll_top_button_link( $content ) {
			$return = $content;
			
			if( isset( $this->arrSettings['scroll_top']['button_link'] ) ) {
				$return = $this->arrSettings['scroll_top']['button_link'];
			}
			
			return $return;
		}

		
	}

	// fire it up!
	//add_action( 'cc2_support_add', array( 'cc2pro_HookAdditions', 'add_support_button') );
	
	add_action('after_setup_theme', array( 'cc2pro_HookAdditions', 'get_instance' ), 9 );

endif;
